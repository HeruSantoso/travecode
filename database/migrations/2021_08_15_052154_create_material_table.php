<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaterialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material', function (Blueprint $table) {
            $table->string('material_id')->primary();
            $table->string('name');
            $table->string('description');
            $table->string('picture');
            $table->string('category')->nullable();
            $table->integer('number_of_meetings');
            $table->string('unit_of_meeting');
            $table->integer('min_quota');
            $table->integer('min_age');
            $table->integer('max_age');
            $table->timestamps();
            $table->string('created_by');
            $table->string('updated_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
