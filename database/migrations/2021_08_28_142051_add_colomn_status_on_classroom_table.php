<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColomnStatusOnClassroomTable extends Migration
{
    /**
     * Run the migrations.
     * status :
     * 0 : draft
     * 1 : open
     * 2 : close
     * @return void
     */
    public function up()
    {
        Schema::table('classroom', function($table) {
            $table->integer('status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
