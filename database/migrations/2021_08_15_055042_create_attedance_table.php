<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttedanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attedance', function (Blueprint $table) {
            $table->string('attedance_id')->primary();
            $table->string('schedule_id');
            // $table->foreign('schedule_id')->references('schedule_id')->on('schedule');
            $table->string('participant_id');
            // $table->foreign('participant_id')->references('participant_id')->on('participant');
            $table->boolean('is_attend');
            $table->string('notes');            
            $table->string('created_by');
            $table->string('updated_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attedance');
    }
}
