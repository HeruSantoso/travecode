<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClassroomRegistrationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classroom_registration', function (Blueprint $table) {
            $table->string('classroom_registration_id')->primary();
            $table->string('student_id');
            // $table->foreign('student_id')->references('user_id')->on('users');
            $table->integer('status');
            $table->string('evidence_of_payment')->nullable();
            $table->string('verified_by')->nullable();
            $table->timestamps();
            $table->string('created_by');
            $table->string('updated_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classroom_registration');
    }
}
