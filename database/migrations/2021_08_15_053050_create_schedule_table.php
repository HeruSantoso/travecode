<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedule', function (Blueprint $table) {
            $table->string('schedule_id')->primary();
            $table->string('topic');
            $table->datetime('start');
            $table->datetime('end');
            $table->string('meeting_link');
            $table->string('module')->nullable();
            $table->string('evidence_live_session')->nullable();
            $table->boolean('is_any_homework')->nullable();
            $table->boolean('number_participant')->nullable();
            $table->string('notes')->nullable();
            $table->string('classroom_id');
            // $table->foreign('classroom_id')->references('classroom_id')->on('classroom');
            $table->string('facilitator_id');
            // $table->foreign('facilitator_id')->references('user_id')->on('users');
            $table->timestamps();
            $table->string('created_by');
            $table->string('updated_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedule');
    }
}
