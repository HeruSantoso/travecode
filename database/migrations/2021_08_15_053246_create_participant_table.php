<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParticipantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participant', function (Blueprint $table) {
            $table->string('participant_id')->primary();
            $table->string('classroom_id');
            // $table->foreign('classroom_id')->references('classroom_id')->on('classroom');
            $table->string('classroom_registration_id');
            // $table->foreign('classroom_registration_id')->references('classroom_registration_id')->on('classroom_registration');
            $table->string('student_id');
            // $table->foreign('student_id')->references('user_id')->on('users');
            $table->double('mid_test_score')->nullable();
            $table->double('final_test_score')->nullable();
            $table->timestamps();            
            $table->string('created_by');
            $table->string('updated_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participant');
    }
}
