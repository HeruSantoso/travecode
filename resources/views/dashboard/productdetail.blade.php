@extends('dashboard.layouts.app')

@section('content')
<div class="container">
    <input id="productId" type="hidden" value="{{ $productId }}"/>
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">Product</h4>
            <ul class="breadcrumbs">
                <li class="nav-home">
                    <a href="/lms">
                        <i class="flaticon-home"></i>
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="/lms/product">Product</a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="/lms/product/121">Detail</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="col-sm-12">
                <p class="small">Update a product using this form, make sure you fill them all</p>
                <div class="form-group form-group-default">
                    <label>Name</label>
                    <input id="inputName" type="text" class="form-control" placeholder="fill name" data-bind="value: form.name"/>
                </div>
                <div class="form-group form-group-default">
                    <label>Image Path</label>
                    <input id="inputImagePath" type="text" class="form-control" placeholder="fill image path" data-bind="value: form.picture"/>
                </div>
                <div class="form-group form-group-default">
                    <label>Description</label>
                    <textarea id="textareaDescription" type="text" class="form-control" placeholder="fill description" rows=5 data-bind="value: form.description"></textarea>
                </div>
            </div>
            <div class="modal-footer no-bd">
                <button type="button" class="btn btn-primary" data-bind="click: updateProduct">Save Product</button>
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addPortfolioModal" data-bind="click: productDetailViewModel.preCreatePortfolio">Add Portofolio</button>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="col-sm-12">
                <p class="small">List of portfolio</p>
                <div class="table-responsive">
                    <table id="add-row" class="display table table-striped table-hover" >
                        <thead>
                            <tr>
                                <th style="width: 80%">Description</th>
                                <th style="width: 20%">Action</th>
                            </tr>
                        </thead>
                        <tbody data-bind="foreach: portfolioList">
                            <tr>
                                <td data-bind="text: description"></td>
                                <td>
                                    <div class="form-button-action">
                                        <button class="btn btn-link btn-primary" data-toggle="modal" data-target="#addPortfolioModal" data-bind="click: productDetailViewModel.preUpdatePortfolio"><i class="fa fa-edit"></i></button>
                                        <button class="btn btn-link btn-danger" data-bind="click: productDetailViewModel.deletePortfolio"><i class="fa fa-times"></i></button>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addPortfolioModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header no-bd">
                    <h5 class="modal-title">
                        <span class="fw-mediumbold">
                        New</span> 
                        <span class="fw-light">
                            Portfolio
                        </span>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="small">Create a new portfolio using this form, make sure you fill them all</p>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-default">
                                <label>Description</label>
                                <textarea type="text" class="form-control" placeholder="fill description" rows=5 data-bind="value: portfolio.description"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer no-bd">
                    <button type="button" id="addRowButton" class="btn btn-primary" data-bind="click: createPortfolio">Add</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    
</script>
@endsection
@section('javascript')
<script src="{{ URL::asset('js/dashboard/product/productdetail.js') }}"></script>
@endsection