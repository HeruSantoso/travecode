@extends('dashboard.layouts.app')

@section('content')
<div class="container">
    <div class="panel-header bg-primary-gradient">
        <div class="page-inner py-5">
            <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                <div>
                    <h2 class="text-white pb-2 fw-bold" data-bind="text: 'Welcome ' + name"></h2>
                    <h5 class="text-white op-7 mb-2">Learning Management System</h5>
                </div>
                <div class="ml-md-auto py-2 py-md-0">
                    <a href="/" class="btn btn-secondary btn-round">Go to website</a>
                </div>
            </div>
        </div>
    </div>
    <row>
        <div class="col-md-12" style="margin: 20px 0">
            <div class="card">
                <div class="card-body">
                    <div id="calendar"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Schedule Information Modal -->
    <div class="modal fade" id="scheduleInfoModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header no-bd">
                    <h5 class="modal-title">
                        <span class="fw-mediumbold">Schedule</span> 
                        <span class="fw-light">Information</span>
                        <span class="badge badge-secondary" data-bind="text: schedule.classroom_status_text, class: schedule.classroom_status_class"></span>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="small">Here is all information about this schedule</p>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-default">
                                <label>Classroom</label>
                                <input id="input-classroom-name" type="text" class="form-control travecode-disabled" placeholder="Classroom" data-bind="value: schedule.classroom_name"/>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group form-group-default">
                                <label>Topic</label>
                                <input id="input-topic-name" type="text" class="form-control travecode-disabled" placeholder="Topic" data-bind="value: schedule.topic"/>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group form-group-default">
                                <label>Facilitator</label>
                                <input id="input-topic-name" type="text" class="form-control travecode-disabled" placeholder="Facilitator" data-bind="value: schedule.facilitator_name"/>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group form-group-default">
                                <label>Room Link</label>
                                <a data-bind="attr: { href: schedule.meeting_link, title: 'Room Link' }" target="_blank">
                                    <input id="input-room-link-name" type="text" class="form-control" placeholder="Room Link" data-bind="value: schedule.meeting_link"/>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer no-bd">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script src="{{ URL::asset('js/dashboard/home/home.js') }}"></script>
@endsection