@extends('dashboard.layouts.app')

@section('content')
<div class="container">
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">Student</h4>
            <ul class="breadcrumbs">
                <li class="nav-home">
                    <a href="/lms">
                        <i class="flaticon-home"></i>
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="/lms/student">Student</a>
                </li>
            </ul>
        </div>
    </div>
    <row>
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <!-- Add Modal -->
                    <div class="modal fade" id="studentFormModal" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header no-bd">
                                    <h5 class="modal-title">
                                        <span class="fw-mediumbold" data-bind="text: page.title"></span> 
                                        <span class="fw-light">Student</span>
                                    </h5>
                                    <button type="button" class="close" data-bind="click: closeModal">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <p class="small" data-bind="text: page.description"></p>
                                    <div class="row">
                                        <input type="hidden" data-bind="value: form.student_id"/>
                                        <div class="col-sm-12">
                                            <div class="form-group form-group-default">
                                                <label>Name</label>
                                                <input id="input-name" type="text" class="form-control" placeholder="Student name" data-bind="value: form.name"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group form-group-default">
                                                <label>Email</label>
                                                <input id="input-email" type="text" class="form-control" placeholder="Email" data-bind="value: form.email"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group form-group-default">
                                                <label>Phone Number</label>
                                                <input id="input-phone-number" type="text" class="form-control" placeholder="Phone Number" data-bind="value: form.phone_number"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group form-group-default">
                                                <label>Grade</label>
                                                <input id="input-grade" type="text" class="form-control" placeholder="Grade" data-bind="value: form.grade"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group form-group-default">
                                                <label>Date of Birth</label>
                                                <div class="input-group" data-bind="value: form.dob">
                                                    <input type="text" class="form-control" id="dob" name="datepicker travecode-datepicker" >
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-calendar"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 travecode-div-container">
                                            <div class="form-group form-group-default">
                                                <label>Password</label>
                                                <input id="input-name" type="password" class="form-control" placeholder="Password" data-bind="value: form.password"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 travecode-div-container">
                                            <div class="form-group form-group-default">
                                                <label>Confirmation Password</label>
                                                <input id="input-name" type="password" class="form-control" placeholder="Password" data-bind="value: form.password_confirmation"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer no-bd">
                                    <button type="button" id="addRowButton" class="btn btn-primary" data-bind="click: saveStudent">Save</button>
                                    <button type="button" class="btn btn-danger" data-bind="click: closeModal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="resetModal" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header no-bd">
                                    <h5 class="modal-title">
                                        <span class="fw-mediumbold" data-bind="text: 'Reset'"></span> 
                                        <span class="fw-light">Password</span>
                                    </h5>
                                    <button type="button" class="close" data-bind="click: closeResetModal">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <p class="small" data-bind="text: 'This form is used for reset password'"></p>
                                    <div class="row">
                                        <div class="col-sm-12 travecode-div-container">
                                            <div class="form-group form-group-default">
                                                <label>Password</label>
                                                <input id="input-name" type="password" class="form-control" placeholder="Password" data-bind="value: resetForm.password"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 travecode-div-container">
                                            <div class="form-group form-group-default">
                                                <label>Confirmation Password</label>
                                                <input id="input-name" type="password" class="form-control" placeholder="Confirmation Password" data-bind="value: resetForm.password_confirmation"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer no-bd">
                                    <button type="button" class="btn btn-primary" data-bind="click: resetPassword">Reset</button>
                                    <button type="button" class="btn btn-danger" data-bind="click: closeResetModal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table id="add-row" class="display table table-striped table-hover" >
                            <thead>
                                <tr>
                                    <th >Name</th>
                                    <th >Email</th>
                                    <th >Phone Number</th>
                                    <th >Date of Birth</th>
                                    <th >Created at</th>
                                    <th >Updated at</th>
                                    <th >Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="d-flex align-items-center">
                        <button class="btn btn-primary btn-round ml-auto" onclick="studentViewModel.openModal(null)">
                            <i class="fa fa-plus"></i>
                            Add New Student
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </row>
</div>
<script>
    
</script>
@endsection
@section('javascript')
<script src="{{ URL::asset('js/dashboard/student/student.js') }}"></script>
@endsection