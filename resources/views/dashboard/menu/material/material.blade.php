@extends('dashboard.layouts.app')

@section('content')
<div class="container">
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">Material</h4>
            <ul class="breadcrumbs">
                <li class="nav-home">
                    <a href="/lms">
                        <i class="flaticon-home"></i>
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="/lms/material">Material</a>
                </li>
            </ul>
        </div>
    </div>
    <row>
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <!-- Add Modal -->
                    <div class="modal fade" id="materialFormModal" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header no-bd">
                                    <h5 class="modal-title">
                                        <span class="fw-mediumbold" data-bind="text: page.title"></span> 
                                        <span class="fw-light">Material</span>
                                    </h5>
                                    <button type="button" class="close" data-bind="click: closeModal">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <p class="small" data-bind="text: page.description"></p>
                                    <div class="row">
                                        <input type="hidden" data-bind="value: form.material_id"/>
                                        <div class="col-sm-12">
                                            <div class="form-group form-group-default">
                                                <label>Name</label>
                                                <input id="input-name" type="text" class="form-control" placeholder="Material name" data-bind="value: form.name"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group form-group-default">
                                                <label>Description</label>
                                                <textarea id="textarea-description" type="text" class="form-control" placeholder="Description" rows=5 data-bind="value: form.description"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-sm-5">
                                            <div class="form-group form-group-default">
                                                <label>Number of Meeting</label>
                                                <input id="input-number-of-meeting" type="text" class="form-control" placeholder="Number of Meeting" data-bind="value: form.number_of_meetings"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-7">
                                            <div class="form-group form-group-default">
                                                <label>Unit</label>
                                                <input id="input-unit-of-meeting" type="text" class="form-control" placeholder="Unit of meeting" data-bind="value: form.unit_of_meeting"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group form-group-default">
                                                <label>Minimal Quota</label>
                                                <input name="input-input-quota" type="text" class="form-control" placeholder="Minimal Quota" data-bind="value: form.min_quota"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group form-group-default">
                                                <label>Min Age</label>
                                                <input name="input-input-quota" type="text" class="form-control" placeholder="Minimal Age" data-bind="value: form.min_age"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group form-group-default">
                                                <label>Max Age</label>
                                                <input name="input-input-quota" type="text" class="form-control" placeholder="Maximal Age" data-bind="value: form.max_age"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group form-group-default">
                                                <label>Image Path</label>
                                                <input id="inputImagePath" type="text" class="form-control" placeholder="fill image path" data-bind="value: form.picture"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer no-bd">
                                    <button type="button" id="addRowButton" class="btn btn-primary" data-bind="click: saveMaterial">Save</button>
                                    <button type="button" class="btn btn-danger" data-bind="click: closeModal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table id="add-row" class="display table table-striped table-hover" >
                            <thead>
                                <tr>
                                    <th >Name</th>
                                    <th >Number of Meeting</th>
                                    <th >Min Quota</th>
                                    <th >Created at</th>
                                    <th >Updated at</th>
                                    <th >Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="d-flex align-items-center">
                        <button class="btn btn-primary btn-round ml-auto" onclick="materialViewModel.openModal(null)">
                            <i class="fa fa-plus"></i>
                            Add New Material
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </row>
</div>
<script>
    
</script>
@endsection
@section('javascript')
<script src="{{ URL::asset('js/dashboard/material/material.js') }}"></script>
@endsection