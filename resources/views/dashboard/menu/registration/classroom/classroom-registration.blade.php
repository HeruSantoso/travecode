@extends('dashboard.layouts.app')

@section('content')
<div class="container">
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">Classroom Registration</h4>
            <ul class="breadcrumbs">
                <li class="nav-home">
                    <a href="/lms">
                        <i class="flaticon-home"></i>
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="/lms/classroom-registration">Classroom Registration</a>
                </li>
            </ul>
        </div>
    </div>
    <row>
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <!-- Add Modal -->
                    <div class="modal fade" id="registrationModal" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header no-bd">
                                    <h5 class="modal-title">
                                        <span class="fw-mediumbold" data-bind="text: page.title"></span> 
                                        <span class="fw-light">Registration</span>
                                    </h5>
                                    <button type="button" class="close" data-bind="click: closeModal">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <p class="small" data-bind="text: page.description"></p>
                                    <div class="row">
                                        <input type="hidden" data-bind="value: form.classroom_registration_id"/>
                                        <div class="col-sm-12">
                                            <div class="form-group form-group-default">
                                                <label>Student</label>
                                                <select id="select-student" class="form-control travecode-select"
                                                    data-bind=" options: studentList,
                                                                optionsText: 'name',
                                                                value: form.student,
                                                                optionsCaption: 'Choose...'"      
                                                ></select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group form-group-default">
                                                <label>Classroom</label>
                                                <select id="select-classroom" class="form-control travecode-select"
                                                    data-bind=" options: classroomList,
                                                                optionsText: 'classroom_name',
                                                                value: form.classroom,
                                                                optionsCaption: 'Choose...'"      
                                                ></select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer no-bd">
                                    <button type="button" id="registrationButton" class="btn btn-primary" data-bind="click: classroomRegistration">Submit</button>
                                    <button type="button" class="btn btn-danger" data-bind="click: closeModal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Pay -->
                    <div class="modal fade" id="payModal" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header no-bd">
                                    <h5 class="modal-title">
                                        <span class="fw-mediumbold">Pay</span> 
                                    </h5>
                                    <button type="button" class="close" data-bind="click: closePayModal">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <p class="small" data-bind="text: 'This form is used for pay registration'"></p>
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <div class="form-group form-group-default">
                                                <label>Evidence of payment</label>
                                                <input id="input-live-session-name" type="text" class="form-control" placeholder="Evidence of payment link" data-bind="value: payForm.evidence_of_payment"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <button class="btn btn-black" style="width: 100%; margin-bottom: 15px;">
                                                <span class="btn-label"><i class="fas fa-cloud-upload-alt"></i></span>
                                                Upload
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer no-bd">
                                    <button type="button" id="payButton" class="btn btn-primary" data-bind="click: pay">Pay</button>
                                    <button type="button" class="btn btn-danger" data-bind="click: closePayModal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Reject -->
                    <div class="modal fade" id="rejectModal" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header no-bd">
                                    <h5 class="modal-title">
                                        <span class="fw-mediumbold">Reject</span> 
                                    </h5>
                                    <button type="button" class="close" data-bind="click: closeRejectModal">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <p class="small" data-bind="text: 'This form is used for reject payment'"></p>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group form-group-default">
                                                <label>Reason</label>
                                                <textarea id="textarea-description" type="text" class="form-control" placeholder="Reason" rows=5 data-bind="value: rejectForm.rejected_reason"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer no-bd">
                                    <button type="button" id="payButton" class="btn btn-primary" data-bind="click: reject">Reject</button>
                                    <button type="button" class="btn btn-danger" data-bind="click: closeRejectModal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table id="add-row" class="display table table-striped table-hover" >
                            <thead>
                                <tr>
                                    <th >Student</th>
                                    <!-- <th >Email</th> -->
                                    <th >Phone Number</th>
                                    <th >Classroom</th>
                                    <th >Status</th>
                                    <!-- <th >Created at</th> -->
                                    <th >Updated at</th>
                                    <th >Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="d-flex align-items-center">
                        <button class="btn btn-primary btn-round ml-auto" data-bind="click: openRegistrationModal">
                            <i class="fa fa-plus"></i>
                            Add New Registration
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </row>
</div>
<script>
    
</script>
@endsection
@section('javascript')
<script src="{{ URL::asset('js/dashboard/registration/classroom/classroom-registration.js') }}"></script>
@endsection