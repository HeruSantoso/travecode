@extends('dashboard.layouts.app')

@section('content')
<div class="container">
    <input id="classroomId" type="hidden" value="{{ $classroomId ?? '' }}"/>
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">Classroom Detail</h4>
            <ul class="breadcrumbs">
                <li class="nav-home">
                    <a href="/lms">
                        <i class="flaticon-home"></i>
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="/lms/classroom">Classroom</a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="/lms/classroom/{{$classroomId}}" >
                        <span data-bind="text: page.classroom.classroom_name"></span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <row>
        <div class="col-md-12">
            <ul class="nav nav-pills nav-secondary nav-pills-no-bd mb-3" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="pills-schedule-tab-icon" data-toggle="pill" href="#pills-schedule-icon" role="tab" aria-controls="pills-home-nobd" aria-selected="true">
                        Schedule <span class="badge badge-count" data-bind="text: page.scheduleCount">0</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-participant-tab-icon" data-toggle="pill" href="#pills-participant-icon" role="tab" aria-controls="pills-profile-nobd" aria-selected="false">
                        Participant <span class="badge badge-count" data-bind="text: page.participantCount">0</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-attedance-tab-icon" data-toggle="pill" href="#pills-attedance-icon" role="tab" aria-controls="pills-profile-nobd" aria-selected="false">
                        Attedance <span class="badge badge-count" data-bind="text: page.participantCount">0</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content mb-3" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-schedule-icon" role="tabpanel" aria-labelledby="pills-schedule-tab-icon">
                    <div class="card">
                        <!-- Classroom Information Modal -->
                        <div class="modal fade" id="classroomInfoModal" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header no-bd">
                                        <h5 class="modal-title">
                                            <span class="fw-mediumbold">Classroom</span> 
                                            <span class="fw-light">Information</span>
                                        </h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <p class="small">Here is all information about this classroom</p>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group form-group-default">
                                                    <label>Classroom</label>
                                                    <input id="input-classroom-name" type="text" class="form-control travecode-disabled" placeholder="Classroom" data-bind="value: page.classroom.classroom_name"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group form-group-default">
                                                    <label>Material</label>
                                                    <input id="input-material-name" type="text" class="form-control travecode-disabled" placeholder="Material" data-bind="value: page.material.name"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="form-group form-group-default">
                                                    <label>Number of meeting</label>
                                                    <input id="input-uom-name" type="text" class="form-control travecode-disabled" placeholder="Number of meeting" data-bind="value: page.material.number_of_meetings"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group form-group-default">
                                                    <label>Min Quota</label>
                                                    <input id="input-min-quota-name" type="text" class="form-control travecode-disabled" placeholder="Min quota" data-bind="value: page.material.min_quota"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group form-group-default">
                                                    <label>Main Facilitator</label>
                                                    <input id="input-facilitator-name" type="text" class="form-control travecode-disabled" placeholder="Classroom name" data-bind="value: page.facilitator.facilitator_name"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer no-bd">
                                        <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Schedule Facilitator Modal -->
                        <div class="modal fade" id="facilitatorSubmissionModal" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header no-bd">
                                        <h5 class="modal-title">
                                            <span class="fw-mediumbold">Facilitator</span> 
                                            <span class="fw-light">Submission</span>
                                        </h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <p class="small">Here is all information about this schedule</p>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group form-group-default">
                                                    <label>Module</label>
                                                    <a data-bind="attr: { href: submissionForm.module, title: 'Download Module' }" target="_blank">
                                                        <input id="input-module-name" type="text" class="form-control travecode-disabled" placeholder="Classroom" data-bind="value: 'Download Module'"/>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group form-group-default">
                                                    <label>Live Session Screenshoot</label>
                                                    <a data-bind="attr: { href: submissionForm.evidence_live_session }" target="_blank">
                                                        <input id="input-live-session-name" type="text" class="form-control travecode-disabled" placeholder="Classroom" data-bind="value: 'View Evidence'"/>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group form-group-default">
                                                    <label>Any homework</label>
                                                    <input type="checkbox" checked data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger" data-bind="attr: { checked: submissionForm.is_any_homework, disabled: true }"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group form-group-default">
                                                    <label>Notes</label>
                                                    <textarea id="textarea-description" type="text" class="form-control travecode-disabled" placeholder="Notes" rows=5 data-bind="value: submissionForm.notes"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer no-bd">
                                        <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Schedule Facilitator Modal -->
                        <div class="modal fade" id="facilitatorEditModal" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header no-bd">
                                        <h5 class="modal-title">
                                            <span class="fw-mediumbold">Facilitator</span> 
                                            <span class="fw-light">Submission</span>
                                        </h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <p class="small">Here is all information about this schedule</p>
                                        <div class="row">
                                            <div class="col-sm-8">
                                                <div class="form-group form-group-default">
                                                    <label>Module</label>
                                                    <input id="input-module-name" type="text" class="form-control" placeholder="Classroom" data-bind="value: submissionForm.module"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <button class="btn btn-black" style="width: 100%; margin-bottom: 15px;">
                                                    <span class="btn-label"><i class="fas fa-cloud-upload-alt"></i></span>
                                                    Upload
                                                </button>
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="form-group form-group-default">
                                                    <label>Live Session Screenshoot</label>
                                                    <input id="input-live-session-name" type="text" class="form-control" placeholder="Classroom" data-bind="value: submissionForm.evidence_live_session"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <button class="btn btn-black" style="width: 100%; margin-bottom: 15px;">
                                                    <span class="btn-label"><i class="fas fa-cloud-upload-alt"></i></span>
                                                    Upload
                                                </button>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group form-group-default">
                                                    <label>Any homework</label>
                                                    <input id="is-any-homework-toggle" type="checkbox" data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="primary" data-offstyle="danger"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group form-group-default">
                                                    <label>Notes</label>
                                                    <textarea id="textarea-description" type="text" class="form-control" placeholder="Notes" rows=5 data-bind="value: submissionForm.notes"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer no-bd">
                                        <button type="button" id="facil-submission-button" class="btn btn-primary" data-bind="click: facilitatorSubmission">Save</button>
                                        <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Add Modal -->
                        <div class="modal fade" id="scheduleFormModal" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header no-bd">
                                        <h5 class="modal-title">
                                            <span class="fw-mediumbold" data-bind="text: page.title"></span> 
                                            <span class="fw-light">Schedule</span>
                                        </h5>
                                        <button type="button" class="close" data-bind="click: closeModal">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <p class="small" data-bind="text: page.description"></p>
                                        <div class="row">
                                            <input type="hidden" data-bind="value: form.schedule_id"/>
                                            <div class="col-sm-12">
                                                <div class="form-group form-group-default">
                                                    <label>Topic</label>
                                                    <input id="input-topic-name" type="text" class="form-control" placeholder="Topic" data-bind="value: form.topic"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group form-group-default">
                                                    <label>Date</label>
                                                    <div class="input-group" data-bind="value: form.start_date">
                                                        <input type="text" class="form-control" id="datepicker-date" name="datepicker travecode-datepicker" >
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="fa fa-calendar"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group form-group-default">
                                                    <label>Start Time</label>
                                                    <div class="input-group" data-bind="value: form.start_time">
                                                        <input type="text" class="form-control" id="timepicker-start" name="timepicker travecode-timepicker" >
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="fa fa-clock"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group form-group-default">
                                                    <label>End Time</label>
                                                    <div class="input-group" data-bind="value: form.end_time">
                                                        <input type="text" class="form-control" id="timepicker-end" name="timepicker travecode-timepicker">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="fa fa-clock"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group form-group-default">
                                                    <label>Room Link</label>
                                                    <input id="input-room-link-name" type="text" class="form-control" placeholder="Room Link" data-bind="value: form.meeting_link"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group form-group-default">
                                                    <label>Facilitator</label>
                                                    <select id="select-facilitator"class="form-control travecode-select"
                                                        data-bind=" options: facilitatorList,
                                                                    optionsText: 'name',
                                                                    value: form.facilitator,
                                                                    optionsCaption: 'Choose...'"      
                                                    ></select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer no-bd">
                                        <button type="button" id="addRowButton" class="btn btn-primary" data-bind="click: saveClassroom">Save</button>
                                        <button type="button" class="btn btn-danger" data-bind="click: closeModal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="add-row" class="display table table-striped table-hover" >
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Topic</th>
                                            <th>Start</th>
                                            <th>End</th>
                                            <th>Facilitator</th>
                                            <th>Room Link</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="align-items-center">
                                <button class="btn btn-secondary btn-round ml-auto" onclick="classroomDetailViewModel.openClassroomModalDetail()">
                                    <i class="fa fa-info" style="margin-right: 10px"></i>
                                    Classroom Information
                                </button>
                                <button class="btn btn-primary btn-round ml-auto float-right" onclick="classroomDetailViewModel.openModal(null)" data-bind="visible: isAdmin()">
                                    <i class="fa fa-plus"></i>
                                    Add New Schedule
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-participant-icon" role="tabpanel" aria-labelledby="pills-participant-tab-icon">
                    <div class="card">
                        <div class="modal fade" id="updateScoreModal" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header no-bd">
                                        <h5 class="modal-title">
                                            <span class="fw-mediumbold" data-bind="text: 'Update score'"></span> 
                                            <span class="fw-light">Schedule</span>
                                        </h5>
                                        <button type="button" class="close" data-bind="click: closeParticipantScoreModal">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <p class="small" data-bind="text: 'This form is used for update score'"></p>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group form-group-default">
                                                    <label>Mid score</label>
                                                    <input id="input-mid-score" type="number" class="form-control" placeholder="Mid Score" data-bind="value: participantForm.mid_test_score"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group form-group-default">
                                                    <label>Final score</label>
                                                    <input id="input-final-score" type="number" class="form-control" placeholder="Final Score" data-bind="value: participantForm.final_test_score"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer no-bd">
                                        <button type="button" class="btn btn-primary" data-bind="click: updateScore">Save</button>
                                        <button type="button" class="btn btn-danger" data-bind="click: closeParticipantScoreModal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="participant-row" class="display table table-striped table-hover" >
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Student Name</th>
                                            <th>Mid Test Score</th>
                                            <th>Final Test Score</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-attedance-icon" role="tabpanel" aria-labelledby="pills-attedance-tab-icon">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="participant-row" class="display table table-striped table-hover" >
                                    <thead>
                                        <tr>
                                            <th>Student Name</th>
                                            <!--ko foreach: { data: attedanceColumns, as: 'date' } -->
                                            <th class="travecode-center" data-bind="text: date"></th>
                                            <!--/ko-->
                                        </tr>
                                    </thead>
                                    <tbody data-bind="foreach: { data: attedances, as: 'att' }">
                                        <tr>
                                            <td data-bind="text: att.student_name"></td>
                                            <!--ko foreach: { data: schedules, as: 'schedule' } -->
                                            <td class="travecode-center">
                                                <input class="attend-toggle" type="checkbox" data-bind="attr: { checked: schedule.attedance.is_attend, 'data-participant': att.participant_id, 'data-schedule': schedule.schedule_id }, event:{ change: $root.onAttendChange }" />
                                            </td>
                                            <!--/ko-->
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </row>
</div>
<script>
    
</script>
@endsection
@section('javascript')
<script src="{{ URL::asset('js/dashboard/classroom/detail/classroom-detail.js') }}"></script>
@endsection