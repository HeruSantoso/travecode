@extends('dashboard.layouts.app')

@section('content')
<div class="container">
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">Classroom</h4>
            <ul class="breadcrumbs">
                <li class="nav-home">
                    <a href="/lms">
                        <i class="flaticon-home"></i>
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="/lms/classroom">Classroom</a>
                </li>
            </ul>
        </div>
    </div>
    <row>
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <!-- Add Modal -->
                    <div class="modal fade" id="classroomFormModal" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header no-bd">
                                    <h5 class="modal-title">
                                        <span class="fw-mediumbold" data-bind="text: page.title"></span> 
                                        <span class="fw-light">Classroom</span>
                                    </h5>
                                    <button type="button" class="close" data-bind="click: closeModal">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <p class="small" data-bind="text: page.description"></p>
                                    <div class="row">
                                        <input type="hidden" data-bind="value: form.classroom_id"/>
                                        <div class="col-sm-12">
                                            <div class="form-group form-group-default">
                                                <label>Name</label>
                                                <input id="input-classroom-name" type="text" class="form-control" placeholder="Classroom name" data-bind="value: form.classroom_name"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group form-group-default">
                                                <label>Material</label>
                                                <select id="select-material" class="form-control travecode-select"
                                                    data-bind=" options: materialList,
                                                                optionsText: 'name',
                                                                value: form.material,
                                                                optionsCaption: 'Choose...'"      
                                                ></select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group form-group-default">
                                                <label>Facilitator</label>
                                                <select id="select-facilitator"class="form-control travecode-select"
                                                    data-bind=" options: facilitatorList,
                                                                optionsText: 'name',
                                                                value: form.facilitator,
                                                                optionsCaption: 'Choose...'"      
                                                ></select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group form-group-default">
                                                <label>Status</label>
                                                <select id="select-status"class="form-control travecode-select"
                                                    data-bind=" options: statusList,
                                                                optionsText: 'text',
                                                                value: form.status,
                                                                optionsCaption: 'Choose...'"      
                                                ></select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer no-bd">
                                    <button type="button" id="addRowButton" class="btn btn-primary" data-bind="click: saveClassroom">Save</button>
                                    <button type="button" class="btn btn-danger" data-bind="click: closeModal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table id="add-row" class="display table table-striped table-hover" >
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Materi</th>
                                    <th>Facilitator</th>
                                    <th>Status</th>
                                    <th>Created at</th>
                                    <th>Updated at</th>
                                    <th data-bind="visible: isAdmin">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="card-footer" data-bind="visible: isAdmin()">
                    <div class="d-flex align-items-center">
                        <button class="btn btn-primary btn-round ml-auto" onclick="classroomViewModel.openModal(null)">
                            <i class="fa fa-plus"></i>
                            Add New Classroom
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </row>
</div>
<script>
    
</script>
@endsection
@section('javascript')
<script src="{{ URL::asset('js/dashboard/classroom/classroom.js') }}"></script>
@endsection