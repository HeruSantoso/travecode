@extends('dashboard.layouts.app')

@section('content')
<div class="container">
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">Product</h4>
            <ul class="breadcrumbs">
                <li class="nav-home">
                    <a href="/lms">
                        <i class="flaticon-home"></i>
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="/lms/product">Product</a>
                </li>
            </ul>
        </div>
    </div>
    <row>
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <!-- Modal -->
                    <div class="modal fade" id="addRowModal" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header no-bd">
                                    <h5 class="modal-title">
                                        <span class="fw-mediumbold">
                                        New</span> 
                                        <span class="fw-light">
                                            Product
                                        </span>
                                    </h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <p class="small">Create a new product using this form, make sure you fill them all</p>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group form-group-default">
                                                <label>Name</label>
                                                <input id="inputName" type="text" class="form-control" placeholder="fill name" data-bind="value: form.name"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group form-group-default">
                                                <label>Image Path</label>
                                                <input id="inputImagePath" type="text" class="form-control" placeholder="fill image path" data-bind="value: form.picture"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group form-group-default">
                                                <label>Description</label>
                                                <textarea id="textareaDescription" type="text" class="form-control" placeholder="fill description" rows=5 data-bind="value: form.description"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer no-bd">
                                    <button type="button" id="addRowButton" class="btn btn-primary" data-bind="click: createProduct">Add</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table id="add-row" class="display table table-striped table-hover" >
                            <thead>
                                <tr>
                                    <th style="width: 40%">Name</th>
                                    <th style="width: 20%">Created at</th>
                                    <th style="width: 20%">Updated at</th>
                                    <th style="width: 20%">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="d-flex align-items-center">
                        <button class="btn btn-primary btn-round ml-auto" data-toggle="modal" data-target="#addRowModal">
                            <i class="fa fa-plus"></i>
                            Add New Product
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </row>
</div>
<script>
    
</script>
@endsection
@section('javascript')
<script src="{{ URL::asset('js/dashboard/product/product.js') }}"></script>
@endsection