<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>Learning Management System</title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href=" {{ URL::asset('dashboard/img/icon.ico') }}" type="image/x-icon"/>

	<!-- Fonts and icons -->
	<script src="{{ URL::asset('dashboard/js/plugin/webfont/webfont.min.js') }}"></script>
	<script>
		WebFont.load({
			google: {"families":["Lato:300,400,700,900"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ["{{URL::asset('dashboard/css/fonts.min.css')}}"]},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>

	<!-- CSS Files -->
	<link rel="stylesheet" href=" {{ URL::asset('dashboard/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href=" {{ URL::asset('dashboard/css/atlantis.min.css') }}">
	<link rel="stylesheet" href=" {{ URL::asset('dashboard/css/login.css') }}">
</head>
<body class="login">
	<div class="wrapper wrapper-login">
		<div class="container container-login animated fadeIn" style="display: block;">
			<h3 class="text-center" style="margin-bottom:50px">Learning Management System</h3>
			<div class="login-form">
				<div class="form-group form-floating-label">
					<input id="username" name="username" type="text" class="form-control input-border-bottom" data-bind="value: email">
					<label for="username" class="placeholder">Email</label>
				</div>
				<div class="form-group form-floating-label">
					<input id="password" name="password" type="password" class="form-control input-border-bottom" data-bind="value: password">
					<label for="password" class="placeholder">Password</label>
					<div class="show-password">
						<i class="icon-eye"></i>
					</div>
				</div>
				<!-- <div class="row form-sub m-0">
					<div></div>
					<a href="#" class="link float-right">Forgot Password ?</a>
				</div> -->
				<div class="form-action mb-3">
					<button class="btn btn-primary btn-rounded btn-login" data-bind="click: login">Sign In</button>
				</div>
				<!-- <div class="login-account">
					<span class="msg">Don't have an account yet ?</span>
					<a href="#" id="show-signup" class="link">Sign Up</a>
				</div> -->
			</div>
		</div>
		<div class="container container-signup animated fadeIn" style="display: none;">
			<h3 class="text-center">Sign Up</h3>
			<div class="login-form">
				<div class="form-group form-floating-label">
					<input id="fullname" name="fullname" type="text" class="form-control input-border-bottom" required="">
					<label for="fullname" class="placeholder">Fullname</label>
				</div>
				<div class="form-group form-floating-label">
					<input id="email" name="email" type="email" class="form-control input-border-bottom" required="">
					<label for="email" class="placeholder">Email</label>
				</div>
				<div class="form-group form-floating-label">
					<input id="passwordsignin" name="passwordsignin" type="password" class="form-control input-border-bottom" required="">
					<label for="passwordsignin" class="placeholder">Password</label>
					<div class="show-password">
						<i class="icon-eye"></i>
					</div>
				</div>
				<div class="form-group form-floating-label">
					<input id="confirmpassword" name="confirmpassword" type="password" class="form-control input-border-bottom" required="">
					<label for="confirmpassword" class="placeholder">Confirm Password</label>
					<div class="show-password">
						<i class="icon-eye"></i>
					</div>
				</div>
				<div class="row form-sub m-0">
					<div class="custom-control custom-checkbox">
						<input type="checkbox" class="custom-control-input" name="agree" id="agree">
						<label class="custom-control-label" for="agree">I Agree the terms and conditions.</label>
					</div>
				</div>
				<div class="form-action">
					<a href="#" id="show-signin" class="btn btn-danger btn-link btn-login mr-3">Cancel</a>
					<a href="#" class="btn btn-primary btn-rounded btn-login">Sign Up</a>
				</div>
			</div>
		</div>
	</div>
	<div id="preloader"></div>
	<!--   Core JS Files   -->
	<script src="{{ URL::asset('dashboard/js/core/jquery.3.2.1.min.js') }}"></script>
	<script src="{{ URL::asset('dashboard/js/core/popper.min.js') }}"></script>
	<script src="{{ URL::asset('dashboard/js/core/bootstrap.min.js') }}"></script>

	<!-- jQuery UI -->
	<script src="{{ URL::asset('dashboard/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js') }}"></script>
	<script src="{{ URL::asset('dashboard/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js') }}"></script>

	<!-- jQuery Scrollbar -->
	<script src="{{ URL::asset('dashboard/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js') }}"></script>

	<!-- Bootstrap Notify -->
	<script src="{{ URL::asset('dashboard/js/plugin/bootstrap-notify/bootstrap-notify.min.js') }}"></script>

	<!-- Sweet Alert -->
	<script src="{{ URL::asset('dashboard/js/plugin/sweetalert/sweetalert.min.js') }}"></script>

	<!-- Atlantis JS -->
	<script src="{{ URL::asset('dashboard/js/atlantis.min.js') }}"></script>
	<script src="{{ URL::asset('js/knockout-3.5.1.min.js') }}"></script>
	<script src="{{ URL::asset('js/knockout-validation-2.0.4.min.js') }}"></script>
	<script src="{{ URL::asset('js/dashboard/dashboard.js') }}"></script>
	<script src="{{ URL::asset('js/dashboard/login/login.js') }}"></script>

</body>
</html>