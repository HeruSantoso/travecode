<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>Learning Management System</title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href=" {{ URL::asset('dashboard/img/travecode-favicon.png') }}" type="image/x-icon"/>

	<!-- Fonts and icons -->
	<script src="{{ URL::asset('dashboard/js/plugin/webfont/webfont.min.js') }}"></script>
	<script>
		WebFont.load({
			google: {"families":["Lato:300,400,700,900"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ["{{URL::asset('dashboard/css/fonts.min.css')}}"]},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>

	<!-- CSS Files -->
	<link rel="stylesheet" href=" {{ URL::asset('dashboard/css/main.css') }}">
	<link rel="stylesheet" href=" {{ URL::asset('dashboard/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href=" {{ URL::asset('dashboard/css/atlantis.css') }}">
</head>
<body>
	<div class="wrapper">
		<div class="main-header">
			<!-- Logo Header -->

			<div class="logo-header" data-background-color="blue">
				<a href="/" class="logo">
					<!-- <img src="{{URL::asset('dashboard/img/logo.svg')}}" alt="navbar brand" class="navbar-brand"> -->
					<span alt="navbar brand" class="navbar-brand" style="color: white; font-weight: 900; margin-top: 2px;">TRAVECODE</span>
				</a>
				<!-- <a href="/" class="admin-logo">TRAVECODE</a> -->
				<button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse" data-target="collapse" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon">
						<i class="icon-menu"></i>
					</span>
				</button>
				<button class="topbar-toggler more"><i class="icon-options-vertical"></i></button>
				<div class="nav-toggle">
					<button class="btn btn-toggle toggle-sidebar toggled"><i class="icon-options-vertical"></i></button>
				</div>
			</div>
			<!-- End Logo Header -->

			<!-- Navbar Header -->
			<nav class="navbar navbar-header navbar-expand-lg" data-background-color="blue2">
				
				<div class="container-fluid">
					<ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
						<li class="nav-item toggle-nav-search hidden-caret">
							<a class="nav-link" data-toggle="collapse" href="#search-nav" role="button" aria-expanded="false" aria-controls="search-nav">
								<i class="fa fa-search"></i>
							</a>
						</li>
						<li class="nav-item dropdown hidden-caret">
							<a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false">
								<div class="avatar-sm">
									<span class="avatar-title rounded-circle border border-white" data-bind="text: getInitials()"></span>
								</div>
							</a>
							<ul class="dropdown-menu dropdown-user animated fadeIn">
								<div class="dropdown-user-scroll scrollbar-outer">
									<li>
										<a class="dropdown-item" data-bind="click: logout">Logout</a>
									</li>
								</div>
							</ul>
						</li>
					</ul>
				</div>
			</nav>
			<!-- End Navbar -->
		</div>

		<!-- Sidebar -->
		<div class="sidebar sidebar-style-2">			
			<div class="sidebar-wrapper scrollbar scrollbar-inner">
				<div class="sidebar-content">
					<div class="user">
						<div class="avatar-sm float-left mr-2">
							<span class="avatar-title rounded-circle border border-white" data-bind="text: getInitials()"></span>
						</div>
						<div class="info">
							<a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
								<b><span data-bind="text: name"></span></b>
								<span class="user-level" data-bind="text: getRole()"></span>
								<span class="caret"></span>
							</a>
							<div class="clearfix"></div>

							<div class="collapse in" id="collapseExample">
								<ul class="nav">
									<li>
										<a data-bind="click: logout">
											<span class="link-collapse" >Logout</span>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<ul class="nav nav-primary" data-bind="visible: isAdmin()">
						<li class="nav-item">
							<a href="/lms">
								<i class="fas fa-home"></i>
								<p>Home</p>
							</a>
						</li>
						<li class="nav-section">
							<span class="sidebar-mini-icon">
								<i class="fa fa-ellipsis-h"></i>
							</span>
							<h4 class="text-section">Menu</h4>
						</li>
						<li class="nav-item">
							<a href="/lms/classroom-registration">
								<i class="fas fa-registered"></i>
								<p>Registration</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="/lms/material">
								<i class="fas fa-book-open"></i>
								<p>Material</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="/lms/classroom">
								<i class="fas fa-school"></i>
								<p>Classroom</p>
							</a>
						</li>
						<li class="nav-section">
							<span class="sidebar-mini-icon">
								<i class="fa fa-ellipsis-h"></i>
							</span>
							<h4 class="text-section">User</h4>
						</li>
						<li class="nav-item">
							<a href="/lms/facilitator">
								<i class="fas fa-chalkboard-teacher"></i>
								<p>Facilitator</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="/lms/student">
								<i class="fas fa-user-graduate"></i>
								<p>Student</p>
							</a>
						</li>
					</ul>
					<ul class="nav nav-primary" data-bind="visible: isFacilitator()">
						<li class="nav-item">
							<a href="/lms">
								<i class="fas fa-home"></i>
								<p>Home</p>
							</a>
						</li>
						<li class="nav-section">
							<span class="sidebar-mini-icon">
								<i class="fa fa-ellipsis-h"></i>
							</span>
							<h4 class="text-section">Menu</h4>
						</li>
						<li class="nav-item">
							<a href="/lms/classroom">
								<i class="fas fa-school"></i>
								<p>Classroom</p>
							</a>
						</li>
					</ul>
					<ul class="nav nav-primary" data-bind="visible: isStudent()">
						<li class="nav-item">
							<a href="/lms">
								<i class="fas fa-home"></i>
								<p>Home</p>
							</a>
						</li>
						<li class="nav-section">
							<span class="sidebar-mini-icon">
								<i class="fa fa-ellipsis-h"></i>
							</span>
							<h4 class="text-section">Menu</h4>
						</li>
						<li class="nav-item">
							<a href="/lms/classroom-registration">
								<i class="fas fa-registered"></i>
								<p>Registration</p>
							</a>
						</li>
						<!-- <li class="nav-item">
							<a href="/lms/product">
								<i class="fas fa-clock"></i>
								<p>Schedule</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="/lms/product">
								<i class="fas fa-school"></i>
								<p>Classroom</p>
							</a>
						</li> -->
					</ul>
				</div>
			</div>
		</div>
		<!-- End Sidebar -->

		<div class="main-panel">
			@yield('content')
		</div>
		<div id="preloader"></div>
	</div>
	<!--   Core JS Files   -->
	<script src="{{ URL::asset('dashboard/js/core/jquery.3.2.1.min.js') }}"></script>
	<script src="{{ URL::asset('dashboard/js/core/popper.min.js') }}"></script>
	<script src="{{ URL::asset('dashboard/js/core/bootstrap.min.js') }}"></script>
	<script src="{{ URL::asset('js/moment.min.js') }}"></script>

	<!-- jQuery UI -->
	<script src="{{ URL::asset('dashboard/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js') }}"></script>
	<script src="{{ URL::asset('dashboard/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js') }}"></script>

	<!-- jQuery Scrollbar -->
	<script src="{{ URL::asset('dashboard/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js') }}"></script>

	<!-- jQuery Sparkline -->
	<script src="{{ URL::asset('dashboard/js/plugin/jquery.sparkline/jquery.sparkline.min.js') }}"></script>

	<!-- Datatables -->
	<script src="{{ URL::asset('dashboard/js/plugin/datatables/datatables.min.js') }}"></script>

	<!-- Bootstrap Notify -->
	<script src="{{ URL::asset('dashboard/js/plugin/bootstrap-notify/bootstrap-notify.min.js') }}"></script>

	<!-- jQuery Vector Maps -->
	<script src="{{ URL::asset('dashboard/js/plugin/jqvmap/jquery.vmap.min.js') }}"></script>
	<script src="{{ URL::asset('dashboard/js/plugin/jqvmap/maps/jquery.vmap.world.js') }}"></script>

	<!-- Sweet Alert -->
	<script src="{{ URL::asset('dashboard/js/plugin/sweetalert/sweetalert.min.js') }}"></script>

	<!-- Select 2 -->
	<script src="{{ URL::asset('dashboard/js/plugin/select2/select2.full.min.js') }}"></script>

	<!-- DateTimePicker -->
	<script src="{{ URL::asset('dashboard/js/plugin/datepicker/bootstrap-datetimepicker.min.js') }}"></script>

	<!-- Bootstrap Toggle -->
	<script src="{{ URL::asset('dashboard/js/plugin/bootstrap-toggle/bootstrap-toggle.min.js') }}"></script>

		<!-- Calender -->
		<script src="{{ URL::asset('dashboard/js/plugin/fullcalendar/fullcalendar.min.js') }}"></script>

	<!-- Atlantis JS -->
	<script src="{{ URL::asset('dashboard/js/atlantis.min.js') }}"></script>	
	<script src="{{ URL::asset('js/knockout-3.5.1.min.js') }}"></script>
	<script src="{{ URL::asset('js/knockout-validation-2.0.4.min.js') }}"></script>
	<script src="{{ URL::asset('js/dashboard/dashboard.js') }}"></script>

	@yield('javascript')

</body>
</html>