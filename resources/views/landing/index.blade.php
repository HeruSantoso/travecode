@extends('landing.layouts.app')

@section('header')
<!-- ======= Header ======= -->
<header id="header" class="fixed-top">
  <div class="container d-flex align-items-center">

    <!-- <h1 class="logo mr-auto"><a href="/">TRAVECODE<span>.</span></a></h1> -->
    <!-- Uncomment below if you prefer to use an image logo -->
    <a href="/" class="logo mr-auto"><img src="{{ URL::asset('landing/img/travecode.png')}}" alt=""/></a>

    <nav class="nav-menu d-none d-lg-block">
      <ul>
        <li class="active"><a href="#hero">Home</a></li>
        <li><a href="#team">Event</a></li>
        <li><a href="#portfolio">Materi</a></li>
        <li><a href="#about">Siapa Kita</a></li>
        <li><a href="#contact">Kontak</a></li>

      </ul>
    </nav><!-- .nav-menu -->

  </div>
</header><!-- End Header -->
@endsection

@section('main')
<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex align-items-center">
  <div class="container" data-aos="zoom-out" data-aos-delay="100">
    <h1>Welcome to <span>TRAVECODE</spa>
    </h1>
    <h2>Learn, Research and Development</h2>
    <div class="d-flex">
      <a href="/lms" class="btn-get-started scrollto">LOGIN</a>
    </div>
  </div>
</section><!-- End Hero -->

<main id="main">


  <!-- ======= Portfolio Section ======= -->
  <section id="portfolio" class="portfolio">
    <div class="container aos-init aos-animate" data-aos="fade-up">

      <div class="section-title">
        <h2>Materi</h2>
        <h3>Check out <span>Materi</span></h3>
        <!-- <p>Ut possimus qui ut temporibus culpa velit eveniet modi omnis est adipisci expedita at voluptas atque vitae autem.</p> -->
      </div>

      <div data-bind="foreach: productList" class="row">
        <div class="col-lg-4 col-md-6 portfolio-item" style="left: 0px; top: 0px;">
          <img data-bind="attr:{src: picture}" class="img-fluid" alt="">
          <div class="portfolio-info">
            <h4 data-bind="text: name"></h4>
            <p class="hs-text-one-line" data-bind="text: description"></p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- ======= About Section ======= -->
  <section id="about" class="about section-bg">
    <div class="container" data-aos="fade-up">

      <div class="section-title">
        <h2>Siapa Kita</h2>
        <h3>Tentang <span>TRAVECODE</span></h3>
        <p>TraveCode dalam pelaksanaan ekstrakurikuler teknologi informasi dilaksanakan secara daring (online) menggunakan learning management system (LMS), google meet, zoom, dan Website</p>
      </div>

      <div class="row">
        <div class="col-lg-6" data-aos="zoom-out" data-aos-delay="100">
          <img src="{{ URL::asset('landing/img/about.jpg')}}" class="img-fluid" alt=""/>
        </div>
        <div class="col-lg-6 pt-4 pt-lg-0 content d-flex flex-column justify-content-center" data-aos="fade-up" data-aos-delay="100">
          <h3>VISI</h3>
          <p class="font-italic">
            Menjadi ruang pendukung ekstrakurikuler dalam bidang Teknologi Informasi, yang memiliki integritas tinggi guna mendukung kualitas kehidupan yang lebih baik.
          </p>
          <h3>MISI</h3>
          <ul>
            <li>
              <i class="bx bx-images"></i>
              <div>
                <p>Menyediakan sejumlah kegiatan yang dapat dipilih dan diikuti sesuai dengan kebutuhan, potensi, bakat, dan minat peserta didik dalam Teknologi Informasi</p>
              </div>
            </li>
            <li>
              <i class="bx bx-images"></i>
              <div>
                <p>Menyelenggarakan sejumlah kegiatan yang memberikan kesempatan kepada peserta didik untuk dapat mengekspresikan dan mengaktualisasikan diri secara optimal melalui kegiatan mandiri dan atau berkelompok</p>
              </div>
            </li>
            <li>
              <i class="bx bx-images"></i>
              <div>
                <p>Menjalin kemitraan dengan berbagai lembaga  pendidikan baik formal atau non formal, dalam dan luar negeri</p>
              </div>
            </li>
          </ul>
        </div>
      </div>

    </div>
  </section><!-- End About Section -->

  <!-- ======= Counts Section ======= -->
  <section id="counts" class="counts">
    <div class="container" data-aos="fade-up">

      <div class="row">

        <div class="col-lg-4 col-md-6">
          <div class="count-box">
            <i class="icofont-simple-smile"></i>
            <span data-toggle="counter-up">1</span>
            <p>Partners</p>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 mt-5 mt-md-0">
          <div class="count-box">
            <i class="icofont-document-folder"></i>
            <span data-toggle="counter-up">200</span>
            <p>Siswa</p>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 mt-5 mt-lg-0">
          <div class="count-box">
            <i class="icofont-live-support"></i>
            <span data-toggle="counter-up">5</span>
            <p>Facilitator</p>
          </div>
        </div>
      </div>

    </div>
  </section><!-- End Counts Section -->

  <!-- ======= Testimonials Section ======= -->
  <section id="testimonials" class="testimonials">
    <div class="container" data-aos="zoom-in">

      <div class="owl-carousel testimonials-carousel">

        <div class="testimonial-item">
          <img src="{{ URL::asset('landing/img/testimonials/testimonials-1.jpg') }}" class="testimonial-img" alt="">
          <h3>Saul Goodman</h3>
          <h4>Orangtua Siswa</h4>
          <p>
            <i class="bx bxs-quote-alt-left quote-icon-left"></i>
            Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper.
            <i class="bx bxs-quote-alt-right quote-icon-right"></i>
          </p>
        </div>

        <div class="testimonial-item">
          <img src="{{ URL::asset('landing/img/testimonials/testimonials-2.jpg') }}" class="testimonial-img" alt="">
          <h3>Sara Wilsson</h3>
          <h4>Orangtua Siswa</h4>
          <p>
            <i class="bx bxs-quote-alt-left quote-icon-left"></i>
            Export tempor illum tamen malis malis eram quae irure esse labore quem cillum quid cillum eram malis quorum velit fore eram velit sunt aliqua noster fugiat irure amet legam anim culpa.
            <i class="bx bxs-quote-alt-right quote-icon-right"></i>
          </p>
        </div>

        <div class="testimonial-item">
          <img src="{{ URL::asset('landing/img/testimonials/testimonials-3.jpg') }}" class="testimonial-img" alt="">
          <h3>Jena Karlis</h3>
          <h4>Kepala Sekolah</h4>
          <p>
            <i class="bx bxs-quote-alt-left quote-icon-left"></i>
            Enim nisi quem export duis labore cillum quae magna enim sint quorum nulla quem veniam duis minim tempor labore quem eram duis noster aute amet eram fore quis sint minim.
            <i class="bx bxs-quote-alt-right quote-icon-right"></i>
          </p>
        </div>

        <div class="testimonial-item">
          <img src="{{ URL::asset('landing/img/testimonials/testimonials-4.jpg') }}" class="testimonial-img" alt="">
          <h3>Matt Brandon</h3>
          <h4>Siswa</h4>
          <p>
            <i class="bx bxs-quote-alt-left quote-icon-left"></i>
            Fugiat enim eram quae cillum dolore dolor amet nulla culpa multos export minim fugiat minim velit minim dolor enim duis veniam ipsum anim magna sunt elit fore quem dolore labore illum veniam.
            <i class="bx bxs-quote-alt-right quote-icon-right"></i>
          </p>
        </div>

        <div class="testimonial-item">
          <img src="{{ URL::asset('landing/img/testimonials/testimonials-5.jpg') }}" class="testimonial-img" alt="">
          <h3>John Larson</h3>
          <h4>Siswa</h4>
          <p>
            <i class="bx bxs-quote-alt-left quote-icon-left"></i>
            Quis quorum aliqua sint quem legam fore sunt eram irure aliqua veniam tempor noster veniam enim culpa labore duis sunt culpa nulla illum cillum fugiat legam esse veniam culpa fore nisi cillum quid.
            <i class="bx bxs-quote-alt-right quote-icon-right"></i>
          </p>
        </div>

      </div>

    </div>
  </section><!-- End Testimonials Section -->

  <!-- ======= Contact Section ======= -->
  <section id="contact" class="contact">
    <div class="container" data-aos="fade-up">

      <div class="section-title">
        <h2>Kontak</h2>
        <h3><span>Hubungi Kita</span></h3>
      </div>

      <div class="row" data-aos="fade-up" data-aos-delay="100">

        <div class="col-lg-3 col-md-6">
          <div class="info-box  mb-4">
            <i class="bx bx-envelope"></i>
            <h3>Email</h3>
            <p>travecode@gmail.com</p>
          </div>
        </div>

        <div class="col-lg-6">
          <div class="info-box mb-4">
            <i class="bx bx-map"></i>
            <h3>Alamat</h3>
            <p>Waterfront Estate, Jalan South Travertine VII No 58, Lippo Cikarang, Bekasi, Jawa Barat</p>
          </div>
        </div>

        <div class="col-lg-3 col-md-6">
          <div class="info-box  mb-4">
            <i class="bx bx-phone-call"></i>
            <h3>Hubungi</h3>
            <p>+0744-9999</p>
          </div>
        </div>

      </div>

    </div>
  </section><!-- End Contact Section -->

  <!-- ======= Team Section ======= -->
  <section id="team" class="team section-bg">
    <div class="container" data-aos="fade-up">

      <div class="section-title">
        <h2>Event</h2>
        <h3>Digital School Competition <span>2021</span></h3>
      </div>

      <div class="row">
        <img src="{{ URL::asset('landing/img/event/digital-school-competition-2021.jpg') }}" class="img-fluid" alt="">
      </div>

      <!-- <div class="section-title">
        <h2>Team</h2>
        <h3>Our Hardworking <span>Team</span></h3>
        <p>Ut possimus qui ut temporibus culpa velit eveniet modi omnis est adipisci expedita at voluptas atque vitae autem.</p>
      </div>

      <div class="row">

        <div class="col-lg-3 col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
          <div class="member">
            <div class="member-img">
              <img src="{{ URL::asset('landing/img/team/team-1.jpg') }}" class="img-fluid" alt="">
              <div class="social">
                <a href=""><i class="icofont-twitter"></i></a>
                <a href=""><i class="icofont-facebook"></i></a>
                <a href=""><i class="icofont-instagram"></i></a>
                <a href=""><i class="icofont-linkedin"></i></a>
              </div>
            </div>
            <div class="member-info">
              <h4>Walter White</h4>
              <span>Chief Executive Officer</span>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
          <div class="member">
            <div class="member-img">
              <img src="{{ URL::asset('landing/img/team/team-2.jpg') }}" class="img-fluid" alt="">
              <div class="social">
                <a href=""><i class="icofont-twitter"></i></a>
                <a href=""><i class="icofont-facebook"></i></a>
                <a href=""><i class="icofont-instagram"></i></a>
                <a href=""><i class="icofont-linkedin"></i></a>
              </div>
            </div>
            <div class="member-info">
              <h4>Sarah Jhonson</h4>
              <span>Product Manager</span>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="300">
          <div class="member">
            <div class="member-img">
              <img src="{{ URL::asset('landing/img/team/team-3.jpg') }}" class="img-fluid" alt="">
              <div class="social">
                <a href=""><i class="icofont-twitter"></i></a>
                <a href=""><i class="icofont-facebook"></i></a>
                <a href=""><i class="icofont-instagram"></i></a>
                <a href=""><i class="icofont-linkedin"></i></a>
              </div>
            </div>
            <div class="member-info">
              <h4>William Anderson</h4>
              <span>CTO</span>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="400">
          <div class="member">
            <div class="member-img">
              <img src="{{ URL::asset('landing/img/team/team-4.jpg') }}" class="img-fluid" alt="">
              <div class="social">
                <a href=""><i class="icofont-twitter"></i></a>
                <a href=""><i class="icofont-facebook"></i></a>
                <a href=""><i class="icofont-instagram"></i></a>
                <a href=""><i class="icofont-linkedin"></i></a>
              </div>
            </div>
            <div class="member-info">
              <h4>Amanda Jepson</h4>
              <span>Accountant</span>
            </div>
          </div>
        </div>

      </div> -->

    </div>
  </section><!-- End Team Section -->

</main><!-- End #main -->
@endsection

@section('javascript')
<script src="{{ URL::asset('js/landing/landing.js') }}"></script>
@endsection