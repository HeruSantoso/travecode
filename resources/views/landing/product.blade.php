@extends('landing.layouts.app')

@section('header')
<!-- ======= Header ======= -->
<header id="header" class="fixed-top">
  <div class="container d-flex align-items-center">

    <h1 class="logo mr-auto"><a href="/">TRAVECODE<span>.</span></a></h1>
    <!-- Uncomment below if you prefer to use an image logo -->
    <!-- <a href="index.html" class="logo mr-auto"><img src="landing/img/logo.png" alt=""></a>-->

    <nav class="nav-menu d-none d-lg-block">
      <ul>
        <li><a href="/">Home</a></li>
        <li class="active"><a href="/">Product Details</a></li>
      </ul>
    </nav>
    <!-- .nav-menu -->

  </div>
</header>
<!-- End Header -->
@endsection

@section('main')
<main id="main">
  <section id="portfolio-details" class="portfolio-details">
    <input id="productId" type="hidden" value="{{ $productId }}"/>
    <div class="container">

      <h4 class="hs-title">Product Details</h4>

      <div class="row gy-4">
        <div class="col-lg-8 hs-margin">  
          <img data-bind="attr:{src: product.picture}" alt="" style="width: 100%">
        </div>

        <div class="col-lg-4 hs-margin">
          <div class="portfolio-description">
            <h2 data-bind="text: product.name"></h2>
            <p data-bind="text: product.description" style="text-align: justify;"></p>
          </div>
        </div>
      </div>

      <h4 class="hs-title">Portfolio</h4>

      <div class="row services" data-bind="foreach: portfolioList">
        <div class="col-md-6 hs-margin">
          <div class="icon-box">
            <p class="description" data-bind="text: description"></p>
          </div>
        </div>
      </div>

    </div>
  </section>
</main><!-- End #main -->
@endsection

@section('javascript')
<script src="{{ URL::asset('js/landing/product.js') }}"></script>
@endsection