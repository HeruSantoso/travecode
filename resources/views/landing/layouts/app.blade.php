<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>TRAVECODE</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{ URL::asset('landing/img/travecode-favicon.png') }}" rel="icon">
  <link href="{{ URL::asset('landing/img/apple-touch-icon.png') }}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{ URL::asset('landing/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ URL::asset('landing/vendor/icofont/icofont.min.css') }}" rel="stylesheet">
  <link href="{{ URL::asset('landing/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
  <link href="{{ URL::asset('landing/vendor/owl.carousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
  <link href="{{ URL::asset('landing/vendor/venobox/venobox.css') }}" rel="stylesheet">
  <link href="{{ URL::asset('landing/vendor/aos/aos.css') }}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{ URL::asset('landing/css/style.css') }}" rel="stylesheet">

  <!-- =======================================================
  * Template Name: BizLand - v1.2.1
  * Template URL: https://bootstrapmade.com/bizland-bootstrap-business-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>
<body>
	@yield('header')

	@yield('main')

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6 footer-contact">
            <h3>TRAVECODE<span>.</span></h3>
            <p>
              Waterfront Estate, Jalan South Travertine VII No 58<br>
              Lippo Cikarang, Bekasi, Jawa Barat<br>
              Indonesia <br><br>
              <strong>Phone:</strong> +1 5589 55488 55<br>
              <strong>Email:</strong> travecode@gmail.com<br>
            </p>
          </div>

          <div class="col-lg-4 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="/">Home</a></li>
            </ul>
          </div>

          <div class="col-lg-4 col-md-6 footer-links">
            <h4>Our Social Networks</h4>
            <p>Temukan kami di beberapa platform social media</p>
            <div class="social-links mt-3">
              <!-- <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
              <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a> -->
              <a href="https://instagram.com/travecode.id" class="instagram"><i class="bx bxl-instagram"></i></a>
              <!-- <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
              <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a> -->
            </div>
          </div>

        </div>
      </div>
    </div>
    <div class="container py-4">
      <div class="copyright">
        © Copyright <strong><span>BizLand</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/bizland-bootstrap-business-template/ -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End Footer -->

  <div id="preloader"></div>
  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="{{ URL::asset('landing/vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ URL::asset('landing/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ URL::asset('landing/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
  <script src="{{ URL::asset('landing/vendor/php-email-form/validate.js') }}"></script>
  <script src="{{ URL::asset('landing/vendor/waypoints/jquery.waypoints.min.js') }}"></script>
  <script src="{{ URL::asset('landing/vendor/counterup/counterup.min.js') }}"></script>
  <script src="{{ URL::asset('landing/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
  <script src="{{ URL::asset('landing/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
  <script src="{{ URL::asset('landing/vendor/venobox/venobox.min.js') }}"></script>
  <script src="{{ URL::asset('landing/vendor/aos/aos.js') }}"></script>

  <!-- Template Main JS File -->
  <script src="{{ URL::asset('landing/js/main.js') }}"></script>
  <script src="{{ URL::asset('js/knockout-3.5.1.min.js') }}"></script>
  <script src="{{ URL::asset('js/knockout-validation-2.0.4.min.js') }}"></script>
  @yield('javascript')

</body>
</html>