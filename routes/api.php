<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Common\Role;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\AccountController;
use App\Http\Controllers\FacilitatorController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\MaterialController;
use App\Http\Controllers\ClassroomController;
use App\Http\Controllers\ScheduleController;
use App\Http\Controllers\ClassroomRegistrationController;
use App\Http\Controllers\ParticipantController;
use App\Http\Controllers\AttedanceController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
const JWT = 'jwt.verify';

// AUTH
Route::post('login', [AuthController::class, 'login']);

// ACCOUNT
Route::post('account/student', [AccountController::class, 'createStudentAccount']);
Route::get('account/student/list', [AccountController::class, 'getStudentList'])
    -> middleware(JWT.':'.Role::multiple(array(Role::ADMIN,Role::FACILITATOR)));
Route::post('account/facilitator', [AccountController::class, 'createFacilitatorAccount'])
    -> middleware(JWT.':'.Role::multiple(array(Role::ADMIN)));
Route::get('account/facilitator/list', [AccountController::class, 'getFacilitatorList'])
    -> middleware(JWT.':'.Role::multiple(array(Role::ADMIN,Role::FACILITATOR,Role::STUDENT)));
Route::put('account/{userId}/reset', [AccountController::class, 'resetPassword'])
    -> middleware(JWT.':'.Role::multiple(array(Role::ADMIN)));

// FACILITATOR
Route::get('facilitator', [FacilitatorController::class, 'pagination']) 
    -> middleware(JWT.':'.Role::multiple(array(Role::ADMIN)));
Route::get('facilitator/{facilitatorId}', [FacilitatorController::class, 'get'])
    -> middleware(JWT.':'.Role::multiple(array(Role::ADMIN)));
Route::post('facilitator', [FacilitatorController::class, 'create']) 
    -> middleware(JWT.':'.Role::multiple(array(Role::ADMIN)));
Route::put('facilitator/{facilitatorId}', [FacilitatorController::class, 'update'])
    -> middleware(JWT.':'.Role::multiple(array(Role::ADMIN)));

// STUDENT
Route::get('student', [StudentController::class, 'pagination']) 
    -> middleware(JWT.':'.Role::multiple(array(Role::ADMIN, Role::FACILITATOR)));
Route::get('student/{studentId}', [StudentController::class, 'get'])
    -> middleware(JWT.':'.Role::multiple(array(Role::ADMIN, Role::FACILITATOR)));
Route::post('student', [StudentController::class, 'create']) 
    -> middleware(JWT.':'.Role::multiple(array(Role::ADMIN)));
Route::put('student/{studentId}', [StudentController::class, 'update'])
    -> middleware(JWT.':'.Role::multiple(array(Role::ADMIN)));

// MATERIAL
Route::get('material', [MaterialController::class, 'pagination']) 
    -> middleware(JWT.':'.Role::multiple(array(Role::ADMIN,Role::FACILITATOR,Role::STUDENT)));
Route::get('material/{productId}', [MaterialController::class, 'get'])
    -> middleware(JWT.':'.Role::multiple(array(Role::ADMIN,Role::FACILITATOR,Role::STUDENT)));
Route::post('material', [MaterialController::class, 'create']) 
    -> middleware(JWT.':'.Role::multiple(array(Role::ADMIN)));
Route::put('material/{productId}', [MaterialController::class, 'update'])
    -> middleware(JWT.':'.Role::multiple(array(Role::ADMIN)));
Route::delete('material/{productId}', [MaterialController::class, 'delete'])
    -> middleware(JWT.':'.Role::multiple(array(Role::ADMIN)));
Route::get('public/material', [MaterialController::class, 'list']);

// CLASSROOM
Route::get('classroom', [ClassroomController::class, 'pagination']) 
    -> middleware(JWT.':'.Role::multiple(array(Role::ADMIN,Role::FACILITATOR,Role::STUDENT)));
Route::get('classroom/{classroomId}', [ClassroomController::class, 'get'])
    -> middleware(JWT.':'.Role::multiple(array(Role::ADMIN,Role::FACILITATOR,Role::STUDENT)));
Route::post('classroom', [ClassroomController::class, 'create']) 
    -> middleware(JWT.':'.Role::multiple(array(Role::ADMIN)));
Route::put('classroom/{classroomId}', [ClassroomController::class, 'update'])
    -> middleware(JWT.':'.Role::multiple(array(Role::ADMIN)));
Route::delete('classroom/{classroomId}', [ClassroomController::class, 'delete'])
    -> middleware(JWT.':'.Role::multiple(array(Role::ADMIN)));
Route::get('classroom-list', [ClassroomController::class, 'list']);
Route::put('classroom/{classroomId}/open', [ClassroomController::class, 'openClassroom'])
    -> middleware(JWT.':'.Role::multiple(array(Role::ADMIN)));
Route::put('classroom/{classroomId}/close', [ClassroomController::class, 'closeClassroom'])
    -> middleware(JWT.':'.Role::multiple(array(Role::ADMIN)));

// SCHEDULE
Route::get('schedule', [ScheduleController::class, 'pagination']) 
    -> middleware(JWT.':'.Role::multiple(array(Role::ADMIN,Role::FACILITATOR,Role::STUDENT)));
Route::get('schedule/{scheduleId}', [ScheduleController::class, 'get'])
    -> middleware(JWT.':'.Role::multiple(array(Role::ADMIN,Role::FACILITATOR,Role::STUDENT)));
Route::post('schedule', [ScheduleController::class, 'create']) 
    -> middleware(JWT.':'.Role::multiple(array(Role::ADMIN)));
Route::put('schedule/{scheduleId}', [ScheduleController::class, 'update'])
    -> middleware(JWT.':'.Role::multiple(array(Role::ADMIN)));
Route::put('schedule/{scheduleId}/complete', [ScheduleController::class, 'completeSchedule'])
    -> middleware(JWT.':'.Role::multiple(array(Role::FACILITATOR)));
Route::delete('schedule/{scheduleId}', [ScheduleController::class, 'delete'])
    -> middleware(JWT.':'.Role::multiple(array(Role::ADMIN)));
Route::get('schedule-list', [ScheduleController::class, 'list']);
Route::get('schedule/calender/list', [ScheduleController::class, 'getCalender']) 
    -> middleware(JWT.':'.Role::multiple(array(Role::ADMIN,Role::FACILITATOR,Role::STUDENT)));

// Classroom Registration
Route::get('registration/classroom', [ClassroomRegistrationController::class, 'pagination']) 
    -> middleware(JWT.':'.Role::multiple(array(Role::ADMIN,Role::STUDENT)));
Route::get('registration/classroom/{classroomRegistrationId}', [ClassroomRegistrationController::class, 'get'])
    -> middleware(JWT.':'.Role::multiple(array(Role::ADMIN,Role::STUDENT)));
Route::post('registration/classroom', [ClassroomRegistrationController::class, 'create']) 
    -> middleware(JWT.':'.Role::multiple(array(Role::ADMIN,Role::STUDENT)));
Route::put('registration/classroom/{classroomRegistrationId}/pay', [ClassroomRegistrationController::class, 'pay'])
    -> middleware(JWT.':'.Role::multiple(array(Role::ADMIN,Role::STUDENT)));
Route::put('registration/classroom/{classroomRegistrationId}/reject', [ClassroomRegistrationController::class, 'reject'])
    -> middleware(JWT.':'.Role::multiple(array(Role::ADMIN)));
Route::put('registration/classroom/{classroomRegistrationId}/verify', [ClassroomRegistrationController::class, 'verify'])
    -> middleware(JWT.':'.Role::multiple(array(Role::ADMIN)));
Route::put('registration/classroom/{classroomRegistrationId}/takein', [ClassroomRegistrationController::class, 'takein'])
    -> middleware(JWT.':'.Role::multiple(array(Role::ADMIN)));

// Participant
Route::get('participant', [ParticipantController::class, 'pagination']) 
    -> middleware(JWT.':'.Role::multiple(array(Role::ADMIN,Role::FACILITATOR)));
Route::put('participant/{participantId}', [ParticipantController::class, 'updateScore'])
    -> middleware(JWT.':'.Role::multiple(array(Role::FACILITATOR)));

// Attedance
Route::get('attedance/{classroomId}', [AttedanceController::class, 'list']) 
    -> middleware(JWT.':'.Role::multiple(array(Role::ADMIN,Role::FACILITATOR)));
Route::put('attedance', [AttedanceController::class, 'attending'])
    -> middleware(JWT.':'.Role::multiple(array(Role::ADMIN,Role::FACILITATOR)));