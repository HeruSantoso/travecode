<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landing/index');
});

Route::get('/product/{productId}', function ($productId) {
    return view('landing/product',['productId' => $productId]);
});

Route::get('/lms', function () {
    return view('dashboard/index');
});

Route::get('/lms/login', function () {
    return view('dashboard/login');
});

Route::get('/lms/product', function () {
    return view('dashboard/product');
});

Route::get('/lms/product/{productId}', function ($productId) {
    return view('dashboard/productdetail',['productId' => $productId]);
});

Route::get('/lms/material', function () {
    return view('dashboard/menu/material/material');
});

Route::get('/lms/facilitator', function () {
    return view('dashboard/menu/facilitator/facilitator');
});

Route::get('/lms/student', function () {
    return view('dashboard/menu/student/student');
});

Route::get('/lms/classroom', function () {
    return view('dashboard/menu/classroom/classroom');
});

Route::get('/lms/classroom/{classroomId}', function ($classroomId) {
    return view('dashboard/menu/classroom/detail/classroom-detail',['classroomId' => $classroomId]);
});

Route::get('/lms/classroom-registration', function () {
    return view('dashboard/menu/registration/classroom/classroom-registration');
});