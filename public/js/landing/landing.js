$(document).ready(function(){
    landingViewModel.getProducts();
})

var landingViewModel = {
    productList: ko.observable([]),
    getProducts: function(){
        // $.ajax({
        //     url: `/api/public/product`,
        //     type: 'GET',
        //     headers: {
        //         'Content-Type': 'application/json',
        //     },
        //     contentType: 'application/json; charset=utf-8',
        //     success: function (data) {
        //         landingViewModel.productList(data);
        //     },
        //     error: function () {
        //         landingViewModel.productList([]);
        //     }
        // });
        var data = [
            {
                "productId" : "1",
                "name" : "Microsoft Office",
                "description": "Ms.Word, Ms.Excel, Ms.Powerpoint",
                "picture": "/img/ms-office.png",
            },
            {
                "productId" : "2",
                "name" : "Content Creator",
                "description": "Pengambilan Gambar, Perancangan Story Board, Editing Video, Youtube",
                "picture": "/img/content-creator.png"
            },
            {
                "productId" : "3",
                "name" : "Animasi",
                "description": "Animasi",
                "picture": "/img/animation.png"
            },
            {
                "productId" : "4",
                "name" : "Desain Grafis",
                "description": "Corel Draw, Corel Photo Paint",
                "picture": "/img/graphic-design.png"
            },
            {
                "productId" : "5",
                "name" : "Game Developer",
                "description": "Game Developer",
                "picture": "/img/game.png"
            },
            {
                "productId" : "6",
                "name" : "Front-End Web Developer",
                "description": "Front-End Web Developer",
                "picture": "/img/web.png"
            },
            {
                "productId" : "7",
                "name" : "Back-End Developer",
                "description": "Back-End Developer",
                "picture": "/img/web.png"
            },
            {
                "productId" : "8",
                "name" : "Android Developer",
                "description": "Android Developer",
                "picture": "/img/mobile.png"
            },
            {
                "productId" : "9",
                "name" : "IOS Developer",
                "description": "IOS Developer",
                "picture": "/img/mobile.png"
            },
        ]
        landingViewModel.productList(data);
    },
};
ko.applyBindings(landingViewModel);