$(document).ready(function(){
    productViewModel.productId = $("#productId").val();
    productViewModel.getProduct();
})

var productViewModel = {
    product: {
        name: ko.observable(),
        picture: ko.observable(),
        description: ko.observable(),
    },
    portfolioList: ko.observable([]),
    productId: ko.observable(),
    getProduct: function(){
        $.ajax({
            url: `/api/public/product/${productViewModel.productId}`,
            type: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                productViewModel.product.name(data.product.name);
                productViewModel.product.picture(data.product.picture);
                productViewModel.product.description(data.product.description);
                productViewModel.portfolioList(data.portfolioList);
            },
            error: function () {
                // window.location = "/";
            }
        });
    },
};
ko.applyBindings(productViewModel);