"use strict";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

window.onpageshow = function () {
  if ($('#preloader').length) {
    $('#preloader').delay(100).fadeOut('slow', function () {
      $(this).remove();
    });
  }

  if (localStorage.getItem('token') !== null) {
    window.location = "/lms";
  }
};

var loginViewModel = _objectSpread({}, commonViewModel, {
  email: ko.observable(),
  password: ko.observable(),
  login: function login() {
    var data = {
      email: loginViewModel.email,
      password: loginViewModel.password
    };
    $.post("/api/login", data, function (data) {
      localStorage.setItem("userId", data.user.user_id);
      localStorage.setItem("name", data.user.name);
      localStorage.setItem("email", data.user.email);
      localStorage.setItem("role", data.user.role);
      localStorage.setItem("dob", data.user.dob);
      localStorage.setItem("grade", data.user.grade);
      localStorage.setItem("token", data.token);
      window.location = "/lms";
    }).fail(function (data) {
      loginViewModel.errorHandler(data);
    });
  }
});

ko.applyBindings(loginViewModel);