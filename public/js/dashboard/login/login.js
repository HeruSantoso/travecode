window.onpageshow = function () {
    if ($('#preloader').length) {
        $('#preloader').delay(100).fadeOut('slow', function() {
            $(this).remove();
        });
    }

    if(localStorage.getItem('token') !== null){
        window.location = "/lms";
    }
};

var loginViewModel = {
    ...commonViewModel,
    email: ko.observable(),
    password: ko.observable(), 
    login: function(){
        var data = {
            email : loginViewModel.email,
            password : loginViewModel.password,
        };
        $.post("/api/login", data, function(data) {
            localStorage.setItem("userId", data.user.user_id);
            localStorage.setItem("name", data.user.name);
            localStorage.setItem("email", data.user.email);
            localStorage.setItem("role", data.user.role);
            localStorage.setItem("dob", data.user.dob);
            localStorage.setItem("grade", data.user.grade);
            localStorage.setItem("token", data.token);
            window.location = "/lms";
        })
        .fail(function(data) {
            loginViewModel.errorHandler(data);
        })
    }
};
ko.applyBindings(loginViewModel);