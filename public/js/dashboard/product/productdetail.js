var table;
$(document).ready(function(){
    productDetailViewModel.productId = $("#productId").val();
    productDetailViewModel.getProduct();
    productDetailViewModel.getPortfolioList();
});

var productDetailViewModel = {
    ...commonViewModel,
    productId : ko.observable(null),
    form : {
        name : ko.observable(null).extend({
            required: true,
        }),
        picture : ko.observable(null).extend({
            required: true,
        }),
        description : ko.observable(null).extend({
            required: true
        }),
    },
    portfolioList: ko.observable([]),
    portfolio: {
        portfolioId: ko.observable(null),
        description: ko.observable(null).extend({
            required: true,
        }),
    },
    isCreatePortfolio: false,
    getProduct: function(){
        $.ajax({
            url: `/api/product/${productDetailViewModel.productId}`,
            type: 'GET',
            headers: {
                'Authorization': 'Bearer ' + productDetailViewModel.token,
                'Content-Type': 'application/json',
            },
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                var form = productDetailViewModel.form;
                form.name(data.name);
                form.picture(data.picture);
                form.description(data.description);
            },
            error: function (data) {
                productDetailViewModel.errorHandler(data);
            }
        });
    },
    updateProduct: function(){
        if(productDetailViewModel.productErrors().length > 0){
            productDetailViewModel.productErrors.showAllMessages();
            return;
        }
        
        swal({
            title: 'Are you sure to save this data?',
            type: 'warning',
            buttons:{
                confirm: {
                    text : 'Yes, save it!',
                    className : 'btn btn-info'
                },
                cancel: {
                    visible: true,
                    className: 'btn btn-danger'
                }
            }
        }).then((save) => {
            if (save) {
                $.ajax({
                    url: `/api/product/${productDetailViewModel.productId}`,
                    type: 'PUT',
                    dataType: 'json',
                    data: ko.toJSON(productDetailViewModel.form),
                    headers: {
                        'Authorization': 'Bearer ' + productDetailViewModel.token,
                        'Content-Type': 'application/json',
                    },
                    contentType: 'application/json; charset=utf-8',
                    success: function (data) {
                        productDetailViewModel.showSuccess(data.message);
                    },
                    error: function (data) {
                        var payload = data.responseJSON;
                        productDetailViewModel.errorHandler(data, [
                            payload.errors.name,
                            payload.errors.picture,
                            payload.errors.description,
                        ]);
                    }
                });
            } else {
                swal.close();
            }
        });
    },
    getPortfolioList: function(){
        $.ajax({
            url: `/api/portfolio/product/${productDetailViewModel.productId}`,
            type: 'GET',
            headers: {
                'Authorization': 'Bearer ' + productDetailViewModel.token,
                'Content-Type': 'application/json',
            },
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                productDetailViewModel.portfolioList(data);
            },
            error: function (data) {
                productDetailViewModel.errorHandler(data);
            }
        });
    },
    createPortfolio: function(){
        if(productDetailViewModel.portfolioErrors().length > 0){
            productDetailViewModel.portfolioErrors.showAllMessages();
            return;
        }

        swal({
            title: 'Are you sure to save this data?',
            type: 'warning',
            buttons:{
                confirm: {
                    text : 'Yes, save it!',
                    className : 'btn btn-info'
                },
                cancel: {
                    visible: true,
                    className: 'btn btn-danger'
                }
            }
        }).then((save) => {
            if (save) {
                var payload = productDetailViewModel.portfolio;
                payload.productId = productDetailViewModel.productId;

                if(productDetailViewModel.isCreatePortfolio){
                    $.ajax({
                        url: `/api/portfolio`,
                        type: 'POST',
                        dataType: 'json',
                        data: ko.toJSON(payload),
                        headers: {
                            'Authorization': 'Bearer ' + productDetailViewModel.token,
                            'Content-Type': 'application/json',
                        },
                        contentType: 'application/json; charset=utf-8',
                        success: function (data) {
                            $('#addPortfolioModal').modal('toggle');
                            productDetailViewModel.getPortfolioList();
                            productDetailViewModel.showSuccess(data.message);
                        },
                        error: function (data) {
                            var payload = data.responseJSON;
                            productDetailViewModel.errorHandler(data, [payload.errors.description]);
                        }
                    });
                } else {
                    $.ajax({
                        url: `/api/portfolio/${payload.portfolioId()}`,
                        type: 'PUT',
                        dataType: 'json',
                        data: ko.toJSON(payload),
                        headers: {
                            'Authorization': 'Bearer ' + productDetailViewModel.token,
                            'Content-Type': 'application/json',
                        },
                        contentType: 'application/json; charset=utf-8',
                        success: function (data) {
                            $('#addPortfolioModal').modal('toggle');
                            productDetailViewModel.getPortfolioList();
                            productDetailViewModel.showSuccess(data.message);
                        },
                        error: function (data) {
                            var payload = data.responseJSON;
                            productDetailViewModel.errorHandler(data, [payload.errors.description]);
                        }
                    });
                }
            } else {
                swal.close();
            }
        });
    },
    deletePortfolio: function(data){
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            buttons:{
                confirm: {
                    text : 'Yes, delete it!',
                    className : 'btn btn-info'
                },
                cancel: {
                    visible: true,
                    className: 'btn btn-danger'
                }
            }
        }).then((Delete) => {
            if (Delete) {
                $.ajax({
                    url: '/api/portfolio/'+data.portfolioId,
                    type: 'DELETE',
                    dataType: 'json',
                    headers: {
                        'Authorization': 'Bearer ' + productDetailViewModel.token,
                    },
                    contentType: 'application/json; charset=utf-8',
                    success: function (data) {
                        productDetailViewModel.getPortfolioList();
                        productDetailViewModel.showSuccess(data.message);
                    },
                    error: function (data) {
                        productDetailViewModel.errorHandler(data);
                    }
                });
            } else {
                swal.close();
            }
        });
    },
    preCreatePortfolio: function(){
        productDetailViewModel.isCreatePortfolio = true;
        productDetailViewModel.portfolio.portfolioId(null);
        productDetailViewModel.portfolio.description(null);
        productDetailViewModel.portfolioErrors.showAllMessages(false);
    },
    preUpdatePortfolio: function(data){
        productDetailViewModel.isCreatePortfolio = false;
        productDetailViewModel.portfolio.portfolioId(data.portfolioId);
        productDetailViewModel.portfolio.description(data.description);
        productDetailViewModel.portfolioErrors.showAllMessages(false);
    }
};
productDetailViewModel.productErrors = ko.validation.group(productDetailViewModel.form);
productDetailViewModel.portfolioErrors = ko.validation.group(productDetailViewModel.portfolio);
ko.applyBindings(productDetailViewModel);