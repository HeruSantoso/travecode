var table;
$(document).ready(function(){
    table = $('#add-row').DataTable( {
        "processing": true,
        "serverSide": true,
        "pageLength": 5,
        "lengthChange": false,
        "ajax": {
            'url': '/api/product',
            'type': 'GET',
            'beforeSend': function (request) {
                request.setRequestHeader("Authorization", "Bearer " + localStorage.getItem("token"));
            },
            'error': function (data, error, code){
                productViewModel.errorHandler(data)
            }
        },
        "order": [[2, 'desc']],
        "columns": [
            { 
                data: "name" 
            },
            { 
                data: "created_at",
                render: function(data, type, full, meta){
                    return productViewModel.dateFormat(data);
                }
            },
            { 
                data: "updated_at",
                render: function(data, type, full, meta){
                    return productViewModel.dateFormat(data);
                }
            },
            {
                data: "productId",
                defaultContent: '',
                orderable: false,
                render: function(data, type, full, meta){
                    var div = document.createElement("div");
                    div.className = "form-button-action";

                    var btnEdit = document.createElement("a");
                    btnEdit.setAttribute('href',"/lms/product/"+data);
                    btnEdit.className = "btn btn-link btn-primary";
                    var iconEdit = document.createElement("i");
                    iconEdit.className = "fa fa-edit";
                    btnEdit.appendChild(iconEdit);

                    var btnDelete = document.createElement("button");
                    btnDelete.className = "btn btn-link btn-danger";
                    btnDelete.setAttribute('onclick',"productViewModel.deleteProduct('"+data+"')");
                    var iconDelete = document.createElement("i");
                    iconDelete.className = "fa fa-times";
                    btnDelete.appendChild(iconDelete);

                    div.appendChild(btnEdit);
                    div.appendChild(btnDelete);

                    return div.outerHTML;
                }
            }                   
        ]
    } );
  });

var productViewModel = {
    ...commonViewModel,
    form : {
        name : ko.observable(null).extend({
            required: true,
        }),
        picture : ko.observable(null).extend({
            required: true,
        }),
        description : ko.observable(null).extend({
            required: true
        }),
    },
    clearForm : function(){
        productViewModel.form.name(null);
        productViewModel.form.picture(null);
        productViewModel.form.description(null);
    },
    deleteProduct: function(data){
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            buttons:{
                confirm: {
                    text : 'Yes, delete it!',
                    className : 'btn btn-info'
                },
                cancel: {
                    visible: true,
                    className: 'btn btn-danger'
                }
            }
        }).then((Delete) => {
            if (Delete) {
                $.ajax({
                    url: '/api/product/'+data,
                    type: 'DELETE',
                    dataType: 'json',
                    headers: {
                        'Authorization': 'Bearer ' + productViewModel.token,
                    },
                    contentType: 'application/json; charset=utf-8',
                    success: function (data) {
                        table.ajax.reload();
                        productViewModel.showSuccess(data.message);
                    },
                    error: function (data) {
                        productViewModel.errorHandler(data);
                    }
                });
            } else {
                swal.close();
            }
        });
    },
    createProduct: function(){
        if(productViewModel.errors().length > 0){
            productViewModel.errors.showAllMessages();
            return;
        }
        swal({
            title: 'Are you sure to save this data?',
            type: 'warning',
            buttons:{
                confirm: {
                    text : 'Yes, save it!',
                    className : 'btn btn-info'
                },
                cancel: {
                    visible: true,
                    className: 'btn btn-danger'
                }
            }
        }).then((save) => {
            if (save) {
                $.ajax({
                    url: '/api/product',
                    type: 'POST',
                    dataType: 'json',
                    data: ko.toJSON(productViewModel.form),
                    headers: {
                        'Authorization': 'Bearer ' + productViewModel.token,
                        'Content-Type': 'application/json',
                    },
                    contentType: 'application/json; charset=utf-8',
                    success: function (data) {
                        productViewModel.clearForm();

                        $('#addRowModal').modal('toggle');
                        table.ajax.reload();
                        productViewModel.showSuccess(data.message);
                    },
                    error: function (data) {
                        var payload = data.responseJSON;
                        productViewModel.errorHandler(data, [
                            payload.errors.name,
                            payload.errors.picture,
                            payload.errors.description,
                        ]);
                    }
                });
            } else {
                productViewModel.clearForm();
                swal.close();
            }
        });
    }
};

productViewModel.errors = ko.validation.group(productViewModel.form);
ko.applyBindings(productViewModel);