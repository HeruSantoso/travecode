"use strict";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var table;
$(document).ready(function () {
  cRegistrationViewModel.init();
});

var cRegistrationViewModel = _objectSpread({}, commonViewModel, {
  init: function init() {
    table = $('#add-row').DataTable({
      "processing": true,
      "serverSide": true,
      "pageLength": 5,
      "lengthChange": false,
      "ajax": {
        'url': '/api/registration/classroom',
        'type': 'GET',
        'beforeSend': function beforeSend(request) {
          request.setRequestHeader("Authorization", "Bearer " + localStorage.getItem("token"));
        },
        'error': function error(data, _error, code) {
          cRegistrationViewModel.errorHandler(data);
        }
      },
      "order": [[4, 'desc']],
      "columns": [{
        data: "student_name"
      }, // { 
      //     data: "email" 
      // },
      {
        data: "phone_number"
      }, {
        data: "classroom_name"
      }, {
        data: "status",
        orderable: false,
        render: function render(data, type, full, meta) {
          var className = '';
          var text = '';

          if (data === 1) {
            text = "Registered";
            className = "badge badge-warning";
          } else if (data === 2) {
            text = "Paid";
            className = "badge badge-success";
          } else if (data === 3) {
            text = "Verified";
            className = "badge badge-info";
          } else if (data === 4) {
            text = "Joined";
            className = "badge badge-secondary";
          } else {
            text = "Rejected";
            className = "badge badge-danger";
          }

          var span = document.createElement("span");
          span.className = className;
          var text = document.createTextNode(text);
          span.appendChild(text);
          return span.outerHTML;
        }
      }, // { 
      //     data: "created_at",
      //     render: function(data, type, full, meta){
      //         return cRegistrationViewModel.dateFormat(data);
      //     }                
      // },
      {
        data: "updated_at",
        render: function render(data, type, full, meta) {
          return cRegistrationViewModel.dateFormat(data);
        }
      }, {
        data: "classroom_registration_id",
        defaultContent: '',
        orderable: false,
        render: function render(data, type, full, meta) {
          var div = document.createElement("div");
          div.className = "form-button-action";
          var btnPay = document.createElement("a");
          btnPay.className = "btn btn-link btn-primary";
          btnPay.setAttribute('onclick', "cRegistrationViewModel.openPayModal('" + data + "')");
          btnPay.setAttribute('data-toggle', "tooltip");
          btnPay.setAttribute('data-placement', "top");
          btnPay.setAttribute('title', "Pay");
          var iconPay = document.createElement("i");
          iconPay.className = "fa fa-money-check-alt";
          btnPay.appendChild(iconPay);
          var btnVerify = document.createElement("a");
          btnVerify.className = "btn btn-link btn-success";
          btnVerify.setAttribute('onclick', "cRegistrationViewModel.verify('" + data + "')");
          btnVerify.setAttribute('data-toggle', "tooltip");
          btnVerify.setAttribute('data-placement', "top");
          btnVerify.setAttribute('title', "Verify");
          var iconVerify = document.createElement("i");
          iconVerify.className = "fa fa-check-circle";
          btnVerify.appendChild(iconVerify);
          var btnReject = document.createElement("button");
          btnReject.className = "btn btn-link btn-danger";
          btnReject.setAttribute('onclick', "cRegistrationViewModel.openRejectModal('" + data + "')");
          btnReject.setAttribute('data-toggle', "tooltip");
          btnReject.setAttribute('data-placement', "top");
          btnReject.setAttribute('title', "Reject");
          var iconReject = document.createElement("i");
          iconReject.className = "fa fa-window-close";
          btnReject.appendChild(iconReject);
          var btnTakeIn = document.createElement("button");
          btnTakeIn.className = "btn btn-link btn-secondary";
          btnTakeIn.setAttribute('onclick', "cRegistrationViewModel.takeIn('" + data + "')");
          btnTakeIn.setAttribute('data-toggle', "tooltip");
          btnTakeIn.setAttribute('data-placement', "top");
          btnTakeIn.setAttribute('title', "Take In");
          var iconTakeIn = document.createElement("i");
          iconTakeIn.className = "fa fa-door-open";
          btnTakeIn.appendChild(iconTakeIn);

          if (full.status === 0 || full.status === 1) {
            div.appendChild(btnPay);
          } else if (full.status === 2 && cRegistrationViewModel.isAdmin()) {
            div.appendChild(btnVerify);
            div.appendChild(btnReject);
          } else if (full.status === 3 && cRegistrationViewModel.isAdmin()) {
            div.appendChild(btnTakeIn);
          }

          return div.outerHTML;
        }
      }]
    });
    cRegistrationViewModel.getClassroomList();
    cRegistrationViewModel.getStudentList();
  },
  modeCreate: function modeCreate() {
    cRegistrationViewModel.page.isEdit(false);
    cRegistrationViewModel.page.title("Classroom");
    cRegistrationViewModel.page.description("Register classroom using this form, make sure you fill them all");
  },
  modeUpdate: function modeUpdate() {
    cRegistrationViewModel.page.isEdit(true);
    cRegistrationViewModel.page.title("Update");
    cRegistrationViewModel.page.description("Update material using this form, make sure you fill them all");
  },
  form: {
    classroom_registration_id: ko.observable(null).extend(),
    student: ko.observable(null).extend({
      required: true
    }),
    classroom: ko.observable(null).extend({
      required: true
    })
  },
  payForm: {
    evidence_of_payment: ko.observable(null).extend({
      required: true
    })
  },
  rejectForm: {
    rejected_reason: ko.observable(null).extend({
      required: true
    })
  },
  page: {
    isEdit: ko.observable(false),
    title: ko.observable(null),
    description: ko.observable(null)
  },
  classroomList: ko.observableArray([]),
  studentList: ko.observableArray([]),
  clearForm: function clearForm() {
    cRegistrationViewModel.form.classroom_registration_id(null);
    cRegistrationViewModel.form.student(null);
    cRegistrationViewModel.form.classroom(null);
    cRegistrationViewModel.refreshSelect2();
    cRegistrationViewModel.errors.showAllMessages(false);
  },
  openRegistrationModal: function openRegistrationModal() {
    cRegistrationViewModel.clearForm();
    cRegistrationViewModel.modeCreate();
    $('#registrationModal').modal('show');
  },
  closeModal: function closeModal() {
    cRegistrationViewModel.clearForm();
    $('#registrationModal').modal('toggle');
  },
  clearPayForm: function clearPayForm() {
    cRegistrationViewModel.form.classroom_registration_id(null);
    cRegistrationViewModel.payForm.evidence_of_payment(null);
    cRegistrationViewModel.errors.showAllMessages(false);
  },
  openPayModal: function openPayModal(classroomRegistrationId) {
    cRegistrationViewModel.clearPayForm();
    cRegistrationViewModel.form.classroom_registration_id(classroomRegistrationId);
    $('#payModal').modal('show');
  },
  closePayModal: function closePayModal() {
    cRegistrationViewModel.clearForm();
    $('#payModal').modal('toggle');
  },
  clearRejectForm: function clearRejectForm() {
    cRegistrationViewModel.form.classroom_registration_id(null);
    cRegistrationViewModel.rejectForm.rejected_reason(null);
    cRegistrationViewModel.errors.showAllMessages(false);
  },
  openRejectModal: function openRejectModal(classroomRegistrationId) {
    cRegistrationViewModel.clearRejectForm();
    cRegistrationViewModel.form.classroom_registration_id(classroomRegistrationId);
    $('#rejectModal').modal('show');
  },
  closeRejectModal: function closeRejectModal() {
    cRegistrationViewModel.clearForm();
    $('#rejectModal').modal('toggle');
  },
  classroomRegistration: function classroomRegistration() {
    if (cRegistrationViewModel.errors().length > 0) {
      cRegistrationViewModel.errors.showAllMessages();
      return;
    }

    var data = {
      "student_id": cRegistrationViewModel.form.student().user_id,
      "classroom_id": cRegistrationViewModel.form.classroom().classroom_id
    };
    swal({
      title: 'Are you sure to register to this classroom?',
      icon: 'warning',
      buttons: {
        confirm: {
          text: 'Yes, register it!',
          className: 'btn btn-info'
        },
        cancel: {
          visible: true,
          className: 'btn btn-danger'
        }
      }
    }).then(function (save) {
      if (save) {
        $.ajax({
          url: '/api/registration/classroom',
          type: 'POST',
          dataType: 'json',
          data: JSON.stringify(data),
          headers: {
            'Authorization': 'Bearer ' + cRegistrationViewModel.token,
            'Content-Type': 'application/json'
          },
          contentType: 'application/json; charset=utf-8',
          success: function success(data) {
            cRegistrationViewModel.closeModal();
            table.ajax.reload();
            cRegistrationViewModel.showSuccess(data.message);
          },
          error: function error(data) {
            var payload = data.responseJSON;

            if (!payload.errors) {
              cRegistrationViewModel.errorHandler(data);
            } else {
              cRegistrationViewModel.errorHandler(data, [payload.errors.student_id, payload.errors.classroom_id]);
            }
          }
        });
      } else {
        cRegistrationViewModel.clearForm();
        swal.close();
      }
    });
  },
  getClassroomList: function getClassroomList() {
    $.ajax({
      url: '/api/classroom-list',
      type: 'GET',
      dataType: 'json',
      headers: {
        'Authorization': 'Bearer ' + cRegistrationViewModel.token
      },
      contentType: 'application/json; charset=utf-8',
      success: function success(data) {
        var classroomList = data.data;
        cRegistrationViewModel.classroomList(classroomList);
      },
      error: function error(data) {
        cRegistrationViewModel.errorHandler(data);
      }
    });
  },
  getStudentList: function getStudentList() {
    if (cRegistrationViewModel.isAdmin()) {
      $.ajax({
        url: '/api/account/student/list',
        type: 'GET',
        dataType: 'json',
        headers: {
          'Authorization': 'Bearer ' + cRegistrationViewModel.token
        },
        contentType: 'application/json; charset=utf-8',
        success: function success(data) {
          var students = data.data;
          cRegistrationViewModel.studentList(students);
        },
        error: function error(data) {
          cRegistrationViewModel.errorHandler(data);
        }
      });
    } else {
      var studentList = [{
        user_id: cRegistrationViewModel.userId,
        name: cRegistrationViewModel.name
      }];
      cRegistrationViewModel.studentList(studentList);
    }
  },
  pay: function pay() {
    if (cRegistrationViewModel.payErrors().length > 0) {
      cRegistrationViewModel.payErrors.showAllMessages();
      return;
    }

    var data = {
      "evidence_of_payment": cRegistrationViewModel.payForm.evidence_of_payment()
    };
    swal({
      title: 'Are you sure to pay this registration?',
      icon: 'warning',
      buttons: {
        confirm: {
          text: 'Yes, pay it!',
          className: 'btn btn-info'
        },
        cancel: {
          visible: true,
          className: 'btn btn-danger'
        }
      }
    }).then(function (save) {
      if (save) {
        $.ajax({
          url: "/api/registration/classroom/".concat(cRegistrationViewModel.form.classroom_registration_id(), "/pay"),
          type: 'PUT',
          dataType: 'json',
          data: JSON.stringify(data),
          headers: {
            'Authorization': 'Bearer ' + cRegistrationViewModel.token,
            'Content-Type': 'application/json'
          },
          contentType: 'application/json; charset=utf-8',
          success: function success(data) {
            cRegistrationViewModel.closePayModal();
            table.ajax.reload();
            cRegistrationViewModel.showSuccess(data.message);
          },
          error: function error(data) {
            var payload = data.responseJSON;

            if (!payload.errors) {
              cRegistrationViewModel.errorHandler(data);
            } else {
              cRegistrationViewModel.errorHandler(data, [payload.payErrors.evidence_of_payment]);
            }
          }
        });
      } else {
        cRegistrationViewModel.clearPayForm();
        swal.close();
      }
    });
  },
  reject: function reject() {
    if (cRegistrationViewModel.rejectErrors().length > 0) {
      cRegistrationViewModel.rejectErrors.showAllMessages();
      return;
    }

    var data = {
      "rejected_reason": cRegistrationViewModel.rejectForm.rejected_reason()
    };
    swal({
      title: 'Are you sure to reject this registration?',
      icon: 'warning',
      buttons: {
        confirm: {
          text: 'Yes, reject it!',
          className: 'btn btn-info'
        },
        cancel: {
          visible: true,
          className: 'btn btn-danger'
        }
      }
    }).then(function (save) {
      if (save) {
        $.ajax({
          url: "/api/registration/classroom/".concat(cRegistrationViewModel.form.classroom_registration_id(), "/reject"),
          type: 'PUT',
          dataType: 'json',
          data: JSON.stringify(data),
          headers: {
            'Authorization': 'Bearer ' + cRegistrationViewModel.token,
            'Content-Type': 'application/json'
          },
          contentType: 'application/json; charset=utf-8',
          success: function success(data) {
            cRegistrationViewModel.closeRejectModal();
            table.ajax.reload();
            cRegistrationViewModel.showSuccess(data.message);
          },
          error: function error(data) {
            var payload = data.responseJSON;

            if (!payload.errors) {
              cRegistrationViewModel.errorHandler(data);
            } else {
              cRegistrationViewModel.errorHandler(data, [payload.rejectErrors.rejected_reason]);
            }
          }
        });
      } else {
        cRegistrationViewModel.clearRejectForm();
        swal.close();
      }
    });
  },
  verify: function verify(classroomRegistrationId) {
    swal({
      title: 'Are you sure to verify this registration?',
      icon: 'warning',
      buttons: {
        confirm: {
          text: 'Yes, verify it!',
          className: 'btn btn-info'
        },
        cancel: {
          visible: true,
          className: 'btn btn-danger'
        }
      }
    }).then(function (save) {
      if (save) {
        $.ajax({
          url: "/api/registration/classroom/".concat(classroomRegistrationId, "/verify"),
          type: 'PUT',
          dataType: 'json',
          headers: {
            'Authorization': 'Bearer ' + cRegistrationViewModel.token,
            'Content-Type': 'application/json'
          },
          contentType: 'application/json; charset=utf-8',
          success: function success(data) {
            table.ajax.reload();
            cRegistrationViewModel.showSuccess(data.message);
          },
          error: function error(data) {
            cRegistrationViewModel.errorHandler(data);
          }
        });
      } else {
        swal.close();
      }
    });
  },
  takeIn: function takeIn(classroomRegistrationId) {
    swal({
      title: 'Are you sure to take in this student to the classroom?',
      icon: 'warning',
      buttons: {
        confirm: {
          text: 'Yes, take in it!',
          className: 'btn btn-info'
        },
        cancel: {
          visible: true,
          className: 'btn btn-danger'
        }
      }
    }).then(function (save) {
      if (save) {
        $.ajax({
          url: "/api/registration/classroom/".concat(classroomRegistrationId, "/takein"),
          type: 'PUT',
          dataType: 'json',
          headers: {
            'Authorization': 'Bearer ' + cRegistrationViewModel.token,
            'Content-Type': 'application/json'
          },
          contentType: 'application/json; charset=utf-8',
          success: function success(data) {
            table.ajax.reload();
            cRegistrationViewModel.showSuccess(data.message);
          },
          error: function error(data) {
            cRegistrationViewModel.errorHandler(data);
          }
        });
      } else {
        swal.close();
      }
    });
  }
});

cRegistrationViewModel.errors = ko.validation.group(cRegistrationViewModel.form);
cRegistrationViewModel.payErrors = ko.validation.group(cRegistrationViewModel.payForm);
cRegistrationViewModel.rejectErrors = ko.validation.group(cRegistrationViewModel.rejectForm);
ko.applyBindings(cRegistrationViewModel);