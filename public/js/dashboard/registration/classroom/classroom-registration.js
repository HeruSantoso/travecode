var table;
$(document).ready(function(){
    cRegistrationViewModel.init();    
});

var cRegistrationViewModel = {
    ...commonViewModel,
    init: function(){
        table = $('#add-row').DataTable( {
            "processing": true,
            "serverSide": true,
            "pageLength": 5,
            "lengthChange": false,
            "ajax": {
                'url': '/api/registration/classroom',
                'type': 'GET',
                'beforeSend': function (request) {
                    request.setRequestHeader("Authorization", "Bearer " + localStorage.getItem("token"));
                },
                'error': function (data, error, code){
                    cRegistrationViewModel.errorHandler(data)
                }
            },
            "order": [[4, 'desc']],
            "columns": [
                { 
                    data: "student_name" 
                },
                // { 
                //     data: "email" 
                // },
                { 
                    data: "phone_number" 
                },
                { 
                    data: "classroom_name" 
                },
                { 
                    data: "status",
                    orderable: false,
                    render: function(data, type, full, meta){
                        var className = '';
                        var text = '';
                        if(data === 1){
                            text = "Registered"
                            className = "badge badge-warning";
                        } else if(data === 2){
                            text = "Paid"
                            className = "badge badge-success";
                        } else if(data === 3){
                            text = "Verified"
                            className = "badge badge-info";
                        } else if(data === 4){
                            text = "Joined"
                            className = "badge badge-secondary";
                        } else {
                            text = "Rejected"
                            className = "badge badge-danger";
                        } 

                        var span = document.createElement("span");
                        span.className = className;

                        var text = document.createTextNode(text);
                        span.appendChild(text);

                        return span.outerHTML;
                    }
                },
                // { 
                //     data: "created_at",
                //     render: function(data, type, full, meta){
                //         return cRegistrationViewModel.dateFormat(data);
                //     }                
                // },
                { 
                    data: "updated_at",
                    render: function(data, type, full, meta){
                        return cRegistrationViewModel.dateFormat(data);
                    }
                },
                {
                    data: "classroom_registration_id",
                    defaultContent: '',
                    orderable: false,
                    render: function(data, type, full, meta){
                        var div = document.createElement("div");
                        div.className = "form-button-action";
   
                        var btnPay = document.createElement("a");
                        btnPay.className = "btn btn-link btn-primary";
                        btnPay.setAttribute('onclick',"cRegistrationViewModel.openPayModal('"+data+"')");
                        btnPay.setAttribute('data-toggle',"tooltip");
                        btnPay.setAttribute('data-placement',"top");
                        btnPay.setAttribute('title',"Pay");
                        var iconPay = document.createElement("i");
                        iconPay.className = "fa fa-money-check-alt";
                        btnPay.appendChild(iconPay);

                        var btnVerify = document.createElement("a");
                        btnVerify.className = "btn btn-link btn-success";
                        btnVerify.setAttribute('onclick',"cRegistrationViewModel.verify('"+data+"')");
                        btnVerify.setAttribute('data-toggle',"tooltip");
                        btnVerify.setAttribute('data-placement',"top");
                        btnVerify.setAttribute('title',"Verify");
                        var iconVerify = document.createElement("i");
                        iconVerify.className = "fa fa-check-circle";
                        btnVerify.appendChild(iconVerify);
    
                        var btnReject = document.createElement("button");
                        btnReject.className = "btn btn-link btn-danger";
                        btnReject.setAttribute('onclick',"cRegistrationViewModel.openRejectModal('"+data+"')");
                        btnReject.setAttribute('data-toggle',"tooltip");
                        btnReject.setAttribute('data-placement',"top");
                        btnReject.setAttribute('title',"Reject");
                        var iconReject = document.createElement("i");
                        iconReject.className = "fa fa-window-close";
                        btnReject.appendChild(iconReject);

                        var btnTakeIn = document.createElement("button");
                        btnTakeIn.className = "btn btn-link btn-secondary";
                        btnTakeIn.setAttribute('onclick',"cRegistrationViewModel.takeIn('"+data+"')");
                        btnTakeIn.setAttribute('data-toggle',"tooltip");
                        btnTakeIn.setAttribute('data-placement',"top");
                        btnTakeIn.setAttribute('title',"Take In");
                        var iconTakeIn = document.createElement("i");
                        iconTakeIn.className = "fa fa-door-open";
                        btnTakeIn.appendChild(iconTakeIn);

                        if (full.status === 0 || full.status === 1) {
                            div.appendChild(btnPay);
                        } else if (full.status === 2 && cRegistrationViewModel.isAdmin()) {
                            div.appendChild(btnVerify);
                            div.appendChild(btnReject);
                        } else if (full.status === 3 && cRegistrationViewModel.isAdmin()) {
                            div.appendChild(btnTakeIn);
                        }
    
                        return div.outerHTML;
                    }
                }                   
            ]
        } );
        cRegistrationViewModel.getClassroomList();
        cRegistrationViewModel.getStudentList();
    },
    modeCreate: function(){
        cRegistrationViewModel.page.isEdit(false);
        cRegistrationViewModel.page.title("Classroom");
        cRegistrationViewModel.page.description("Register classroom using this form, make sure you fill them all");
    },
    modeUpdate: function(){
        cRegistrationViewModel.page.isEdit(true);
        cRegistrationViewModel.page.title("Update");
        cRegistrationViewModel.page.description("Update material using this form, make sure you fill them all");
    },
    form : {
        classroom_registration_id : ko.observable(null).extend(),
        student : ko.observable(null).extend({
            required: true,
        }),
        classroom : ko.observable(null).extend({
            required: true,
        }),
    },
    payForm : {
        evidence_of_payment : ko.observable(null).extend({
            required: true,
        }),
    },
    rejectForm : {
        rejected_reason : ko.observable(null).extend({
            required: true,
        }),
    },
    page: {
        isEdit : ko.observable(false),
        title: ko.observable(null),
        description: ko.observable(null),
    },
    classroomList: ko.observableArray([]),
    studentList: ko.observableArray([]),
    clearForm : function(){
        cRegistrationViewModel.form.classroom_registration_id(null);
        cRegistrationViewModel.form.student(null);
        cRegistrationViewModel.form.classroom(null);

        cRegistrationViewModel.refreshSelect2();

        cRegistrationViewModel.errors.showAllMessages(false);
    },
    openRegistrationModal: function(){
        cRegistrationViewModel.clearForm();
        cRegistrationViewModel.modeCreate();
        $('#registrationModal').modal('show');
    },
    closeModal: function(){
        cRegistrationViewModel.clearForm();
        $('#registrationModal').modal('toggle');
    },
    clearPayForm : function(){
        cRegistrationViewModel.form.classroom_registration_id(null);
        cRegistrationViewModel.payForm.evidence_of_payment(null);

        cRegistrationViewModel.errors.showAllMessages(false);
    },
    openPayModal: function(classroomRegistrationId){
        cRegistrationViewModel.clearPayForm();
        cRegistrationViewModel.form.classroom_registration_id(classroomRegistrationId);
        $('#payModal').modal('show');
    },
    closePayModal: function(){
        cRegistrationViewModel.clearForm();
        $('#payModal').modal('toggle');
    },
    clearRejectForm : function(){
        cRegistrationViewModel.form.classroom_registration_id(null);
        cRegistrationViewModel.rejectForm.rejected_reason(null);

        cRegistrationViewModel.errors.showAllMessages(false);
    },
    openRejectModal: function(classroomRegistrationId){
        cRegistrationViewModel.clearRejectForm();
        cRegistrationViewModel.form.classroom_registration_id(classroomRegistrationId);
        $('#rejectModal').modal('show');
    },
    closeRejectModal: function(){
        cRegistrationViewModel.clearForm();
        $('#rejectModal').modal('toggle');
    },
    classroomRegistration: function(){
        if(cRegistrationViewModel.errors().length > 0){
            cRegistrationViewModel.errors.showAllMessages();
            return;
        }

        var data = {
            "student_id": cRegistrationViewModel.form.student().user_id,
            "classroom_id": cRegistrationViewModel.form.classroom().classroom_id,
        }

        swal({
            title: 'Are you sure to register to this classroom?',
            icon: 'warning',
            buttons:{
                confirm: {
                    text : 'Yes, register it!',
                    className : 'btn btn-info'
                },
                cancel: {
                    visible: true,
                    className: 'btn btn-danger'
                }
            }
        }).then((save) => {
            if (save) {
                $.ajax({
                    url: '/api/registration/classroom',
                    type: 'POST',
                    dataType: 'json',
                    data: JSON.stringify(data),
                    headers: {
                        'Authorization': 'Bearer ' + cRegistrationViewModel.token,
                        'Content-Type': 'application/json',
                    },
                    contentType: 'application/json; charset=utf-8',
                    success: function (data) {
                        cRegistrationViewModel.closeModal();
                        table.ajax.reload();
                        cRegistrationViewModel.showSuccess(data.message);
                    },
                    error: function (data) {
                        var payload = data.responseJSON;
                        if (!payload.errors) {
                            cRegistrationViewModel.errorHandler(data);
                        } else {
                            cRegistrationViewModel.errorHandler(data, [
                                payload.errors.student_id,
                                payload.errors.classroom_id,
                            ]);
                        }
                    }
                });
            } else {
                cRegistrationViewModel.clearForm();
                swal.close();
            }
        });
    },
    getClassroomList: function(){

        $.ajax({
            url: '/api/classroom-list',
            type: 'GET',
            dataType: 'json',
            headers: {
                'Authorization': 'Bearer ' + cRegistrationViewModel.token,
            },
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                var classroomList = data.data;
                cRegistrationViewModel.classroomList(classroomList);
            },
            error: function (data) {
                cRegistrationViewModel.errorHandler(data);
            }
        });
    },
    getStudentList: function(){
        if (cRegistrationViewModel.isAdmin()) {
            $.ajax({
                url: '/api/account/student/list',
                type: 'GET',
                dataType: 'json',
                headers: {
                    'Authorization': 'Bearer ' + cRegistrationViewModel.token,
                },
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    var students = data.data;
                    cRegistrationViewModel.studentList(students);
                },
                error: function (data) {
                    cRegistrationViewModel.errorHandler(data);
                }
            });
        } else {
            var studentList = [
                {
                    user_id : cRegistrationViewModel.userId,
                    name : cRegistrationViewModel.name,
                }
            ]
            cRegistrationViewModel.studentList(studentList);
        }
    },
    pay: function(){
        if(cRegistrationViewModel.payErrors().length > 0){
            cRegistrationViewModel.payErrors.showAllMessages();
            return;
        }

        var data = {
            "evidence_of_payment": cRegistrationViewModel.payForm.evidence_of_payment(),
        }

        swal({
            title: 'Are you sure to pay this registration?',
            icon: 'warning',
            buttons:{
                confirm: {
                    text : 'Yes, pay it!',
                    className : 'btn btn-info'
                },
                cancel: {
                    visible: true,
                    className: 'btn btn-danger'
                }
            }
        }).then((save) => {
            if (save) {
                $.ajax({
                    url: `/api/registration/classroom/${cRegistrationViewModel.form.classroom_registration_id()}/pay`,
                    type: 'PUT',
                    dataType: 'json',
                    data: JSON.stringify(data),
                    headers: {
                        'Authorization': 'Bearer ' + cRegistrationViewModel.token,
                        'Content-Type': 'application/json',
                    },
                    contentType: 'application/json; charset=utf-8',
                    success: function (data) {
                        cRegistrationViewModel.closePayModal();
                        table.ajax.reload();
                        cRegistrationViewModel.showSuccess(data.message);
                    },
                    error: function (data) {
                        var payload = data.responseJSON;
                        if (!payload.errors) {
                            cRegistrationViewModel.errorHandler(data);
                        } else {
                            cRegistrationViewModel.errorHandler(data, [
                                payload.payErrors.evidence_of_payment
                            ]);
                        }
                    }
                });
            } else {
                cRegistrationViewModel.clearPayForm();
                swal.close();
            }
        });
    },
    reject: function(){
        if(cRegistrationViewModel.rejectErrors().length > 0){
            cRegistrationViewModel.rejectErrors.showAllMessages();
            return;
        }

        var data = {
            "rejected_reason": cRegistrationViewModel.rejectForm.rejected_reason(),
        }

        swal({
            title: 'Are you sure to reject this registration?',
            icon: 'warning',
            buttons:{
                confirm: {
                    text : 'Yes, reject it!',
                    className : 'btn btn-info'
                },
                cancel: {
                    visible: true,
                    className: 'btn btn-danger'
                }
            }
        }).then((save) => {
            if (save) {
                $.ajax({
                    url: `/api/registration/classroom/${cRegistrationViewModel.form.classroom_registration_id()}/reject`,
                    type: 'PUT',
                    dataType: 'json',
                    data: JSON.stringify(data),
                    headers: {
                        'Authorization': 'Bearer ' + cRegistrationViewModel.token,
                        'Content-Type': 'application/json',
                    },
                    contentType: 'application/json; charset=utf-8',
                    success: function (data) {
                        cRegistrationViewModel.closeRejectModal();
                        table.ajax.reload();
                        cRegistrationViewModel.showSuccess(data.message);
                    },
                    error: function (data) {
                        var payload = data.responseJSON;
                        if (!payload.errors) {
                            cRegistrationViewModel.errorHandler(data);
                        } else {
                            cRegistrationViewModel.errorHandler(data, [
                                payload.rejectErrors.rejected_reason
                            ]);
                        }
                    }
                });
            } else {
                cRegistrationViewModel.clearRejectForm();
                swal.close();
            }
        });
    },
    verify: function(classroomRegistrationId){
        swal({
            title: 'Are you sure to verify this registration?',
            icon: 'warning',
            buttons:{
                confirm: {
                    text : 'Yes, verify it!',
                    className : 'btn btn-info'
                },
                cancel: {
                    visible: true,
                    className: 'btn btn-danger'
                }
            }
        }).then((save) => {
            if (save) {
                $.ajax({
                    url: `/api/registration/classroom/${classroomRegistrationId}/verify`,
                    type: 'PUT',
                    dataType: 'json',
                    headers: {
                        'Authorization': 'Bearer ' + cRegistrationViewModel.token,
                        'Content-Type': 'application/json',
                    },
                    contentType: 'application/json; charset=utf-8',
                    success: function (data) {
                        table.ajax.reload();
                        cRegistrationViewModel.showSuccess(data.message);
                    },
                    error: function (data) {
                        cRegistrationViewModel.errorHandler(data);
                    }
                });
            } else {
                swal.close();
            }
        });
    },
    takeIn: function(classroomRegistrationId){
        swal({
            title: 'Are you sure to take in this student to the classroom?',
            icon: 'warning',
            buttons:{
                confirm: {
                    text : 'Yes, take in it!',
                    className : 'btn btn-info'
                },
                cancel: {
                    visible: true,
                    className: 'btn btn-danger'
                }
            }
        }).then((save) => {
            if (save) {
                $.ajax({
                    url: `/api/registration/classroom/${classroomRegistrationId}/takein`,
                    type: 'PUT',
                    dataType: 'json',
                    headers: {
                        'Authorization': 'Bearer ' + cRegistrationViewModel.token,
                        'Content-Type': 'application/json',
                    },
                    contentType: 'application/json; charset=utf-8',
                    success: function (data) {
                        table.ajax.reload();
                        cRegistrationViewModel.showSuccess(data.message);
                    },
                    error: function (data) {
                        cRegistrationViewModel.errorHandler(data);
                    }
                });
            } else {
                swal.close();
            }
        });
    }
};

cRegistrationViewModel.errors = ko.validation.group(cRegistrationViewModel.form);
cRegistrationViewModel.payErrors = ko.validation.group(cRegistrationViewModel.payForm);
cRegistrationViewModel.rejectErrors = ko.validation.group(cRegistrationViewModel.rejectForm);
ko.applyBindings(cRegistrationViewModel);