"use strict";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var table;
$(document).ready(function () {
  materialViewModel.init();
});

var materialViewModel = _objectSpread({}, commonViewModel, {
  init: function init() {
    table = $('#add-row').DataTable({
      "processing": true,
      "serverSide": true,
      "pageLength": 5,
      "lengthChange": false,
      "ajax": {
        'url': '/api/material',
        'type': 'GET',
        'beforeSend': function beforeSend(request) {
          request.setRequestHeader("Authorization", "Bearer " + localStorage.getItem("token"));
        },
        'error': function error(data, _error, code) {
          materialViewModel.errorHandler(data);
        }
      },
      "order": [[4, 'desc']],
      "columns": [{
        data: "name"
      }, {
        data: "number_of_meetings",
        render: function render(data, type, full, meta) {
          return full.number_of_meetings + "x " + full.unit_of_meeting;
        }
      }, {
        data: "min_quota"
      }, {
        data: "created_at",
        render: function render(data, type, full, meta) {
          return materialViewModel.dateFormat(data);
        }
      }, {
        data: "updated_at",
        render: function render(data, type, full, meta) {
          return materialViewModel.dateFormat(data);
        }
      }, {
        data: "material_id",
        defaultContent: '',
        orderable: false,
        render: function render(data, type, full, meta) {
          var div = document.createElement("div");
          div.className = "form-button-action";
          var btnEdit = document.createElement("a");
          btnEdit.className = "btn btn-link btn-primary";
          btnEdit.setAttribute('onclick', "materialViewModel.openModal('" + data + "')");
          var iconEdit = document.createElement("i");
          iconEdit.className = "fa fa-edit";
          btnEdit.appendChild(iconEdit);
          var btnDelete = document.createElement("button");
          btnDelete.className = "btn btn-link btn-danger";
          btnDelete.setAttribute('onclick', "materialViewModel.deleteMaterial('" + data + "')");
          var iconDelete = document.createElement("i");
          iconDelete.className = "fa fa-times";
          btnDelete.appendChild(iconDelete);
          div.appendChild(btnEdit);
          div.appendChild(btnDelete);
          return div.outerHTML;
        }
      }]
    });
  },
  modeCreate: function modeCreate() {
    materialViewModel.page.isEdit(false);
    materialViewModel.page.title("New");
    materialViewModel.page.description("Create a new material using this form, make sure you fill them all");
  },
  modeUpdate: function modeUpdate() {
    materialViewModel.page.isEdit(true);
    materialViewModel.page.title("Update");
    materialViewModel.page.description("Update material using this form, make sure you fill them all");
  },
  form: {
    material_id: ko.observable(null).extend(),
    name: ko.observable(null).extend({
      required: true
    }),
    description: ko.observable(null).extend({
      required: true
    }),
    picture: ko.observable(null).extend({
      required: true
    }),
    number_of_meetings: ko.observable(null).extend({
      required: true
    }),
    unit_of_meeting: ko.observable(null).extend({
      required: true
    }),
    min_quota: ko.observable(null).extend({
      required: true
    }),
    min_age: ko.observable(null).extend({
      required: true
    }),
    max_age: ko.observable(null).extend({
      required: true
    })
  },
  page: {
    isEdit: ko.observable(false),
    title: ko.observable(null),
    description: ko.observable(null)
  },
  clearForm: function clearForm() {
    materialViewModel.form.material_id(null);
    materialViewModel.form.name(null);
    materialViewModel.form.picture(null);
    materialViewModel.form.description(null);
    materialViewModel.form.number_of_meetings(null);
    materialViewModel.form.unit_of_meeting(null);
    materialViewModel.form.min_quota(null);
    materialViewModel.form.min_age(null);
    materialViewModel.form.max_age(null);
    materialViewModel.errors.showAllMessages(false);
  },
  openModal: function openModal(materialId) {
    if (materialId !== null) {
      $.ajax({
        url: '/api/material/' + materialId,
        type: 'GET',
        dataType: 'json',
        headers: {
          'Authorization': 'Bearer ' + materialViewModel.token
        },
        contentType: 'application/json; charset=utf-8',
        success: function success(data) {
          var material = data.data;
          materialViewModel.form.material_id(material.material_id);
          materialViewModel.form.name(material.name);
          materialViewModel.form.picture(material.picture);
          materialViewModel.form.description(material.description);
          materialViewModel.form.number_of_meetings(material.number_of_meetings);
          materialViewModel.form.unit_of_meeting(material.unit_of_meeting);
          materialViewModel.form.min_quota(material.min_quota);
          materialViewModel.form.min_age(material.min_age);
          materialViewModel.form.max_age(material.max_age);
          materialViewModel.modeUpdate();
          $('#materialFormModal').modal('show');
        },
        error: function error(data) {
          materialViewModel.errorHandler(data);
        }
      });
    } else {
      materialViewModel.modeCreate();
      $('#materialFormModal').modal('show');
    }
  },
  closeModal: function closeModal() {
    materialViewModel.clearForm();
    $('#materialFormModal').modal('toggle');
  },
  deleteMaterial: function deleteMaterial(data) {
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      buttons: {
        confirm: {
          text: 'Yes, delete it!',
          className: 'btn btn-info'
        },
        cancel: {
          visible: true,
          className: 'btn btn-danger'
        }
      }
    }).then(function (Delete) {
      if (Delete) {
        $.ajax({
          url: '/api/material/' + data,
          type: 'DELETE',
          dataType: 'json',
          headers: {
            'Authorization': 'Bearer ' + materialViewModel.token
          },
          contentType: 'application/json; charset=utf-8',
          success: function success(data) {
            table.ajax.reload();
            materialViewModel.showSuccess(data.message);
          },
          error: function error(data) {
            materialViewModel.errorHandler(data);
          }
        });
      } else {
        swal.close();
      }
    });
  },
  saveMaterial: function saveMaterial() {
    if (materialViewModel.errors().length > 0) {
      materialViewModel.errors.showAllMessages();
      return;
    }

    if (materialViewModel.page.isEdit() === true) {
      swal({
        title: 'Are you sure to update this data?',
        icon: 'warning',
        buttons: {
          confirm: {
            text: 'Yes, update it!',
            className: 'btn btn-info'
          },
          cancel: {
            visible: true,
            className: 'btn btn-danger'
          }
        }
      }).then(function (save) {
        if (save) {
          $.ajax({
            url: "/api/material/".concat(materialViewModel.form.material_id()),
            type: 'PUT',
            dataType: 'json',
            data: ko.toJSON(materialViewModel.form),
            headers: {
              'Authorization': 'Bearer ' + materialViewModel.token,
              'Content-Type': 'application/json'
            },
            contentType: 'application/json; charset=utf-8',
            success: function success(data) {
              materialViewModel.closeModal();
              table.ajax.reload();
              materialViewModel.showSuccess(data.message);
            },
            error: function error(data) {
              var payload = data.responseJSON;
              materialViewModel.errorHandler(data, [payload.errors.name, payload.errors.picture, payload.errors.description, payload.errors.unit_of_meeting, payload.errors.number_of_meetings, payload.errors.min_quota, payload.errors.min_age, payload.errors.max_age]);
            }
          });
        } else {
          swal.close();
        }
      });
    } else {
      swal({
        title: 'Are you sure to save this data?',
        icon: 'warning',
        buttons: {
          confirm: {
            text: 'Yes, save it!',
            className: 'btn btn-info'
          },
          cancel: {
            visible: true,
            className: 'btn btn-danger'
          }
        }
      }).then(function (save) {
        if (save) {
          $.ajax({
            url: '/api/material',
            type: 'POST',
            dataType: 'json',
            data: ko.toJSON(materialViewModel.form),
            headers: {
              'Authorization': 'Bearer ' + materialViewModel.token,
              'Content-Type': 'application/json'
            },
            contentType: 'application/json; charset=utf-8',
            success: function success(data) {
              materialViewModel.closeModal();
              table.ajax.reload();
              materialViewModel.showSuccess(data.message);
            },
            error: function error(data) {
              var payload = data.responseJSON;
              materialViewModel.errorHandler(data, [payload.errors.name, payload.errors.picture, payload.errors.description, payload.errors.unit_of_meeting, payload.errors.number_of_meetings, payload.errors.min_quota, payload.errors.min_age, payload.errors.max_age]);
            }
          });
        } else {
          materialViewModel.clearForm();
          swal.close();
        }
      });
    }
  }
});

materialViewModel.errors = ko.validation.group(materialViewModel.form);
ko.applyBindings(materialViewModel);