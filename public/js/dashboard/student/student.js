var table;
$(document).ready(function(){
    studentViewModel.init();    
});

var studentViewModel = {
    ...commonViewModel,
    init: function(){
        table = $('#add-row').DataTable( {
            "processing": true,
            "serverSide": true,
            "pageLength": 5,
            "lengthChange": false,
            "ajax": {
                'url': '/api/student',
                'type': 'GET',
                'beforeSend': function (request) {
                    request.setRequestHeader("Authorization", "Bearer " + localStorage.getItem("token"));
                },
                'error': function (data, error, code){
                    studentViewModel.errorHandler(data)
                }
            },
            "order": [[4, 'desc']],
            "columns": [
                { 
                    data: "name" 
                },{ 
                    data: "email" 
                },
                { 
                    data: "phone_number" 
                },
                { 
                    data: "dob",
                    render: function(data, type, full, meta){
                        return studentViewModel.dateFormat(data, 'DD MMM YYYY');
                    }   
                },
                { 
                    data: "created_at",
                    render: function(data, type, full, meta){
                        return studentViewModel.dateFormat(data);
                    }                
                },
                { 
                    data: "updated_at",
                    render: function(data, type, full, meta){
                        return studentViewModel.dateFormat(data);
                    }
                },
                {
                    data: "user_id",
                    defaultContent: '',
                    orderable: false,
                    render: function(data, type, full, meta){
                        var div = document.createElement("div");
                        div.className = "form-button-action";
    
                        var btnEdit = document.createElement("a");
                        btnEdit.className = "btn btn-link btn-primary";
                        btnEdit.setAttribute('onclick',"studentViewModel.openModal('"+data+"')");
                        var iconEdit = document.createElement("i");
                        iconEdit.className = "fa fa-edit";
                        btnEdit.appendChild(iconEdit);
     
                        div.appendChild(btnEdit);

                        var btnReset = document.createElement("a");
                        btnReset.className = "btn btn-link btn-secondary";
                        btnReset.setAttribute('onclick',"studentViewModel.openResetModal('"+data+"')");
                        var iconReset = document.createElement("i");
                        iconReset.className = "fa fa-user-lock";
                        btnReset.appendChild(iconReset);
     
                        div.appendChild(btnReset);
    
                        return div.outerHTML;
                    }
                }                   
            ]
        } );
        $('#dob').datetimepicker({
            format: 'YYYY-MM-DD',
        });
    },
    modeCreate: function(){
        studentViewModel.page.isEdit(false);
        studentViewModel.page.title("New");
        studentViewModel.page.description("Create a new student using this form, make sure you fill them all");
        $( ".travecode-div-container" ).show();
    },
    modeUpdate: function(){
        studentViewModel.page.isEdit(true);
        studentViewModel.page.title("Update");
        studentViewModel.page.description("Update student using this form, make sure you fill them all");
        $( ".travecode-div-container" ).hide();
    },
    form : {
        user_id : ko.observable(null).extend(),
        name : ko.observable(null).extend({
            required: true,
        }),
        email : ko.observable(null).extend({
            required: true,
        }),
        password : ko.observable(null).extend(),
        password_confirmation : ko.observable(null).extend(),
        dob : ko.observable(null).extend({
            required: true,
        }),
        grade : ko.observable(null).extend({
            required: true,
        }),
        phone_number : ko.observable(null).extend({
            required: true,
        })
    },
    resetForm: {
        user_id : ko.observable(null).extend({
            required: true,
        }),
        password : ko.observable(null).extend({
            required: true,
        }),
        password_confirmation : ko.observable(null).extend({
            required: true,
        }),
    },
    page: {
        isEdit : ko.observable(false),
        title: ko.observable(null),
        description: ko.observable(null),
    },
    clearForm : function(){
        studentViewModel.form.user_id(null);
        studentViewModel.form.name(null);
        studentViewModel.form.email(null);
        studentViewModel.form.password(null);
        studentViewModel.form.password_confirmation(null);
        studentViewModel.form.dob(null);
        studentViewModel.form.grade(null);
        studentViewModel.form.phone_number(null);

        $('#dob').val(null);

        studentViewModel.errors.showAllMessages(false);
    },
    openModal: function(studentId){
        studentViewModel.clearForm();
        if(studentId !== null){
            $.ajax({
                url: '/api/student/'+studentId,
                type: 'GET',
                dataType: 'json',
                headers: {
                    'Authorization': 'Bearer ' + studentViewModel.token,
                },
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    var student = data.data;
    
                    studentViewModel.form.user_id(student.user_id);
                    studentViewModel.form.name(student.name);
                    studentViewModel.form.email(student.email);
                    studentViewModel.form.dob(student.dob);
                    studentViewModel.form.grade(student.grade);
                    studentViewModel.form.phone_number(student.phone_number);
   
                    $('#dob').val(studentViewModel.dateFormat(student.dob, "YYYY-MM-DD"));

                    studentViewModel.modeUpdate();
                    $('#studentFormModal').modal('show');
                },
                error: function (data) {
                    studentViewModel.errorHandler(data);
                }
            });
        } else {
            studentViewModel.modeCreate();
            $('#studentFormModal').modal('show');
        }
    },
    closeModal: function(){
        studentViewModel.clearForm();
        $('#studentFormModal').modal('toggle');
    },
    saveStudent: function(){
        studentViewModel.form.dob($('#dob').val())

        if(studentViewModel.errors().length > 0){
            studentViewModel.errors.showAllMessages();
            return;
        }

        if (studentViewModel.page.isEdit() === true) {
            swal({
                title: 'Are you sure to update this data?',
                icon: 'warning',
                buttons:{
                    confirm: {
                        text : 'Yes, update it!',
                        className : 'btn btn-info'
                    },
                    cancel: {
                        visible: true,
                        className: 'btn btn-danger'
                    }
                }
            }).then((save) => {
                if (save) {
                    $.ajax({
                        url: `/api/student/${studentViewModel.form.user_id()}`,
                        type: 'PUT',
                        dataType: 'json',
                        data: ko.toJSON(studentViewModel.form),
                        headers: {
                            'Authorization': 'Bearer ' + studentViewModel.token,
                            'Content-Type': 'application/json',
                        },
                        contentType: 'application/json; charset=utf-8',
                        success: function (data) {
                            studentViewModel.closeModal();
                            table.ajax.reload();
                            studentViewModel.showSuccess(data.message);
                        },
                        error: function (data) {
                            var payload = data.responseJSON;
                            var errors = [];
                            if(payload.code === 'validation_data'){
                                errors = [
                                    payload.errors.name,
                                    payload.errors.email,
                                    payload.errors.dob,
                                    payload.errors.grade,
                                    payload.errors.phone_number
                                ]
                            }
                            studentViewModel.errorHandler(data, errors);
                        }
                    });
                } else {
                    swal.close();
                }
            });
        } else {
            swal({
                title: 'Are you sure to save this data?',
                icon: 'warning',
                buttons:{
                    confirm: {
                        text : 'Yes, save it!',
                        className : 'btn btn-info'
                    },
                    cancel: {
                        visible: true,
                        className: 'btn btn-danger'
                    }
                }
            }).then((save) => {
                if (save) {
                    $.ajax({
                        url: '/api/student',
                        type: 'POST',
                        dataType: 'json',
                        data: ko.toJSON(studentViewModel.form),
                        headers: {
                            'Authorization': 'Bearer ' + studentViewModel.token,
                            'Content-Type': 'application/json',
                        },
                        contentType: 'application/json; charset=utf-8',
                        success: function (data) {
                            studentViewModel.closeModal();
                            table.ajax.reload();
                            studentViewModel.showSuccess(data.message);
                        },
                        error: function (data) {
                            var payload = data.responseJSON;
                            var errors = [];
                            if(payload.code === 'validation_data'){
                                errors = [
                                    payload.errors.name,
                                    payload.errors.email,
                                    payload.errors.dob,
                                    payload.errors.grade,
                                    payload.errors.phone_number,
                                    payload.errors.password,
                                    payload.errors.password_confirmation
                                ]
                            }
                            studentViewModel.errorHandler(data, errors);
                        }
                    });
                } else {
                    studentViewModel.clearForm();
                    swal.close();
                }
            });
        }
    },
    openResetModal: function(userId){
        studentViewModel.resetForm.user_id(userId);
        studentViewModel.resetForm.password(null);
        studentViewModel.resetForm.password_confirmation(null);
        $('#resetModal').modal('show');
    },
    closeResetModal: function(){
        studentViewModel.clearForm();
        $('#resetModal').modal('toggle');
    },
    resetPassword: function(){
        if(studentViewModel.resetErrors().length > 0){
            studentViewModel.resetErrors.showAllMessages();
            return;
        }

        swal({
            title: 'Are you sure to reset this password?',
            icon: 'warning',
            buttons:{
                confirm: {
                    text : 'Yes, reset it!',
                    className : 'btn btn-info'
                },
                cancel: {
                    visible: true,
                    className: 'btn btn-danger'
                }
            }
        }).then((save) => {
            if (save) {
                $.ajax({
                    url: `/api/account/${studentViewModel.resetForm.user_id()}/reset`,
                    type: 'PUT',
                    dataType: 'json',
                    data: ko.toJSON(studentViewModel.resetForm),
                    headers: {
                        'Authorization': 'Bearer ' + studentViewModel.token,
                        'Content-Type': 'application/json',
                    },
                    contentType: 'application/json; charset=utf-8',
                    success: function (data) {
                        studentViewModel.closeResetModal();
                        table.ajax.reload();
                        studentViewModel.showSuccess(data.message);
                    },
                    error: function (data) {
                        var payload = data.responseJSON;
                        var errors = [];
                        if(payload.code === 'validation_data'){
                            errors = [
                                payload.errors.name,
                                payload.errors.email,
                                payload.errors.dob,
                                payload.errors.grade,
                                payload.errors.phone_number
                            ]
                        }
                        studentViewModel.errorHandler(data, errors);
                    }
                });
            } else {
                swal.close();
            }
        });
    }
};

studentViewModel.errors = ko.validation.group(studentViewModel.form);
studentViewModel.resetErrors = ko.validation.group(studentViewModel.resetForm);
ko.applyBindings(studentViewModel);