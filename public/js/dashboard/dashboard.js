
window.onpageshow = function () {
    commonViewModel.init();
};
var commonViewModel = {
    name : localStorage.getItem('name'),
    token : localStorage.getItem('token'),
    role : localStorage.getItem('role'),
    userId : localStorage.getItem('userId'),
    getRole: function(){
        var role = localStorage.getItem('role');
        if (role === 'role_admin') {
            return "Administrator";
        }
        if (role === 'role_facilitator') {
            return "Facilitator";
        }
        if (role === 'role_student') {
            return "Student";
        }
        return "Not Found";
    },
    isAdmin: function(){
        return localStorage.getItem('role') === 'role_admin';
    },
    isFacilitator: function(){
        return localStorage.getItem('role') === 'role_facilitator';
    },
    isStudent: function(){
        return localStorage.getItem('role') === 'role_student';
    },
    getInitials: function() {
        var name = localStorage.getItem('name');
        var initials = name.split(' ');
        
        if(initials.length > 1) {
          initials = initials.shift().charAt(0) + initials.pop().charAt(0);
        } else {
          initials = name.substring(0, 2);
        }
        
        return initials.toUpperCase();
    },
    errorHandler: function(data, errorObjectList){
        try {
            var payload = data.responseJSON;
            if(data.status === 400){
                if(errorObjectList && payload.code === 'validation_data'){
                    commonViewModel.showErrorInvalidData(errorObjectList)
                } else {
                    commonViewModel.showError(payload.message);
                }
            } else if((data.status === 401) || (data.status === 500 && payload.message === 'Token has expired')){
                localStorage.clear();
                window.location = "/lms/login?expired=true";
            } else {
                commonViewModel.showError(payload.message);
            }
        } catch (err){
            showError("Something went wrong !");
        }
    },
    showErrorInvalidData: function(objectList){
        Array.from(objectList).forEach(object => {
            if(object && object.length > 0){
                Array.from(object).forEach(message => {
                    commonViewModel.showError(message);
                });
            }
        })
    },
    showSuccess: function(message){
        var content = {};
        content.message = message;
        content.icon = 'fa fa-exclamation-circle';
        content.target = '_blank';
    
        $.notify(content,{
            type: 'info',
            placement: {
                from: 'top',
                align: 'right'
            },
            delay: 1000,
        });
    },
    showError: function(message){
        if(message === undefined){
            message = "Something went wrong, please try again later !"
        }
        var content = {};
        content.message = message;
        content.icon = 'fa fa-exclamation-circle';
        content.target = '_blank';
    
        $.notify(content,{
            type: 'warning',
            placement: {
                from: 'top',
                align: 'right'
            },
            delay: 3000,
        });
    },
    logout: function(){
        localStorage.clear();
        window.location = "/lms/login";
    },
    dateFormat: function(date, format){
        if(format){
            return moment(date).format(format);            
        }
        return moment(date).format('DD MMM YYYY, HH:mm');
    },
    init: function(){
        if ($('#preloader').length) {
            $('#preloader').delay(100).fadeOut('slow', function() {
                $(this).remove();
            });
        }
        $('.travecode-select').select2({
            theme: "bootstrap",
            minimumResultsForSearch: -1
        });
    },
    refreshSelect2: function(){
        $('.travecode-select').trigger('change.select2');
    }
}

ko.validation.init({
    registerExtenders: true,
    messagesOnModified: true,
    insertMessages: true,
    parseInputAttributes: true,
    messageTemplate: null
}, true);