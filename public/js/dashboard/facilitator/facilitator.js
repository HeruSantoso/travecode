var table;
$(document).ready(function(){
    facilitatorViewModel.init();    
});

var facilitatorViewModel = {
    ...commonViewModel,
    init: function(){
        table = $('#add-row').DataTable( {
            "processing": true,
            "serverSide": true,
            "pageLength": 5,
            "lengthChange": false,
            "ajax": {
                'url': '/api/facilitator',
                'type': 'GET',
                'beforeSend': function (request) {
                    request.setRequestHeader("Authorization", "Bearer " + localStorage.getItem("token"));
                },
                'error': function (data, error, code){
                    facilitatorViewModel.errorHandler(data)
                }
            },
            "order": [[4, 'desc']],
            "columns": [
                { 
                    data: "name" 
                },{ 
                    data: "email" 
                },
                { 
                    data: "phone_number" 
                },
                { 
                    data: "dob",
                    render: function(data, type, full, meta){
                        return facilitatorViewModel.dateFormat(data, 'DD MMM YYYY');
                    }   
                },
                { 
                    data: "created_at",
                    render: function(data, type, full, meta){
                        return facilitatorViewModel.dateFormat(data);
                    }                
                },
                { 
                    data: "updated_at",
                    render: function(data, type, full, meta){
                        return facilitatorViewModel.dateFormat(data);
                    }
                },
                {
                    data: "user_id",
                    defaultContent: '',
                    orderable: false,
                    render: function(data, type, full, meta){
                        var div = document.createElement("div");
                        div.className = "form-button-action";
    
                        var btnEdit = document.createElement("a");
                        btnEdit.className = "btn btn-link btn-primary";
                        btnEdit.setAttribute('onclick',"facilitatorViewModel.openModal('"+data+"')");
                        var iconEdit = document.createElement("i");
                        iconEdit.className = "fa fa-edit";
                        btnEdit.appendChild(iconEdit);
     
                        div.appendChild(btnEdit);

                        var btnReset = document.createElement("a");
                        btnReset.className = "btn btn-link btn-secondary";
                        btnReset.setAttribute('onclick',"facilitatorViewModel.openResetModal('"+data+"')");
                        var iconReset = document.createElement("i");
                        iconReset.className = "fa fa-user-lock";
                        btnReset.appendChild(iconReset);
     
                        div.appendChild(btnReset);

                        return div.outerHTML;
                    }
                }                   
            ]
        } );
        $('#dob').datetimepicker({
            format: 'YYYY-MM-DD',
        });
    },
    modeCreate: function(){
        facilitatorViewModel.page.isEdit(false);
        facilitatorViewModel.page.title("New");
        facilitatorViewModel.page.description("Create a new facilitator using this form, make sure you fill them all");
        $( ".travecode-div-container" ).show();
    },
    modeUpdate: function(){
        facilitatorViewModel.page.isEdit(true);
        facilitatorViewModel.page.title("Update");
        facilitatorViewModel.page.description("Update facilitator using this form, make sure you fill them all");
        $( ".travecode-div-container" ).hide();
    },
    form : {
        user_id : ko.observable(null).extend(),
        name : ko.observable(null).extend({
            required: true,
        }),
        email : ko.observable(null).extend({
            required: true,
        }),
        password : ko.observable(null).extend(),
        password_confirmation : ko.observable(null).extend(),
        dob : ko.observable(null).extend({
            required: true,
        }),
        grade : ko.observable(null).extend({
            required: true,
        }),
        phone_number : ko.observable(null).extend({
            required: true,
        })
    },
    resetForm: {
        user_id : ko.observable(null).extend({
            required: true,
        }),
        password : ko.observable(null).extend({
            required: true,
        }),
        password_confirmation : ko.observable(null).extend({
            required: true,
        }),
    },
    page: {
        isEdit : ko.observable(false),
        title: ko.observable(null),
        description: ko.observable(null),
    },
    clearForm : function(){
        facilitatorViewModel.form.user_id(null);
        facilitatorViewModel.form.name(null);
        facilitatorViewModel.form.email(null);
        facilitatorViewModel.form.password(null);
        facilitatorViewModel.form.password_confirmation(null);
        facilitatorViewModel.form.dob(null);
        facilitatorViewModel.form.grade(null);
        facilitatorViewModel.form.phone_number(null);

        $('#dob').val(null);

        facilitatorViewModel.errors.showAllMessages(false);
    },
    openModal: function(facilitatorId){
        facilitatorViewModel.clearForm();
        if(facilitatorId !== null){
            $.ajax({
                url: '/api/facilitator/'+facilitatorId,
                type: 'GET',
                dataType: 'json',
                headers: {
                    'Authorization': 'Bearer ' + facilitatorViewModel.token,
                },
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    var facilitator = data.data;
    
                    facilitatorViewModel.form.user_id(facilitator.user_id);
                    facilitatorViewModel.form.name(facilitator.name);
                    facilitatorViewModel.form.email(facilitator.email);
                    facilitatorViewModel.form.dob(facilitator.dob);
                    facilitatorViewModel.form.grade(facilitator.grade);
                    facilitatorViewModel.form.phone_number(facilitator.phone_number);
   
                    $('#dob').val(facilitatorViewModel.dateFormat(facilitator.dob, "YYYY-MM-DD"));

                    facilitatorViewModel.modeUpdate();
                    $('#facilitatorFormModal').modal('show');
                },
                error: function (data) {
                    facilitatorViewModel.errorHandler(data);
                }
            });
        } else {
            facilitatorViewModel.modeCreate();
            $('#facilitatorFormModal').modal('show');
        }
    },
    closeModal: function(){
        facilitatorViewModel.clearForm();
        $('#facilitatorFormModal').modal('toggle');
    },
    saveFacilitator: function(){
        facilitatorViewModel.form.dob($('#dob').val())

        if(facilitatorViewModel.errors().length > 0){
            facilitatorViewModel.errors.showAllMessages();
            return;
        }

        if (facilitatorViewModel.page.isEdit() === true) {
            swal({
                title: 'Are you sure to update this data?',
                icon: 'warning',
                buttons:{
                    confirm: {
                        text : 'Yes, update it!',
                        className : 'btn btn-info'
                    },
                    cancel: {
                        visible: true,
                        className: 'btn btn-danger'
                    }
                }
            }).then((save) => {
                if (save) {
                    $.ajax({
                        url: `/api/facilitator/${facilitatorViewModel.form.user_id()}`,
                        type: 'PUT',
                        dataType: 'json',
                        data: ko.toJSON(facilitatorViewModel.form),
                        headers: {
                            'Authorization': 'Bearer ' + facilitatorViewModel.token,
                            'Content-Type': 'application/json',
                        },
                        contentType: 'application/json; charset=utf-8',
                        success: function (data) {
                            facilitatorViewModel.closeModal();
                            table.ajax.reload();
                            facilitatorViewModel.showSuccess(data.message);
                        },
                        error: function (data) {
                            var payload = data.responseJSON;
                            var errors = [];
                            if(payload.code === 'validation_data'){
                                errors = [
                                    payload.errors.name,
                                    payload.errors.email,
                                    payload.errors.dob,
                                    payload.errors.grade,
                                    payload.errors.phone_number
                                ]
                            }
                            facilitatorViewModel.errorHandler(data, errors);
                        }
                    });
                } else {
                    swal.close();
                }
            });
        } else {
            swal({
                title: 'Are you sure to save this data?',
                icon: 'warning',
                buttons:{
                    confirm: {
                        text : 'Yes, save it!',
                        className : 'btn btn-info'
                    },
                    cancel: {
                        visible: true,
                        className: 'btn btn-danger'
                    }
                }
            }).then((save) => {
                if (save) {
                    $.ajax({
                        url: '/api/facilitator',
                        type: 'POST',
                        dataType: 'json',
                        data: ko.toJSON(facilitatorViewModel.form),
                        headers: {
                            'Authorization': 'Bearer ' + facilitatorViewModel.token,
                            'Content-Type': 'application/json',
                        },
                        contentType: 'application/json; charset=utf-8',
                        success: function (data) {
                            facilitatorViewModel.closeModal();
                            table.ajax.reload();
                            facilitatorViewModel.showSuccess(data.message);
                        },
                        error: function (data) {
                            var payload = data.responseJSON;
                            var errors = [];
                            if(payload.code === 'validation_data'){
                                errors = [
                                    payload.errors.name,
                                    payload.errors.email,
                                    payload.errors.dob,
                                    payload.errors.grade,
                                    payload.errors.phone_number,
                                    payload.errors.password,
                                    payload.errors.password_confirmation
                                ]
                            }
                            facilitatorViewModel.errorHandler(data, errors);
                        }
                    });
                } else {
                    facilitatorViewModel.clearForm();
                    swal.close();
                }
            });
        }
    },
    openResetModal: function(userId){
        facilitatorViewModel.resetForm.user_id(userId);
        facilitatorViewModel.resetForm.password(null);
        facilitatorViewModel.resetForm.password_confirmation(null);
        $('#resetModal').modal('show');
    },
    closeResetModal: function(){
        facilitatorViewModel.clearForm();
        $('#resetModal').modal('toggle');
    },
    resetPassword: function(){
        if(facilitatorViewModel.resetErrors().length > 0){
            facilitatorViewModel.resetErrors.showAllMessages();
            return;
        }

        swal({
            title: 'Are you sure to reset this password?',
            icon: 'warning',
            buttons:{
                confirm: {
                    text : 'Yes, reset it!',
                    className : 'btn btn-info'
                },
                cancel: {
                    visible: true,
                    className: 'btn btn-danger'
                }
            }
        }).then((save) => {
            if (save) {
                $.ajax({
                    url: `/api/account/${facilitatorViewModel.resetForm.user_id()}/reset`,
                    type: 'PUT',
                    dataType: 'json',
                    data: ko.toJSON(facilitatorViewModel.resetForm),
                    headers: {
                        'Authorization': 'Bearer ' + facilitatorViewModel.token,
                        'Content-Type': 'application/json',
                    },
                    contentType: 'application/json; charset=utf-8',
                    success: function (data) {
                        facilitatorViewModel.closeResetModal();
                        table.ajax.reload();
                        facilitatorViewModel.showSuccess(data.message);
                    },
                    error: function (data) {
                        var payload = data.responseJSON;
                        var errors = [];
                        if(payload.code === 'validation_data'){
                            errors = [
                                payload.errors.name,
                                payload.errors.email,
                                payload.errors.dob,
                                payload.errors.grade,
                                payload.errors.phone_number
                            ]
                        }
                        facilitatorViewModel.errorHandler(data, errors);
                    }
                });
            } else {
                swal.close();
            }
        });
    }
};

facilitatorViewModel.errors = ko.validation.group(facilitatorViewModel.form);
facilitatorViewModel.resetErrors = ko.validation.group(facilitatorViewModel.resetForm);
ko.applyBindings(facilitatorViewModel);