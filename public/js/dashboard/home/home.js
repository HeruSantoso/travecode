$( document ).ready(function() {
    if(localStorage.getItem('token') === null){
        window.location = "/lms/login";
    }
    adminHomeViewModel.initCalender();
});

var adminHomeViewModel = {
    ...commonViewModel,
    schedule: {
        classroom_name: ko.observable(null),
        topic: ko.observable(null),
        facilitator_name: ko.observable(null),
        meeting_link: ko.observable(null),
        classroom_status_text: ko.observable(null),
        classroom_status_class: ko.observable(null)
    },
    init: function(){
        adminHomeViewModel.initCalender();
    },
    initCalender: function(){
        $calendar = $('#calendar');
        $calendar.fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay,listWeek'
            },
            selectable : true,
            selectHelper: true,
            eventClick: function(info) {
                adminHomeViewModel.openScheduleInformationModal(info.extendedProps);
            },
            events: function(start, end, timezone, callback) {
                $.ajax({
                    url: '/api/schedule/calender/list',
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        start: adminHomeViewModel.dateFormat(start, 'YYYY-MM-DD'),
                        end: adminHomeViewModel.dateFormat(end, 'YYYY-MM-DD')
                    },
                    headers: {
                        'Authorization': 'Bearer ' + adminHomeViewModel.token,
                    },
                    contentType: 'application/json; charset=utf-8',
                    success: function (data) {
                        callback(
                            data
                        )
                    },
                    error: function (data) {
                        adminHomeViewModel.errorHandler(data);
                    }
                });
            }

        });
    },
    openScheduleInformationModal: function(data){
        adminHomeViewModel.schedule.classroom_name(data.classroom_name);
        adminHomeViewModel.schedule.topic(data.topic);
        adminHomeViewModel.schedule.facilitator_name(data.facilitator_name);
        adminHomeViewModel.schedule.meeting_link(data.meeting_link);
        adminHomeViewModel.schedule.classroom_name(data.classroom_name);

        var classroomStatusText = '';
        var classroomStatusClass = '';
        if(data.classroom_status === 0){
            classroomStatusText = 'Draft';
            classroomStatusClass = 'badge badge-warning';
        } else if(data.classroom_status === 1){
            classroomStatusText = 'Open'
            classroomStatusClass = 'badge badge-info';
        } else if(data.classroom_status === 2){
            classroomStatusText = 'Close'
            classroomStatusClass = 'badge badge-danger';
        }
        adminHomeViewModel.schedule.classroom_status_text(classroomStatusText);
        adminHomeViewModel.schedule.classroom_status_class(classroomStatusClass);

        $("#scheduleInfoModal").modal('show');
        
    },
};
ko.applyBindings(adminHomeViewModel);