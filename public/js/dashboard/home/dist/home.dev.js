"use strict";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

$(document).ready(function () {
  if (localStorage.getItem('token') === null) {
    window.location = "/lms/login";
  }

  adminHomeViewModel.initCalender();
});

var adminHomeViewModel = _objectSpread({}, commonViewModel, {
  schedule: {
    classroom_name: ko.observable(null),
    topic: ko.observable(null),
    facilitator_name: ko.observable(null),
    meeting_link: ko.observable(null),
    classroom_status_text: ko.observable(null),
    classroom_status_class: ko.observable(null)
  },
  init: function init() {
    adminHomeViewModel.initCalender();
  },
  initCalender: function initCalender() {
    $calendar = $('#calendar');
    $calendar.fullCalendar({
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay,listWeek'
      },
      selectable: true,
      selectHelper: true,
      eventClick: function eventClick(info) {
        adminHomeViewModel.openScheduleInformationModal(info.extendedProps);
      },
      events: function events(start, end, timezone, callback) {
        $.ajax({
          url: '/api/schedule/calender/list',
          type: 'GET',
          dataType: 'json',
          data: {
            start: adminHomeViewModel.dateFormat(start, 'YYYY-MM-DD'),
            end: adminHomeViewModel.dateFormat(end, 'YYYY-MM-DD')
          },
          headers: {
            'Authorization': 'Bearer ' + adminHomeViewModel.token
          },
          contentType: 'application/json; charset=utf-8',
          success: function success(data) {
            callback(data);
          },
          error: function error(data) {
            adminHomeViewModel.errorHandler(data);
          }
        });
      }
    });
  },
  openScheduleInformationModal: function openScheduleInformationModal(data) {
    adminHomeViewModel.schedule.classroom_name(data.classroom_name);
    adminHomeViewModel.schedule.topic(data.topic);
    adminHomeViewModel.schedule.facilitator_name(data.facilitator_name);
    adminHomeViewModel.schedule.meeting_link(data.meeting_link);
    adminHomeViewModel.schedule.classroom_name(data.classroom_name);
    var classroomStatusText = '';
    var classroomStatusClass = '';

    if (data.classroom_status === 0) {
      classroomStatusText = 'Draft';
      classroomStatusClass = 'badge badge-warning';
    } else if (data.classroom_status === 1) {
      classroomStatusText = 'Open';
      classroomStatusClass = 'badge badge-info';
    } else if (data.classroom_status === 2) {
      classroomStatusText = 'Close';
      classroomStatusClass = 'badge badge-danger';
    }

    adminHomeViewModel.schedule.classroom_status_text(classroomStatusText);
    adminHomeViewModel.schedule.classroom_status_class(classroomStatusClass);
    $("#scheduleInfoModal").modal('show');
  }
});

ko.applyBindings(adminHomeViewModel);