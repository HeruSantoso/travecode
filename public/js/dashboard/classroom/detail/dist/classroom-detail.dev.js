"use strict";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var table;
var participantTable;
$(document).ready(function () {
  classroomDetailViewModel.getMaterialList();
  classroomDetailViewModel.getFacilitatorList();
  classroomDetailViewModel.init();
});

var classroomDetailViewModel = _objectSpread({}, commonViewModel, {
  init: function init() {
    table = $('#add-row').DataTable({
      "processing": true,
      "serverSide": true,
      "pageLength": 10,
      "lengthChange": false,
      "ajax": {
        'url': '/api/schedule?classroom_id=' + $("#classroomId").val(),
        'type': 'GET',
        'beforeSend': function beforeSend(request) {
          request.setRequestHeader("Authorization", "Bearer " + localStorage.getItem("token"));
        },
        'complete': function complete(data) {
          var iTotalRecords = data['responseJSON'].iTotalRecords;
          classroomDetailViewModel.page.scheduleCount(iTotalRecords);
        },
        'error': function error(data, _error, code) {
          classroomDetailViewModel.errorHandler(data);
        }
      },
      "order": [[2, 'asc']],
      "columns": [{
        orderable: false,
        render: function render(data, type, full, meta) {
          return meta.row + meta.settings._iDisplayStart + 1;
        }
      }, {
        data: "topic"
      }, {
        data: "start",
        render: function render(data, type, full, meta) {
          return classroomDetailViewModel.dateFormat(data);
        }
      }, {
        data: "end",
        render: function render(data, type, full, meta) {
          return classroomDetailViewModel.dateFormat(data);
        }
      }, {
        data: "facilitator_name"
      }, {
        orderable: false,
        data: "meeting_link",
        render: function render(data, type, full, meta) {
          var a = document.createElement("a");
          a.setAttribute('href', data);
          a.setAttribute('target', '_blank');
          var text = document.createTextNode("Link");
          a.appendChild(text);
          return a.outerHTML;
        }
      }, {
        data: "module",
        render: function render(data, type, full, meta) {
          var className = '';
          var text = '';

          if (data) {
            text = "Done";
            className = "badge badge-success";
          } else {
            text = "Not yet";
            className = "badge badge-warning";
          }

          var span = document.createElement("span");
          span.className = className;
          var isAdmin = classroomDetailViewModel.isAdmin() && data;

          if (isAdmin) {
            span.className = className + " travecode-clickable";
            span.setAttribute('onclick', "classroomDetailViewModel.openScheduleSubmissionModal('" + full.schedule_id + "')");
          }

          var text = document.createTextNode(text);
          span.appendChild(text);
          return span.outerHTML;
        }
      }, {
        data: "schedule_id",
        defaultContent: '',
        orderable: false,
        render: function render(data, type, full, meta) {
          var div = document.createElement("div");
          div.className = "form-button-action";

          if (classroomDetailViewModel.isAdmin()) {
            var btnEdit = document.createElement("a");
            btnEdit.className = "btn btn-link btn-primary";
            btnEdit.setAttribute('onclick', "classroomDetailViewModel.openModal('" + data + "')");
            var iconEdit = document.createElement("i");
            iconEdit.className = "fa fa-edit";
            btnEdit.appendChild(iconEdit);
            div.appendChild(btnEdit);
            var btnDelete = document.createElement("button");
            btnDelete.className = "btn btn-link btn-danger";
            btnDelete.setAttribute('onclick', "classroomDetailViewModel.deleteSchedule('" + data + "')");
            var iconDelete = document.createElement("i");
            iconDelete.className = "fa fa-times";
            btnDelete.appendChild(iconDelete);
            div.appendChild(btnDelete);
          } else if (classroomDetailViewModel.isFacilitator() && classroomDetailViewModel.userId === full.facilitator_id) {
            var btnEdit = document.createElement("a");
            btnEdit.className = "btn btn-link btn-primary";
            btnEdit.setAttribute('onclick', "classroomDetailViewModel.openFacilitatorEditModal('" + full.schedule_id + "')");
            var iconEdit = document.createElement("i");
            iconEdit.className = "fa fa-edit";
            btnEdit.appendChild(iconEdit);
            div.appendChild(btnEdit);
          } else if (classroomDetailViewModel.userId !== full.facilitator_id && full.module) {
            var btnEdit = document.createElement("a");
            btnEdit.className = "btn btn-link btn-primary";
            btnEdit.setAttribute('onclick', "classroomDetailViewModel.openScheduleSubmissionModal('" + full.schedule_id + "')");
            var iconEdit = document.createElement("i");
            iconEdit.className = "fa fa-eye";
            btnEdit.appendChild(iconEdit);
            div.appendChild(btnEdit);
          }

          return div.outerHTML;
        }
      }]
    });
    $('#timepicker-start').datetimepicker({
      format: 'HH:mm'
    });
    $('#timepicker-end').datetimepicker({
      format: 'HH:mm'
    });
    $('#datepicker-date').datetimepicker({
      format: 'YYYY-MM-DD'
    });
    $('#is-any-homework-toggle').change(function () {
      classroomDetailViewModel.isAnyHomeworkOnChange($(this).prop('checked'));
    });
    participantTable = $('#participant-row').DataTable({
      "processing": true,
      "serverSide": true,
      "pageLength": 10,
      "lengthChange": false,
      "ajax": {
        'url': '/api/participant?classroom_id=' + $("#classroomId").val(),
        'type': 'GET',
        'beforeSend': function beforeSend(request) {
          request.setRequestHeader("Authorization", "Bearer " + localStorage.getItem("token"));
        },
        'complete': function complete(data) {
          var iTotalRecords = data['responseJSON'].iTotalRecords;
          classroomDetailViewModel.page.participantCount(iTotalRecords);
        },
        'error': function error(data, _error2, code) {
          classroomDetailViewModel.errorHandler(data);
        }
      },
      "order": [[1, 'asc']],
      "columns": [{
        orderable: false,
        render: function render(data, type, full, meta) {
          return meta.row + meta.settings._iDisplayStart + 1;
        }
      }, {
        data: "student_name"
      }, {
        data: "mid_test_score"
      }, {
        data: "final_test_score"
      }, {
        data: "participant_id",
        defaultContent: '',
        orderable: false,
        render: function render(data, type, full, meta) {
          var div = document.createElement("div");
          div.className = "form-button-action";

          if (classroomDetailViewModel.isAdmin()) {} else if (classroomDetailViewModel.isFacilitator()) {
            var btnEdit = document.createElement("a");
            btnEdit.className = "btn btn-link btn-primary";
            btnEdit.setAttribute('onclick', "classroomDetailViewModel.openParticipantScoreModal('" + full.participant_id + "','" + full.mid_test_score + "','" + full.final_test_score + "')");
            btnEdit.setAttribute('id', 'btn-update-score');
            var iconEdit = document.createElement("i");
            iconEdit.className = "fa fa-edit";
            btnEdit.appendChild(iconEdit);
            div.appendChild(btnEdit);
          } // var btnAttend = document.createElement("a");
          // btnAttend.className = "btn btn-link btn-secondary";
          // btnAttend.setAttribute('href',`/lms/classroom/${$("#classroomId").val()}/${full.participant_id}`);
          // var iconAttend = document.createElement("i");
          // iconAttend.className = "fa fa-calendar-check";
          // btnAttend.appendChild(iconAttend);
          // div.appendChild(btnAttend);


          return div.outerHTML;
        }
      }]
    });
    classroomDetailViewModel.getAttedances();
  },
  materialList: ko.observableArray([]),
  facilitatorList: ko.observableArray([]),
  modeCreate: function modeCreate() {
    classroomDetailViewModel.page.isEdit(false);
    classroomDetailViewModel.page.title("New");
    classroomDetailViewModel.page.description("Create a new schedule using this form, make sure you fill them all");
  },
  modeUpdate: function modeUpdate() {
    classroomDetailViewModel.page.isEdit(true);
    classroomDetailViewModel.page.title("Update");
    classroomDetailViewModel.page.description("Update schedule using this form, make sure you fill them all");
  },
  form: {
    schedule_id: ko.observable(null).extend(),
    topic: ko.observable(null).extend({
      required: true
    }),
    start_date: ko.observable(null).extend({
      required: true
    }),
    start_time: ko.observable(null).extend({
      required: true
    }),
    end_date: ko.observable(null).extend({
      required: true
    }),
    end_time: ko.observable(null).extend({
      required: true
    }),
    meeting_link: ko.observable(null).extend({
      required: true
    }),
    facilitator: ko.observable(null).extend({
      required: true
    })
  },
  submissionForm: {
    module: ko.observable(null).extend({
      required: true
    }),
    evidence_live_session: ko.observable(null).extend({
      required: true
    }),
    is_any_homework: ko.observable(null).extend({
      required: true
    }),
    notes: ko.observable(null).extend({
      required: true
    })
  },
  participantForm: {
    participant_id: ko.observable(null),
    mid_test_score: ko.observable(null),
    final_test_score: ko.observable(null)
  },
  page: {
    isEdit: ko.observable(false),
    title: ko.observable(null),
    description: ko.observable(null),
    classroom: {
      classroom_id: ko.observable(null),
      classroom_name: ko.observable(null)
    },
    facilitator: {
      facilitator_id: ko.observable(null),
      facilitator_name: ko.observable(null)
    },
    material: {
      material_id: ko.observable(null),
      name: ko.observable(null),
      number_of_meetings: ko.observable(null),
      min_quota: ko.observable(null)
    },
    scheduleCount: ko.observable(0),
    participantCount: ko.observable(0)
  },
  clearForm: function clearForm() {
    classroomDetailViewModel.form.schedule_id(null);
    classroomDetailViewModel.form.topic(null);
    classroomDetailViewModel.form.start_date(null);
    classroomDetailViewModel.form.start_time(null);
    classroomDetailViewModel.form.end_date(null);
    classroomDetailViewModel.form.end_time(null);
    classroomDetailViewModel.form.meeting_link(null);
    classroomDetailViewModel.form.facilitator(null);
    $('#datepicker-date').val(null);
    $('#timepicker-start').val(null);
    $('#timepicker-end').val(null);
    classroomDetailViewModel.errors.showAllMessages(false);
  },
  openModal: function openModal(scheduleId) {
    classroomDetailViewModel.clearForm();

    if (scheduleId !== null) {
      $.ajax({
        url: '/api/schedule/' + scheduleId,
        type: 'GET',
        dataType: 'json',
        headers: {
          'Authorization': 'Bearer ' + classroomDetailViewModel.token
        },
        contentType: 'application/json; charset=utf-8',
        success: function success(data) {
          var schedule = data.data;
          classroomDetailViewModel.form.schedule_id(schedule.schedule_id);
          classroomDetailViewModel.form.topic(schedule.topic);
          classroomDetailViewModel.form.meeting_link(schedule.meeting_link);
          var facilitator = classroomDetailViewModel.facilitatorList().filter(function (d) {
            return d.user_id === schedule.facilitator_id;
          });
          classroomDetailViewModel.form.facilitator(facilitator[0]);
          classroomDetailViewModel.refreshSelect2();
          $('#datepicker-date').val(classroomDetailViewModel.dateFormat(schedule.start, "YYYY-MM-DD"));
          $('#timepicker-start').val(classroomDetailViewModel.dateFormat(schedule.start, "HH:mm"));
          $('#timepicker-end').val(classroomDetailViewModel.dateFormat(schedule.end, "HH:mm"));
          classroomDetailViewModel.modeUpdate();
          $("#scheduleFormModal").modal('show');
        },
        error: function error(data) {
          classroomDetailViewModel.errorHandler(data);
        }
      });
    } else {
      var facilitator = classroomDetailViewModel.facilitatorList().filter(function (d) {
        return d.user_id === classroomDetailViewModel.page.facilitator.facilitator_id();
      });
      classroomDetailViewModel.form.facilitator(facilitator[0]);
      classroomDetailViewModel.refreshSelect2();
      classroomDetailViewModel.modeCreate();
      $("#scheduleFormModal").modal('show');
    }
  },
  closeModal: function closeModal() {
    $("#scheduleFormModal").modal('toggle');
  },
  openScheduleSubmissionModal: function openScheduleSubmissionModal(scheduleId) {
    classroomDetailViewModel.submissionForm.module(null);
    classroomDetailViewModel.submissionForm.evidence_live_session(null);
    classroomDetailViewModel.submissionForm.is_any_homework(null);
    classroomDetailViewModel.submissionForm.notes(null);
    $('#is-any-homework-toggle').prop('checked', false).change();
    $.ajax({
      url: '/api/schedule/' + scheduleId,
      type: 'GET',
      dataType: 'json',
      headers: {
        'Authorization': 'Bearer ' + classroomDetailViewModel.token
      },
      contentType: 'application/json; charset=utf-8',
      success: function success(data) {
        var schedule = data.data;
        classroomDetailViewModel.submissionForm.module(schedule.module);
        classroomDetailViewModel.submissionForm.evidence_live_session(schedule.evidence_live_session);
        classroomDetailViewModel.submissionForm.is_any_homework(schedule.is_any_homework);
        classroomDetailViewModel.submissionForm.notes(schedule.notes);
        $('#is-any-homework-toggle').prop('checked', schedule.is_any_homework).change();
        $("#facilitatorSubmissionModal").modal('show');
      },
      error: function error(data) {
        classroomDetailViewModel.errorHandler(data);
      }
    });
  },
  openFacilitatorEditModal: function openFacilitatorEditModal(scheduleId) {
    classroomDetailViewModel.submissionForm.module(null);
    classroomDetailViewModel.submissionForm.evidence_live_session(null);
    classroomDetailViewModel.submissionForm.is_any_homework(null);
    classroomDetailViewModel.submissionForm.notes(null);
    $('#is-any-homework-toggle').prop('checked', false).change();
    $.ajax({
      url: '/api/schedule/' + scheduleId,
      type: 'GET',
      dataType: 'json',
      headers: {
        'Authorization': 'Bearer ' + classroomDetailViewModel.token
      },
      contentType: 'application/json; charset=utf-8',
      success: function success(data) {
        var schedule = data.data;
        classroomDetailViewModel.form.schedule_id(schedule.schedule_id);
        classroomDetailViewModel.submissionForm.module(schedule.module);
        classroomDetailViewModel.submissionForm.evidence_live_session(schedule.evidence_live_session);
        classroomDetailViewModel.submissionForm.is_any_homework(schedule.is_any_homework);
        classroomDetailViewModel.submissionForm.notes(schedule.notes);
        $('#is-any-homework-toggle').prop('checked', schedule.is_any_homework).change();
        $("#facilitatorEditModal").modal('show');
      },
      error: function error(data) {
        classroomDetailViewModel.errorHandler(data);
      }
    });
  },
  closeFacilitatorEditModal: function closeFacilitatorEditModal() {
    $("#facilitatorEditModal").modal('toggle');
  },
  openClassroomModalDetail: function openClassroomModalDetail() {
    $("#classroomInfoModal").modal('show');
    classroomDetailViewModel.getDetailClassroom();
  },
  getDetailClassroom: function getDetailClassroom() {
    $.ajax({
      url: '/api/classroom/' + $("#classroomId").val(),
      type: 'GET',
      dataType: 'json',
      headers: {
        'Authorization': 'Bearer ' + classroomDetailViewModel.token
      },
      contentType: 'application/json; charset=utf-8',
      success: function success(data) {
        var classroom = data.data;
        classroomDetailViewModel.page.classroom.classroom_id(classroom.classroom_id);
        classroomDetailViewModel.page.classroom.classroom_name(classroom.classroom_name);
        var facilitator = classroomDetailViewModel.facilitatorList().filter(function (d) {
          return d.user_id === classroom.facilitator_id;
        });
        classroomDetailViewModel.page.facilitator.facilitator_id(classroom.facilitator_id);
        classroomDetailViewModel.page.facilitator.facilitator_name(facilitator[0].name);
        var material = classroomDetailViewModel.materialList().filter(function (d) {
          return d.material_id === classroom.material_id;
        });
        classroomDetailViewModel.page.material.material_id(classroom.material_id);
        classroomDetailViewModel.page.material.name(material[0].name);
        classroomDetailViewModel.page.material.number_of_meetings(material[0].number_of_meetings + 'x ' + material[0].unit_of_meeting);
        classroomDetailViewModel.page.material.min_quota(material[0].min_quota);

        if (classroomDetailViewModel.page.facilitator.facilitator_id() !== classroomDetailViewModel.userId) {
          $('#btn-update-score').hide();
        }
      },
      error: function error(data) {
        classroomDetailViewModel.errorHandler(data);
      }
    });
  },
  deleteSchedule: function deleteSchedule(data) {
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      buttons: {
        confirm: {
          text: 'Yes, delete it!',
          className: 'btn btn-info'
        },
        cancel: {
          visible: true,
          className: 'btn btn-danger'
        }
      }
    }).then(function (Delete) {
      if (Delete) {
        $.ajax({
          url: '/api/schedule/' + data,
          type: 'DELETE',
          dataType: 'json',
          headers: {
            'Authorization': 'Bearer ' + classroomDetailViewModel.token
          },
          contentType: 'application/json; charset=utf-8',
          success: function success(data) {
            table.ajax.reload();
            classroomDetailViewModel.showSuccess(data.message);
          },
          error: function error(data) {
            classroomDetailViewModel.errorHandler(data);
          }
        });
      } else {
        swal.close();
      }
    });
  },
  saveClassroom: function saveClassroom() {
    classroomDetailViewModel.form.start_date($('#datepicker-date').val());
    classroomDetailViewModel.form.start_time($('#timepicker-start').val());
    classroomDetailViewModel.form.end_date($('#datepicker-date').val());
    classroomDetailViewModel.form.end_time($('#timepicker-end').val());

    if (classroomDetailViewModel.errors().length > 0) {
      classroomDetailViewModel.errors.showAllMessages();
      return;
    }

    var data = {
      "topic": classroomDetailViewModel.form.topic(),
      "meeting_link": classroomDetailViewModel.form.meeting_link(),
      "start": classroomDetailViewModel.form.start_date() + ' ' + classroomDetailViewModel.form.start_time(),
      "end": classroomDetailViewModel.form.end_date() + ' ' + classroomDetailViewModel.form.end_time(),
      "classroom_id": classroomDetailViewModel.page.classroom.classroom_id(),
      "facilitator_id": classroomDetailViewModel.form.facilitator().user_id
    };

    if (classroomDetailViewModel.page.isEdit() === true) {
      swal({
        title: 'Are you sure to update this data?',
        icon: 'warning',
        buttons: {
          confirm: {
            text: 'Yes, update it!',
            className: 'btn btn-info'
          },
          cancel: {
            visible: true,
            className: 'btn btn-danger'
          }
        }
      }).then(function (save) {
        if (save) {
          $.ajax({
            url: "/api/schedule/".concat(classroomDetailViewModel.form.schedule_id()),
            type: 'PUT',
            dataType: 'json',
            data: JSON.stringify(data),
            headers: {
              'Authorization': 'Bearer ' + classroomDetailViewModel.token,
              'Content-Type': 'application/json'
            },
            contentType: 'application/json; charset=utf-8',
            success: function success(data) {
              classroomDetailViewModel.closeModal();
              table.ajax.reload();
              classroomDetailViewModel.showSuccess(data.message);
            },
            error: function error(data) {
              var payload = data.responseJSON;
              classroomDetailViewModel.errorHandler(data, [payload.errors.topic, payload.errors.material_id, payload.errors.facilitator_id]);
            }
          });
        } else {
          swal.close();
        }
      });
    } else {
      swal({
        title: 'Are you sure to save this data?',
        icon: 'warning',
        buttons: {
          confirm: {
            text: 'Yes, save it!',
            className: 'btn btn-info'
          },
          cancel: {
            visible: true,
            className: 'btn btn-danger'
          }
        }
      }).then(function (save) {
        if (save) {
          $.ajax({
            url: '/api/schedule',
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify(data),
            headers: {
              'Authorization': 'Bearer ' + classroomDetailViewModel.token,
              'Content-Type': 'application/json'
            },
            contentType: 'application/json; charset=utf-8',
            success: function success(data) {
              classroomDetailViewModel.closeModal();
              table.ajax.reload();
              classroomDetailViewModel.showSuccess(data.message);
            },
            error: function error(data) {
              var payload = data.responseJSON;
              classroomDetailViewModel.errorHandler(data, [payload.errors.topic, payload.errors.meeting_link, payload.errors.facilitator_id, payload.errors.start, payload.errors.end]);
            }
          });
        } else {
          swal.close();
        }
      });
    }
  },
  facilitatorSubmission: function facilitatorSubmission() {
    if (classroomDetailViewModel.submissionErrors().length > 0) {
      classroomDetailViewModel.submissionErrors.showAllMessages();
      return;
    }

    var data = {
      "module": classroomDetailViewModel.submissionForm.module(),
      "evidence_live_session": classroomDetailViewModel.submissionForm.evidence_live_session(),
      "is_any_homework": classroomDetailViewModel.submissionForm.is_any_homework(),
      "notes": classroomDetailViewModel.submissionForm.notes()
    };

    if (classroomDetailViewModel.form.schedule_id()) {
      swal({
        title: 'Are you sure to update this data?',
        icon: 'warning',
        buttons: {
          confirm: {
            text: 'Yes, update it!',
            className: 'btn btn-info'
          },
          cancel: {
            visible: true,
            className: 'btn btn-danger'
          }
        }
      }).then(function (save) {
        if (save) {
          $.ajax({
            url: "/api/schedule/".concat(classroomDetailViewModel.form.schedule_id(), "/complete"),
            type: 'PUT',
            dataType: 'json',
            data: JSON.stringify(data),
            headers: {
              'Authorization': 'Bearer ' + classroomDetailViewModel.token,
              'Content-Type': 'application/json'
            },
            contentType: 'application/json; charset=utf-8',
            success: function success(data) {
              classroomDetailViewModel.closeFacilitatorEditModal();
              table.ajax.reload();
              classroomDetailViewModel.showSuccess(data.message);
            },
            error: function error(data) {
              var payload = data.responseJSON;
              classroomDetailViewModel.errorHandler(data, [payload.errors.module, payload.errors.evidence_live_session, payload.errors.is_any_homework, payload.errors.notes]);
            }
          });
        } else {
          swal.close();
        }
      });
    }
  },
  getMaterialList: function getMaterialList() {
    $.ajax({
      url: '/api/public/material',
      type: 'GET',
      dataType: 'json',
      headers: {
        'Authorization': 'Bearer ' + classroomDetailViewModel.token
      },
      contentType: 'application/json; charset=utf-8',
      success: function success(data) {
        var materials = data.data;
        classroomDetailViewModel.materialList(materials);
      },
      error: function error(data) {
        classroomDetailViewModel.errorHandler(data);
      }
    });
  },
  getFacilitatorList: function getFacilitatorList() {
    $.ajax({
      url: '/api/account/facilitator/list',
      type: 'GET',
      dataType: 'json',
      headers: {
        'Authorization': 'Bearer ' + classroomDetailViewModel.token
      },
      contentType: 'application/json; charset=utf-8',
      success: function success(data) {
        var facilitators = data.data;
        classroomDetailViewModel.facilitatorList(facilitators);
        classroomDetailViewModel.getDetailClassroom();
      },
      error: function error(data) {
        classroomDetailViewModel.errorHandler(data);
      }
    });
  },
  isAnyHomeworkOnChange: function isAnyHomeworkOnChange(data) {
    classroomDetailViewModel.submissionForm.is_any_homework(data);
  },
  openParticipantScoreModal: function openParticipantScoreModal(participant_id, mid_test_score, final_test_score) {
    if (mid_test_score === "null") {
      mid_test_score = null;
    }

    if (final_test_score === "null") {
      final_test_score = null;
    }

    classroomDetailViewModel.participantForm.participant_id(participant_id);
    classroomDetailViewModel.participantForm.mid_test_score(mid_test_score);
    classroomDetailViewModel.participantForm.final_test_score(final_test_score);
    $("#updateScoreModal").modal('show');
  },
  closeParticipantScoreModal: function closeParticipantScoreModal() {
    $("#updateScoreModal").modal('toggle');
  },
  updateScore: function updateScore() {
    var data = {
      mid_test_score: classroomDetailViewModel.participantForm.mid_test_score(),
      final_test_score: classroomDetailViewModel.participantForm.final_test_score()
    };
    console.log(data);
    swal({
      title: 'Are you sure to update this data?',
      icon: 'warning',
      buttons: {
        confirm: {
          text: 'Yes, update it!',
          className: 'btn btn-info'
        },
        cancel: {
          visible: true,
          className: 'btn btn-danger'
        }
      }
    }).then(function (save) {
      if (save) {
        $.ajax({
          url: "/api/participant/".concat(classroomDetailViewModel.participantForm.participant_id()),
          type: 'PUT',
          dataType: 'json',
          data: JSON.stringify(data),
          headers: {
            'Authorization': 'Bearer ' + classroomDetailViewModel.token,
            'Content-Type': 'application/json'
          },
          contentType: 'application/json; charset=utf-8',
          success: function success(data) {
            classroomDetailViewModel.closeParticipantScoreModal();
            participantTable.ajax.reload();
            classroomDetailViewModel.showSuccess(data.message);
          },
          error: function error(data) {
            classroomDetailViewModel.errorHandler(data);
          }
        });
      } else {
        swal.close();
      }
    });
  },
  attedanceColumns: ko.observable([]),
  attedances: ko.observable([]),
  getAttedances: function getAttedances() {
    $.ajax({
      url: '/api/attedance/' + $("#classroomId").val(),
      type: 'GET',
      dataType: 'json',
      headers: {
        'Authorization': 'Bearer ' + classroomDetailViewModel.token,
        'Content-Type': 'application/json'
      },
      contentType: 'application/json; charset=utf-8',
      success: function success(data) {
        var attedanceColumns = [];

        if (data.data.length > 0) {
          for (var i = 0; i < data.data[0].schedules.length; i++) {
            var date = data.data[0].schedules[i].date;
            attedanceColumns.push(classroomDetailViewModel.dateFormat(date, 'DD MMM YYYY'));
          }
        }

        classroomDetailViewModel.attedanceColumns(attedanceColumns);
        classroomDetailViewModel.attedances(data.data);
      },
      error: function error(data) {
        classroomDetailViewModel.errorHandler(data);
      }
    });
  },
  attendSubmission: function attendSubmission(participantId, scheduleId, isAttend) {
    $.ajax({
      url: '/api/attedance',
      type: 'PUT',
      dataType: 'json',
      data: JSON.stringify({
        'participant_id': participantId,
        'schedule_id': scheduleId,
        'is_attend': isAttend
      }),
      headers: {
        'Authorization': 'Bearer ' + classroomDetailViewModel.token,
        'Content-Type': 'application/json'
      },
      contentType: 'application/json; charset=utf-8',
      success: function success(data) {
        classroomDetailViewModel.showSuccess(data.message);
      },
      error: function error(data) {
        classroomDetailViewModel.errorHandler(data);
      }
    });
  },
  onAttendChange: function onAttendChange(obj, event) {
    var isAttend = event.originalEvent.target.checked;
    var participantId = event.originalEvent.target.getAttribute('data-participant');
    var scheduleId = event.originalEvent.target.getAttribute('data-schedule');
    classroomDetailViewModel.attendSubmission(participantId, scheduleId, isAttend);
  }
});

classroomDetailViewModel.errors = ko.validation.group(classroomDetailViewModel.form);
classroomDetailViewModel.submissionErrors = ko.validation.group(classroomDetailViewModel.submissionForm);
ko.applyBindings(classroomDetailViewModel);