"use strict";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var table;
$(document).ready(function () {
  classroomViewModel.init();
});

var classroomViewModel = _objectSpread({}, commonViewModel, {
  init: function init() {
    table = $('#add-row').DataTable({
      "processing": true,
      "serverSide": true,
      "pageLength": 5,
      "lengthChange": false,
      "ajax": {
        'url': '/api/classroom',
        'type': 'GET',
        'beforeSend': function beforeSend(request) {
          request.setRequestHeader("Authorization", "Bearer " + localStorage.getItem("token"));
        },
        'error': function error(data, _error, code) {
          classroomViewModel.errorHandler(data);
        }
      },
      "order": [[4, 'desc']],
      "columns": [{
        data: "classroom_name",
        render: function render(data, type, full, meta) {
          var assignSchedule = document.createElement("a");
          assignSchedule.setAttribute('href', "/lms/classroom/" + full.classroom_id + "");
          assignSchedule.appendChild(document.createTextNode(data));
          return assignSchedule.outerHTML;
        }
      }, {
        data: "material_name"
      }, {
        data: "facilitator_name"
      }, {
        data: "status",
        orderable: false,
        render: function render(data, type, full, meta) {
          var className = '';
          var text = '';

          if (data === 1) {
            text = "Open";
            className = "badge badge-success";
          } else if (data === 2) {
            text = "Close";
            className = "badge badge-danger";
          } else {
            text = "Draft";
            className = "badge badge-warning";
          }

          var span = document.createElement("span");
          span.className = className;
          var text = document.createTextNode(text);
          span.appendChild(text);
          return span.outerHTML;
        }
      }, {
        data: "created_at",
        render: function render(data, type, full, meta) {
          return classroomViewModel.dateFormat(data);
        }
      }, {
        data: "updated_at",
        render: function render(data, type, full, meta) {
          return classroomViewModel.dateFormat(data);
        }
      }, {
        data: "classroom_id",
        defaultContent: '',
        orderable: false,
        visible: classroomViewModel.isAdmin(),
        render: function render(data, type, full, meta) {
          var div = document.createElement("div");
          div.className = "form-button-action";
          var btnEdit = document.createElement("a");
          btnEdit.className = "btn btn-link btn-primary";
          btnEdit.setAttribute('onclick', "classroomViewModel.openModal('" + data + "')");
          var iconEdit = document.createElement("i");
          iconEdit.className = "fa fa-edit";
          btnEdit.appendChild(iconEdit);
          var btnDelete = document.createElement("button");
          btnDelete.className = "btn btn-link btn-danger";
          btnDelete.setAttribute('onclick', "classroomViewModel.deleteClassroom('" + data + "')");
          var iconDelete = document.createElement("i");
          iconDelete.className = "fa fa-times";
          btnDelete.appendChild(iconDelete);
          div.appendChild(btnEdit);
          div.appendChild(btnDelete);
          return div.outerHTML;
        }
      }]
    });
    classroomViewModel.getMaterialList();
    classroomViewModel.getFacilitatorList();
  },
  materialList: ko.observableArray([]),
  facilitatorList: ko.observableArray([]),
  statusList: ko.observableArray([{
    value: 0,
    text: 'Draft'
  }, {
    value: 1,
    text: 'Open'
  }, {
    value: 2,
    text: 'Close'
  }]),
  modeCreate: function modeCreate() {
    classroomViewModel.page.isEdit(false);
    classroomViewModel.page.title("New");
    classroomViewModel.page.description("Create a new classroom using this form, make sure you fill them all");
    classroomViewModel.clearForm();
  },
  modeUpdate: function modeUpdate() {
    classroomViewModel.page.isEdit(true);
    classroomViewModel.page.title("Update");
    classroomViewModel.page.description("Update classroom using this form, make sure you fill them all");
  },
  form: {
    classroom_id: ko.observable(null).extend(),
    classroom_name: ko.observable(null).extend({
      required: true
    }),
    material: ko.observable(null).extend({
      required: true
    }),
    facilitator: ko.observable(null).extend({
      required: true
    }),
    status: ko.observable(null).extend({
      required: true
    })
  },
  page: {
    isEdit: ko.observable(false),
    title: ko.observable(null),
    description: ko.observable(null)
  },
  clearForm: function clearForm() {
    classroomViewModel.form.classroom_id(null);
    classroomViewModel.form.classroom_name(null);
    classroomViewModel.form.material(null);
    classroomViewModel.form.facilitator(null);
    classroomViewModel.errors.showAllMessages(false);
  },
  openModal: function openModal(classroomId) {
    if (classroomId !== null) {
      $.ajax({
        url: '/api/classroom/' + classroomId,
        type: 'GET',
        dataType: 'json',
        headers: {
          'Authorization': 'Bearer ' + classroomViewModel.token
        },
        contentType: 'application/json; charset=utf-8',
        success: function success(data) {
          var classroom = data.data;
          classroomViewModel.form.classroom_id(classroom.classroom_id);
          classroomViewModel.form.classroom_name(classroom.classroom_name);
          var facilitator = classroomViewModel.facilitatorList().filter(function (d) {
            return d.user_id === classroom.facilitator_id;
          });
          classroomViewModel.form.facilitator(facilitator[0]);
          var material = classroomViewModel.materialList().filter(function (d) {
            return d.material_id === classroom.material_id;
          });
          classroomViewModel.form.material(material[0]);
          var status = classroomViewModel.statusList().filter(function (d) {
            return d.value === classroom.status;
          });
          classroomViewModel.form.status(status[0]);
          classroomViewModel.refreshSelect2();
          classroomViewModel.modeUpdate();
          $('#classroomFormModal').modal('show');
        },
        error: function error(data) {
          classroomViewModel.errorHandler(data);
        }
      });
    } else {
      classroomViewModel.modeCreate();
      $('#classroomFormModal').modal('show');
    }
  },
  closeModal: function closeModal() {
    $('#classroomFormModal').modal('toggle');
  },
  deleteClassroom: function deleteClassroom(data) {
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      buttons: {
        confirm: {
          text: 'Yes, delete it!',
          className: 'btn btn-info'
        },
        cancel: {
          visible: true,
          className: 'btn btn-danger'
        }
      }
    }).then(function (Delete) {
      if (Delete) {
        $.ajax({
          url: '/api/classroom/' + data,
          type: 'DELETE',
          dataType: 'json',
          headers: {
            'Authorization': 'Bearer ' + classroomViewModel.token
          },
          contentType: 'application/json; charset=utf-8',
          success: function success(data) {
            table.ajax.reload();
            classroomViewModel.showSuccess(data.message);
          },
          error: function error(data) {
            classroomViewModel.errorHandler(data);
          }
        });
      } else {
        swal.close();
      }
    });
  },
  saveClassroom: function saveClassroom() {
    if (classroomViewModel.errors().length > 0) {
      classroomViewModel.errors.showAllMessages();
      return;
    }

    var data = {
      "classroom_name": classroomViewModel.form.classroom_name(),
      "material_id": classroomViewModel.form.material().material_id,
      "facilitator_id": classroomViewModel.form.facilitator().user_id,
      "status": classroomViewModel.form.status().value
    };

    if (classroomViewModel.page.isEdit() === true) {
      swal({
        title: 'Are you sure to update this data?',
        icon: 'warning',
        buttons: {
          confirm: {
            text: 'Yes, update it!',
            className: 'btn btn-info'
          },
          cancel: {
            visible: true,
            className: 'btn btn-danger'
          }
        }
      }).then(function (save) {
        if (save) {
          $.ajax({
            url: "/api/classroom/".concat(classroomViewModel.form.classroom_id()),
            type: 'PUT',
            dataType: 'json',
            data: JSON.stringify(data),
            headers: {
              'Authorization': 'Bearer ' + classroomViewModel.token,
              'Content-Type': 'application/json'
            },
            contentType: 'application/json; charset=utf-8',
            success: function success(data) {
              classroomViewModel.closeModal();
              table.ajax.reload();
              classroomViewModel.showSuccess(data.message);
            },
            error: function error(data) {
              var payload = data.responseJSON;
              classroomViewModel.errorHandler(data, [payload.errors.classroom_name, payload.errors.material_id, payload.errors.facilitator_id, payload.errors.status]);
            }
          });
        } else {
          swal.close();
        }
      });
    } else {
      swal({
        title: 'Are you sure to save this data?',
        icon: 'warning',
        buttons: {
          confirm: {
            text: 'Yes, save it!',
            className: 'btn btn-info'
          },
          cancel: {
            visible: true,
            className: 'btn btn-danger'
          }
        }
      }).then(function (save) {
        if (save) {
          $.ajax({
            url: '/api/classroom',
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify(data),
            headers: {
              'Authorization': 'Bearer ' + classroomViewModel.token,
              'Content-Type': 'application/json'
            },
            contentType: 'application/json; charset=utf-8',
            success: function success(data) {
              classroomViewModel.closeModal();
              table.ajax.reload();
              classroomViewModel.showSuccess(data.message);
            },
            error: function error(data) {
              var payload = data.responseJSON;
              classroomViewModel.errorHandler(data, [payload.errors.classroom_name, payload.errors.material_id, payload.errors.facilitator_id, payload.errors.status]);
            }
          });
        } else {
          swal.close();
        }
      });
    }
  },
  getMaterialList: function getMaterialList() {
    $.ajax({
      url: '/api/public/material',
      type: 'GET',
      dataType: 'json',
      headers: {
        'Authorization': 'Bearer ' + classroomViewModel.token
      },
      contentType: 'application/json; charset=utf-8',
      success: function success(data) {
        var materials = data.data;
        classroomViewModel.materialList(materials);
      },
      error: function error(data) {
        classroomViewModel.errorHandler(data);
      }
    });
  },
  getFacilitatorList: function getFacilitatorList() {
    $.ajax({
      url: '/api/account/facilitator/list',
      type: 'GET',
      dataType: 'json',
      headers: {
        'Authorization': 'Bearer ' + classroomViewModel.token
      },
      contentType: 'application/json; charset=utf-8',
      success: function success(data) {
        var facilitators = data.data;
        classroomViewModel.facilitatorList(facilitators);
      },
      error: function error(data) {
        classroomViewModel.errorHandler(data);
      }
    });
  }
});

classroomViewModel.errors = ko.validation.group(classroomViewModel.form);
ko.applyBindings(classroomViewModel);