<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Classroom;
use App\Models\User;
use App\Models\Material;
use App\Common\Role;
use App\Common\ClassroomConst;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use Ramsey\Uuid\Uuid as Generator;
use Illuminate\Support\Facades\DB;
use JWTAuth;

class ClassroomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pagination(Request $request)
    {
        Log::info($request);
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // Rows display per page

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name 
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value


        $userLogged = JWTAuth::user();
        $status = [0,1,2];
        if(Role::ADMIN !== $userLogged->role){
            $status = [1];
        }

        // Total records
        $totalRecords = Classroom::select('count(*) as allcount')
            ->whereIn('status', $status)
            ->count();
        $totalRecordswithFilter = Classroom::select('count(*) as allcount')
            ->where('classroom_name', 'like', '%' .$searchValue . '%')
            ->whereIn('status', $status)
            ->count();

        // Fetch records
        $records = Classroom::join('users', 'users.user_id', '=', 'classroom.facilitator_id')
            ->join('material', 'material.material_id', '=', 'classroom.material_id')
            ->orderBy($columnName,$columnSortOrder)
            ->where('classroom_name', 'like', '%' .$searchValue . '%')
            ->whereIn('status', $status)
            ->skip($start)
            ->take($rowperpage)
            ->get([
                'classroom.classroom_id as classroom_id',
                'classroom.classroom_name as classroom_name',
                'classroom.created_at as created_at',
                'classroom.updated_at as updated_at', 
                'classroom.status as status',
                'classroom.facilitator_id as facilitator_id', 
                'users.name as facilitator_name', 
                'material.material_id as material_id', 
                'material.name as material_name'
            ]);

        $data_arr = array();
        $sno = $start+1;
        foreach($records as $record){
            $classroom_id = $record->classroom_id;
            $classroom_name = $record->classroom_name;
            $facilitator_id = $record->facilitator_id;
            $facilitator_name = $record->facilitator_name;
            $material_id = $record->material_id;
            $material_name = $record->material_name;
            $created_at = $record->created_at;
            $updated_at = $record->updated_at;
            $status = $record->status;

            $data_arr[] = array(
                "classroom_id" => $classroom_id,
                "classroom_name" => $classroom_name,
                "facilitator_id" => $facilitator_id,
                "facilitator_name" => $facilitator_name,
                "status" => $status,
                "material_id" => $material_id,
                "material_name" => $material_name,
                "created_at" => $created_at,
                "updated_at" => $updated_at
            );
        }

        return response()->json([
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data_arr
        ]);;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $classrooms = Classroom::orderBy("classroom_name","asc")->where("status","=",ClassroomConst::OPEN)->get();
        return response()->json([
            'data' => $classrooms
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $classroomId
     * @return \Illuminate\Http\Response
     */
    public function get($classroomId)
    {
        Log::info('REQUEST TO GET CLASSROOM');
        $classroom = Classroom::find($classroomId);
        if($classroom === null){
            return response()->json(['message' => 'Classroom not found'], 404);
        }
        
        return response()->json([
            'data' => $classroom
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        Log::info('REQUEST TO CREATE CLASSROOM');
        $validator = Validator::make($request->all(), [
            'classroom_name' => 'required|string|max:100',
            'facilitator_id' => 'required|string',
            'material_id' => 'required|string',
            'status' => 'required|integer',
        ]);
        if($validator->fails()){
            return response()->json([
                'code' => 'validation_data',
                'message' => 'The given data is invalid',
                'errors' => $validator->errors(),
            ], 400);
        }

        // Validate facilitator
        $user = User::where('user_id', '=', $request->facilitator_id)->first();
        if($user === null){
            return response()->json(['message' => 'Facilitator not found'], 500);
        } else {
            if($user->role !== Role::FACILITATOR){
                return response()->json(['message' => 'Invalid facilitator'], 500);
            }
        }
        
        // Validate material
        $material = Material::where('material_id', '=', $request->material_id)->first();
        if($material === null){
            return response()->json(['message' => 'Material not found'], 500);
        }

        try {
            $classroom = new Classroom();
            $classroom->classroom_id = Generator::uuid4()->toString();
            $classroom->classroom_name = $request->classroom_name;
            $classroom->facilitator_id = $request->facilitator_id;
            $classroom->material_id = $request->material_id;
            $classroom->status = $request->status;
            $classroom->save();
            Log::info($classroom);

            return response()->json([
                'message' => 'Classroom is successfully created',
                'data' => $classroom
            ], 201);

        } catch (Throwable $e){
            Log::error($e);
            return response()->json(['message' => 'Failed to save'], 500);
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $classroomId
     * @return \Illuminate\Http\Response
     */
    public function update($classroomId, Request $request)
    {
        Log::info('REQUEST TO UPDATE CLASSROOM : ' .$classroomId);

        $validator = Validator::make($request->all(), [
            'classroom_name' => 'required|string|max:100',
            'facilitator_id' => 'required|string',
            'material_id' => 'required|string',
            'status' => 'required|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'code' => 'validation_data',
                'message' => 'The given data is invalid',
                'errors' => $validator->errors(),
            ], 400);
        }

        // Validate facilitator
        $user = User::where('user_id', '=', $request->facilitator_id)->first();
        if($user === null){
            return response()->json(['message' => 'Facilitator not found'], 500);
        } else {
            if($user->role !== Role::FACILITATOR){
                return response()->json(['message' => 'Invalid facilitator'], 500);
            }
        }
        
        // Validate material
        $material = Material::where('material_id', '=', $request->material_id)->first();
        if($user === null){
            return response()->json(['message' => 'Material not found'], 500);
        }

        // Find classroom
        $classroom = Classroom::find($classroomId);
        if($classroom === null){
            return response()->json(['message' => 'Classroom not found'], 404);
        }

        try {
            $classroom->classroom_name = $request->classroom_name;
            $classroom->facilitator_id = $request->facilitator_id;
            $classroom->material_id = $request->material_id;
            $classroom->status = $request->status;
            $classroom->save();

            return response()->json([
                'message' => 'Classroom is successfully updated',
                'data' => $classroom,
            ], 200);
        } catch (Throwable $e){
            Log::error($e);
            return response()->json(['message' => 'Failed to save'], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $classroomId
     * @return \Illuminate\Http\Response
     */
    public function delete($classroomId)
    {
        $classroom = Classroom::find($classroomId);
        if($classroom === null){
            return response()->json(['message' => 'Classroom not found'], 404);
        }

        try {
            $classroom -> delete();
            return response()->json(['message' => 'Classroom is successfully deleted'], 200);
        } catch (Throwable $e){
            Log::error($e);
            return response()->json(['message' => 'Failed to delete'], 500);
        }
    }

    public function openClassroom($classroomId, Request $request)
    {
        Log::info('REQUEST TO OPEN CLASSROOM : ' .$classroomId);

        // Find classroom
        $classroom = Classroom::find($classroomId);
        if($classroom === null){
            return response()->json(['message' => 'Classroom not found'], 404);
        }

        try {
            $classroom->status = 1;
            $classroom->save();

            return response()->json([
                'message' => 'Classroom has opened',
                'data' => $classroom,
            ], 200);
        } catch (Throwable $e){
            Log::error($e);
            return response()->json(['message' => 'Failed to save'], 500);
        }
    }

    public function closeClassroom($classroomId, Request $request)
    {
        Log::info('REQUEST TO CLOSE CLASSROOM : ' .$classroomId);

        // Find classroom
        $classroom = Classroom::find($classroomId);
        if($classroom === null){
            return response()->json(['message' => 'Classroom not found'], 404);
        }

        try {
            $classroom->status = 2;
            $classroom->save();

            return response()->json([
                'message' => 'Classroom has closed',
                'data' => $classroom,
            ], 200);
        } catch (Throwable $e){
            Log::error($e);
            return response()->json(['message' => 'Failed to save'], 500);
        }
    }
}
