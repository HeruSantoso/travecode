<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Ramsey\Uuid\Uuid as Generator;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['message' => 'Incorrect email or password'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['message' => 'Login Failed, please try again later'], 500);
        }

        $user = User::where('email', '=', $request->email)->first();

        return response()->json(compact('user','token'));
    }

}
