<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Participant;
use App\Models\Classroom;
use App\Models\User;
use App\Common\Role;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use Ramsey\Uuid\Uuid as Generator;
use JWTAuth;
use \stdClass;

class ParticipantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pagination(Request $request)
    {
        Log::info($request);
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // Rows display per page

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');
        $classroom_id = $request->get('classroom_id');

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name 
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value
        $classroomIdValue = $classroom_id; // Search value

        // Total records
        $totalRecords = Participant::select('count(*) as allcount')
            ->where('classroom_id', 'like', '%' .$classroomIdValue . '%')
            ->count();
        $totalRecordswithFilter = Participant::select('count(*) as allcount')
            ->join('users', 'users.user_id', '=', 'participant.student_id')
            ->where('users.name', 'like', '%' .$searchValue . '%')
            ->where('classroom_id', 'like', '%' .$classroomIdValue . '%')
            ->count();

        // Fetch records
        $records = Participant::join('users', 'users.user_id', '=', 'participant.student_id')
        ->orderBy($columnName,$columnSortOrder)
        ->where('users.name', 'like', '%' .$searchValue . '%')
        ->where('participant.classroom_id', 'like', '%' .$classroomIdValue . '%')
        ->skip($start)
        ->take($rowperpage)
        ->get([
            'participant.participant_id as participant_id',
            'participant.classroom_id as classroom_id',
            'participant.classroom_registration_id as classroom_registration_id',
            'participant.mid_test_score as mid_test_score',
            'participant.final_test_score as final_test_score',
            'participant.student_id as student_id',
            'users.name as student_name'
        ]);

        $data_arr = array();
        $sno = $start+1;
        foreach($records as $record){
            $participant_id = $record->participant_id;
            $classroom_id = $record->classroom_id;
            $classroom_registration_id = $record->classroom_registration_id;
            $student_id = $record->student_id;
            $student_name = $record->student_name;
            $mid_test_score = $record->mid_test_score;
            $final_test_score = $record->final_test_score;

            $data_arr[] = array(
                "participant_id" => $participant_id,
                "classroom_id" => $classroom_id,
                "classroom_registration_id" => $classroom_registration_id,
                "mid_test_score" => $mid_test_score,
                "final_test_score" => $final_test_score,
                "student_id" => $student_id,
                "student_name" => $student_name
            );
        }

        return response()->json([
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data_arr
        ]);;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $participantId
     * @return \Illuminate\Http\Response
     */
    public function delete($participantId)
    {
        $participant = Participant::find($participantId);
        if($participant === null){
            return response()->json(['message' => 'Participant not found'], 404);
        }

        try {
            $participant -> delete();
            return response()->json(['message' => 'Participant is successfully deleted'], 200);
        } catch (Throwable $e){
            Log::error($e);
            return response()->json(['message' => 'Failed to delete'], 500);
        }
    }

    public function updateScore($participantId, Request $request)
    {
        Log::info('REQUEST TO CREATE Material');
        $validator = Validator::make($request->all(), [
            'mid_test_score' => 'integer|nullable',
            'final_test_score' => 'integer|nullable',
        ]);
        if($validator->fails()){
            return response()->json([
                'code' => 'validation_data',
                'message' => 'The given data is invalid',
                'errors' => $validator->errors(),
            ], 400);
        }

        $participant = Participant::find($participantId);
        if($participant === null){
            return response()->json(['message' => 'Participant not found'], 404);
        }

        try {
            $participant->mid_test_score = $request->mid_test_score;
            $participant->final_test_score = $request->final_test_score;
            $participant->save();
            Log::info($participant);

            return response()->json([
                'message' => 'Participant is successfully updated',
                'data' => $participant
            ], 201);

        } catch (Throwable $e){
            Log::error($e);
            return response()->json(['message' => 'Failed to save'], 500);
        }
    }

}
