<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Material;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use Ramsey\Uuid\Uuid as Generator;

class MaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pagination(Request $request)
    {
        Log::info($request);
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // Rows display per page

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name 
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value

        // Total records
        $totalRecords = Material::select('count(*) as allcount')->count();
        $totalRecordswithFilter = Material::select('count(*) as allcount')->where('name', 'like', '%' .$searchValue . '%')->count();

        // Fetch records
        $records = Material::orderBy($columnName,$columnSortOrder)
            ->where('name', 'like', '%' .$searchValue . '%')
            ->skip($start)
            ->take($rowperpage)
            ->get();

        $data_arr = array();
        $sno = $start+1;
        foreach($records as $record){
            $material_id = $record->material_id;
            $name = $record->name;
            $number_of_meetings = $record->number_of_meetings;
            $unit_of_meeting = $record->unit_of_meeting;
            $min_quota = $record->min_quota;
            $created_at = $record->created_at;
            $updated_at = $record->updated_at;

            $data_arr[] = array(
                "material_id" => $material_id,
                "name" => $name,
                "number_of_meetings" => $number_of_meetings,
                "unit_of_meeting" => $unit_of_meeting,
                "min_quota" => $min_quota,
                "created_at" => $created_at,
                "updated_at" => $updated_at
            );
        }

        return response()->json([
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data_arr
        ]);;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $materials = Material::orderBy("name","asc")->get();
        return response()->json([
            'data' => $materials
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $materialId
     * @return \Illuminate\Http\Response
     */
    public function get($materialId)
    {
        Log::info('REQUEST TO GET Material');
        $material = Material::find($materialId);
        if($material === null){
            return response()->json(['message' => 'Material not found'], 404);
        }
        
        return response()->json([
            'data' => $material
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        Log::info('REQUEST TO CREATE Material');
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:100',
            'description' => 'required|string|max:255',
            'picture' => 'required|string|max:255',
            'category' => 'string|max:50',
            'number_of_meetings' => 'required|integer',
            'unit_of_meeting' => 'required|string|max:50',
            'min_quota' => 'required|integer',
            'min_age' => 'required|integer',
            'max_age' => 'required|integer',
        ]);
        if($validator->fails()){
            return response()->json([
                'code' => 'validation_data',
                'message' => 'The given data is invalid',
                'errors' => $validator->errors(),
            ], 400);
        }

        try {
            $material = new Material();
            $material->material_id = Generator::uuid4()->toString();
            $material->name = $request->name;
            $material->description = $request->description;
            $material->picture = $request->picture;
            $material->category = $request->category;
            $material->number_of_meetings = $request->number_of_meetings;
            $material->unit_of_meeting = $request->unit_of_meeting;
            $material->min_quota = $request->min_quota;
            $material->min_age = $request->min_age;
            $material->max_age = $request->max_age;
            $material->save();
            Log::info($material);

            return response()->json([
                'message' => 'Material is successfully created',
                'data' => $material
            ], 201);

        } catch (Throwable $e){
            Log::error($e);
            return response()->json(['message' => 'Failed to save'], 500);
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $materialId
     * @return \Illuminate\Http\Response
     */
    public function update($materialId, Request $request)
    {
        Log::info('REQUEST TO UPDATE Material : ' .$materialId);

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:100',
            'description' => 'required|string|max:255',
            'picture' => 'required|string|max:255',
            'category' => 'string|max:50',
            'number_of_meetings' => 'required|integer',
            'unit_of_meeting' => 'required|string|max:50',
            'min_quota' => 'required|integer',
            'min_age' => 'required|integer',
            'max_age' => 'required|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'code' => 'validation_data',
                'message' => 'The given data is invalid',
                'errors' => $validator->errors(),
            ], 400);
        }

        $material = Material::find($materialId);
        if($material === null){
            return response()->json(['message' => 'Material not found'], 404);
        }

        try {
            $material->name = $request->name;
            $material->description = $request->description;
            $material->picture = $request->picture;
            $material->category = $request->category;
            $material->number_of_meetings = $request->number_of_meetings;
            $material->unit_of_meeting = $request->unit_of_meeting;
            $material->min_quota = $request->min_quota;
            $material->min_age = $request->min_age;
            $material->max_age = $request->max_age;
            $material->save();

            return response()->json([
                'message' => 'Material is successfully updated',
                'data' => $material,
            ], 200);
        } catch (Throwable $e){
            Log::error($e);
            return response()->json(['message' => 'Failed to save'], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $materialId
     * @return \Illuminate\Http\Response
     */
    public function delete($materialId)
    {
        $material = Material::find($materialId);
        if($material === null){
            return response()->json(['message' => 'Material not found'], 404);
        }

        try {
            $material -> delete();
            return response()->json(['message' => 'Material is successfully deleted'], 200);
        } catch (Throwable $e){
            Log::error($e);
            return response()->json(['message' => 'Failed to delete'], 500);
        }
    }
}
