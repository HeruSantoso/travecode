<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Common\Role;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use Ramsey\Uuid\Uuid as Generator;
use Illuminate\Support\Facades\Hash;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pagination(Request $request)
    {
        Log::info($request);
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // Rows display per page

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name 
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value

        // Total records
        $totalRecords = User::select('count(*) as allcount')
            ->where('role', '=', Role::STUDENT)
            ->count();
        $totalRecordswithFilter = User::select('count(*) as allcount')
            ->where('name', 'like', '%' .$searchValue . '%')
            ->where('role', '=', Role::STUDENT)
            ->count();

        // Fetch records
        $records = User::orderBy($columnName,$columnSortOrder)
            ->where('name', 'like', '%' .$searchValue . '%')
            ->where('role', '=', Role::STUDENT)
            ->skip($start)
            ->take($rowperpage)
            ->get();

        $data_arr = array();
        $sno = $start+1;
        foreach($records as $record){
            $user_id = $record->user_id;
            $name = $record->name;
            $email = $record->email;
            $dob = $record->dob;
            $grade = $record->grade;
            $phone_number = $record->phone_number;
            $created_at = $record->created_at;
            $updated_at = $record->updated_at;

            $data_arr[] = array(
                "user_id" => $user_id,
                "name" => $name,
                "email" => $email,
                "dob" => $dob,
                "grade" => $grade,
                "phone_number" => $phone_number,
                "created_at" => $created_at,
                "updated_at" => $updated_at
            );
        }

        return response()->json([
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data_arr
        ]);;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $students = User::orderBy("name","asc")
            ->where('role', '=', Role::STUDENT)
            ->get();
        return response()->json([
            'data' => $students
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $studentId
     * @return \Illuminate\Http\Response
     */
    public function get($studentId)
    {
        Log::info('REQUEST TO GET User');
        $student = User::find($studentId);
        if($student === null){
            return response()->json(['message' => 'Student not found'], 404);
        } else {
            if ($student->role !== Role::STUDENT){
                return response()->json(['message' => 'This account is not student'], 400);
            }
        }
        
        return response()->json([
            'data' => $student
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        Log::info('REQUEST TO CREATE User');
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'dob' => 'required|date',
            'grade' => 'required|string',
            'phone_number' => 'required|string'
        ]);
        if($validator->fails()){
            return response()->json([
                'code' => 'validation_data',
                'message' => 'The given data is invalid',
                'errors' => $validator->errors(),
            ], 400);
        }

        try {
            $student = new User();
            $student->user_id = Generator::uuid4()->toString();
            $student->name = $request->name;
            $student->email = strtolower($request->email);
            $student->password = Hash::make($request->password);
            $student->dob = $request->dob;
            $student->grade = $request->grade;
            $student->phone_number = $request->phone_number;
            $student->role = Role::STUDENT;
            $student->save();
            Log::info($student);

            return response()->json([
                'message' => 'User is successfully created',
                'data' => $student
            ], 201);

        } catch (Throwable $e){
            Log::error($e);
            return response()->json(['message' => 'Failed to save'], 500);
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $studentId
     * @return \Illuminate\Http\Response
     */
    public function update($studentId, Request $request)
    {
        Log::info('REQUEST TO UPDATE User : ' .$studentId);

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'dob' => 'required|date',
            'grade' => 'required|string',
            'phone_number' => 'required|string'
        ]);;

        if($validator->fails()){
            return response()->json([
                'code' => 'validation_data',
                'message' => 'The given data is invalid',
                'errors' => $validator->errors(),
            ], 400);
        }

        $student = User::find($studentId);
        if($student === null){
            return response()->json(['message' => 'User not found'], 404);
        } else {
            if ($student->role !== Role::STUDENT){
                return response()->json(['message' => 'This account is not student'], 400);
            }

            if ($student->email !== $request->email){
                $studentFound = User::where('email','=',strtolower($request->email)) -> first();
                if($studentFound !== null){
                    return response()->json(['message' => 'Email is already used'], 400);
                }
            }
        }

        try {
            $student->name = $request->name;
            $student->email = strtolower($request->email);
            $student->dob = $request->dob;
            $student->grade = $request->grade;
            $student->phone_number = $request->phone_number;
            $student->role = Role::STUDENT;
            $student->save();

            return response()->json([
                'message' => 'User is successfully updated',
                'data' => $student,
            ], 200);
        } catch (Throwable $e){
            Log::error($e);
            return response()->json(['message' => 'Failed to save'], 500);
        }
    }

}
