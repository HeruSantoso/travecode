<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Schedule;
use App\Models\Classroom;
use App\Models\User;
use App\Models\Participant;
use App\Models\Attedance;
use App\Common\Role;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use Ramsey\Uuid\Uuid as Generator;
use JWTAuth;
use \stdClass;

class AttedanceController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param  int  $scheduleId
     * @return \Illuminate\Http\Response
     */
    public function list($classroomId)
    {
        Log::info('REQUEST TO GET SCHEDULE');
        $schedules = Schedule::where('classroom_id', '=', $classroomId)
            ->orderBy("start","asc")->get([
                'schedule.schedule_id',
                'schedule.date',
                'schedule.start',
                'schedule.end',
                'schedule.classroom_id',
                'schedule.facilitator_id',
            ]);

        $participants = Participant::join('users', 'users.user_id', '=', 'participant.student_id')
            ->where('classroom_id','=',$classroomId)
            ->orderBy("student_name","asc")
            ->get([
                'participant.participant_id',
                'participant.student_id',
                'users.name as student_name',
            ]);

        $attedances = Attedance::join('schedule', 'schedule.schedule_id', '=', 'attedance.schedule_id')
            ->where('schedule.classroom_id','=',$classroomId)->get([
                'attedance.attedance_id',
                'attedance.is_attend',
                'attedance.participant_id',
                'attedance.schedule_id',
                'attedance.notes',
            ]);

        $data_arr = array();
        foreach($participants as $participant){

            // Get schedule list
            $scheduleList = array();
            foreach($schedules as $sch){

                // Get attendance
                $att = new stdClass();
                $att->is_attend = false;

                foreach($attedances as $atted) {
                    if ($atted->schedule_id === $sch->schedule_id && $atted->participant_id === $participant->participant_id) {
                        $att = $atted;
                    }
                }

                $scheduleList[] = array(
                    'schedule_id' => $sch->schedule_id,
                    'date' => $sch->date,
                    'start' => $sch->start,
                    'end' => $sch->end,
                    'classroom_id' => $sch->classroom_id,
                    'facilitator_id' => $sch->facilitator_id,
                    'attedance' => $att
                );
            }
            
            $data_arr[] = array(
                'participant_id' => $participant->participant_id,
                'student_id' => $participant->student_id,
                'student_name' => $participant->student_name,
                'schedules' => $scheduleList
            );

        }

        
        return response()->json([
            'data' => $data_arr
        ]);
    }

    public function attending(Request $request)
    {
        try {        
            $validator = Validator::make($request->all(), [
                'participant_id' => 'required|string',
                'schedule_id' => 'required|string',
                'is_attend' => 'required|boolean',
            ]);
            if($validator->fails()){
                return response()->json([
                    'code' => 'validation_data',
                    'message' => 'The given data is invalid',
                    'errors' => $validator->errors(),
                ], 400);
            }

            $scheduleId = $request->schedule_id;
            $participantId = $request->participant_id;
            $isAttend = $request->is_attend;

            $schedule = Schedule::find($scheduleId);
            if($schedule === null){
                return response()->json(['message' => 'Schedule not found'], 404);
            }

            $participant = Participant::find($participantId);
            if($participant === null){
                return response()->json(['message' => 'Participant not found'], 404);
            }

            $attedance = Attedance::where('schedule_id','=',$scheduleId)->where('participant_id','=',$participantId)->first();
            if($attedance === null){
                $attedance = new Attedance();
                $attedance->attedance_id = Generator::uuid4()->toString();
            }
            
            $attedance->participant_id = $participantId;
            $attedance->schedule_id = $scheduleId;
            $attedance->is_attend = $isAttend;
            $attedance->notes = 'Verified';
            $attedance->save();

            return response()->json([
                'message' => 'Attending submission success',
                'data' => $attedance,
            ], 200);
        } catch (Throwable $e){
            Log::error($e);
            return response()->json(['message' => 'Failed to save'], 500);
        }
    }
    
}
