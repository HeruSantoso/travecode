<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ClassroomRegistration;
use App\Models\User;
use App\Models\Classroom;
use App\Models\Participant;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use Ramsey\Uuid\Uuid as Generator;
use App\Common\ClassroomRegistrationConst;
use App\Common\Role;
use JWTAuth;

class ClassroomRegistrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pagination(Request $request)
    {
        Log::info($request);
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // Rows display per page

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name 
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value

        $studentId = "";
        $userLogged = JWTAuth::user();
        if ($userLogged->role === Role::STUDENT) {
            $studentId = $userLogged -> user_id;
        }

        // Total records
        $totalRecords = ClassroomRegistration::select('count(*) as allcount')
            ->where('classroom_registration.student_id', 'like', '%' .$studentId . '%')
            ->count();
        $totalRecordswithFilter = ClassroomRegistration::select('count(*) as allcount')
            ->join('users', 'users.user_id', '=', 'classroom_registration.student_id')
            ->where('users.name', 'like', '%' .$searchValue . '%')
            ->where('classroom_registration.student_id', 'like', '%' .$studentId . '%')
            ->count();

        // Fetch records
        $records = ClassroomRegistration::join('users', 'users.user_id', '=', 'classroom_registration.student_id')
            ->join('classroom', 'classroom.classroom_id', '=', 'classroom_registration.classroom_id')
            ->orderBy($columnName,$columnSortOrder)
            ->where('users.name', 'like', '%' .$searchValue . '%')
            ->where('classroom_registration.student_id', 'like', '%' .$studentId . '%')
            ->skip($start)
            ->take($rowperpage)
            ->get([
                'classroom_registration.classroom_registration_id as classroom_registration_id',
                'classroom_registration.status as status',
                'classroom_registration.created_at as created_at',
                'classroom_registration.updated_at as updated_at',
                'users.user_id as student_id',
                'users.email as email',
                'users.phone_number as phone_number',
                'users.name as student_name',
                'classroom.classroom_id as classroom_id',
                'classroom.classroom_name as classroom_name',
            ]);

        $data_arr = array();
        $sno = $start+1;
        foreach($records as $record){
            $classroom_registration_id = $record->classroom_registration_id;
            $status = $record->status;
            $student_id = $record->student_id;
            $student_name = $record->student_name;
            $email = $record->email;
            $phone_number = $record->phone_number;
            $classroom_id = $record->classroom_id;
            $classroom_name = $record->classroom_name;
            $created_at = $record->created_at;
            $updated_at = $record->updated_at;

            $data_arr[] = array(
                "classroom_registration_id" => $classroom_registration_id,
                "status" => $status,
                "student_id" => $student_id,
                "student_name" => $student_name,
                "email" => $email,
                "phone_number" => $phone_number,
                "classroom_id" => $classroom_id,
                "classroom_name" => $classroom_name,
                "created_at" => $created_at,
                "updated_at" => $updated_at
            );
        }

        return response()->json([
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data_arr
        ]);;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $materials = ClassroomRegistration::orderBy("status","asc")->get();
        return response()->json([
            'data' => $materials
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $classroomRegistrationId
     * @return \Illuminate\Http\Response
     */
    public function get($classroomRegistrationId)
    {
        Log::info('REQUEST TO GET ClassroomRegistration');
        $classroomRegistration = ClassroomRegistration::find($classroomRegistrationId);
        if($classroomRegistration === null){
            return response()->json(['message' => 'Registration not found'], 404);
        }
        
        return response()->json([
            'data' => $classroomRegistration
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        Log::info('REQUEST TO CREATE ClassroomRegistration');
        $validator = Validator::make($request->all(), [
            'student_id' => 'required|string|max:100',
            'classroom_id' => 'required|string|max:100',
        ]);
        if($validator->fails()){
            return response()->json([
                'code' => 'validation_data',
                'message' => 'The given data is invalid',
                'errors' => $validator->errors(),
            ], 400);
        }

        try {
            $student = User::find($request->student_id);
            if($student === null){
                return response()->json(['message' => 'Student not found'], 404);
            }

            $classroom = Classroom::find($request->classroom_id);
            if($classroom === null){
                return response()->json(['message' => 'Classroom not found'], 404);
            }

            $registrationFound = ClassroomRegistration::select('count(*) as allcount')
                ->where('classroom_id', '=', $classroom->classroom_id)
                ->where('student_id', '=', $student->user_id)
                ->count();
            
            if($registrationFound > 0){
                return response()->json(['message' => 'This student has registered for this classroom'], 400);
            }

            $classroomRegistration = new ClassroomRegistration();
            $classroomRegistration->classroom_registration_id = Generator::uuid4()->toString();
            $classroomRegistration->student_id = $request->student_id;
            $classroomRegistration->status = ClassroomRegistrationConst::REGISTERED;
            $classroomRegistration->classroom_id = $request->classroom_id;
            $classroomRegistration->save();
            Log::info($classroomRegistration);

            return response()->json([
                'message' => 'Registration is successfully created',
                'data' => $classroomRegistration
            ], 201);

        } catch (Throwable $e){
            Log::error($e);
            return response()->json(['message' => 'Failed to save'], 500);
        }
    }

    /**
     * Manual Payment.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $classroomRegistrationId
     * @return \Illuminate\Http\Response
     */
    public function pay($classroomRegistrationId, Request $request)
    {
        Log::info('REQUEST TO UPDATE ClassroomRegistration : ' .$classroomRegistrationId);

        $validator = Validator::make($request->all(), [
            'evidence_of_payment' => 'required|string|max:255',
        ]);

        if($validator->fails()){
            return response()->json([
                'code' => 'validation_data',
                'message' => 'The given data is invalid',
                'errors' => $validator->errors(),
            ], 400);
        }

        $classroomRegistration = ClassroomRegistration::find($classroomRegistrationId);
        if($classroomRegistration === null){
            return response()->json(['message' => 'Registration not found'], 404);
        }

        if($classroomRegistration->status === ClassroomRegistrationConst::VERIFIED){
            return response()->json(['message' => 'Payment has verified by admin'], 400);
        }

        try {
            $classroomRegistration->status = ClassroomRegistrationConst::PAID;
            $classroomRegistration->evidence_of_payment = $request->evidence_of_payment;
            $classroomRegistration->save();

            return response()->json([
                'message' => 'Payment completed, waiting to verified',
                'data' => $classroomRegistration,
            ], 200);
        } catch (Throwable $e){
            Log::error($e);
            return response()->json(['message' => 'Failed to save'], 500);
        }
    }

    /**
     * Reject Payment.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $classroomRegistrationId
     * @return \Illuminate\Http\Response
     */
    public function reject($classroomRegistrationId, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'rejected_reason' => 'required|string|max:255',
        ]);

        if($validator->fails()){
            return response()->json([
                'code' => 'validation_data',
                'message' => 'The given data is invalid',
                'errors' => $validator->errors(),
            ], 400);
        }

        $classroomRegistration = ClassroomRegistration::find($classroomRegistrationId);
        if($classroomRegistration === null){
            return response()->json(['message' => 'Registration not found'], 404);
        }

        if($classroomRegistration->status === ClassroomRegistrationConst::VERIFIED){
            return response()->json(['message' => 'Payment has verified by admin'], 400);
        }

        try {
            $classroomRegistration->status = ClassroomRegistrationConst::REJECTED;
            $classroomRegistration->rejected_reason = $request->rejected_reason;
            $classroomRegistration->save();

            return response()->json([
                'message' => 'Payment is successfully rejected',
                'data' => $classroomRegistration,
            ], 200);
        } catch (Throwable $e){
            Log::error($e);
            return response()->json(['message' => 'Failed to save'], 500);
        }
    }

    /**
     * Reject Payment.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $classroomRegistrationId
     * @return \Illuminate\Http\Response
     */
    public function verify($classroomRegistrationId, Request $request)
    {
        try {
            $classroomRegistration = ClassroomRegistration::find($classroomRegistrationId);
            if($classroomRegistration === null){
                return response()->json(['message' => 'Registration not found'], 404);
            }

            if($classroomRegistration->status === ClassroomRegistrationConst::VERIFIED){
                return response()->json(['message' => 'Payment has verified by admin'], 400);
            }

            $userLogged = JWTAuth::user();

            $classroomRegistration->status = ClassroomRegistrationConst::VERIFIED;
            $classroomRegistration->verified_by = $userLogged->user_id;
            $classroomRegistration->verified_at = date('Y-m-d H:i:s');
            $classroomRegistration->save();

            return response()->json([
                'message' => 'Payment is successfully verified',
                'data' => $classroomRegistration,
            ], 200);
        } catch (Throwable $e){
            Log::error($e);
            return response()->json(['message' => 'Failed to save'], 500);
        }
    }

    public function takeIn($classroomRegistrationId, Request $request)
    {
        try {
            $classroomRegistration = ClassroomRegistration::find($classroomRegistrationId);
            $participantFound = Participant::select('count(*) as allcount')
                ->where('classroom_id', '=', $classroomRegistration->classroom_id)
                ->where('classroom_registration_id', '=', $classroomRegistration->classroom_registration_id)
                ->where('student_id', '=', $classroomRegistration->student_id)
                ->count();

            if($classroomRegistration === null){
                return response()->json(['message' => 'Registration not found'], 404);
            }

            if($classroomRegistration->status !== ClassroomRegistrationConst::VERIFIED){
                return response()->json(['message' => 'This student is not allowed to take in to this classroom'], 400);
            }

            if($classroomRegistration->status === ClassroomRegistrationConst::JOINED && $participantFound > 0){
                return response()->json(['message' => 'This student has already joined to this classroom'], 400);
            }

            $classroomRegistration->status = ClassroomRegistrationConst::JOINED;
            $classroomRegistration->save();

            $participant = new Participant();
            $participant->participant_id = Generator::uuid4()->toString();
            $participant->classroom_id = $classroomRegistration->classroom_id;
            $participant->classroom_registration_id = $classroomRegistration->classroom_registration_id;
            $participant->student_id = $classroomRegistration->student_id;
            $participant->save();

            return response()->json([
                'message' => 'This student is successfully joined',
                'data' => $classroomRegistration,
            ], 200);
        } catch (Throwable $e){
            Log::error($e);
            return response()->json(['message' => 'Failed to save'], 500);
        }
    }

}
