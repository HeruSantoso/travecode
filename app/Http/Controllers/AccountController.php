<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Common\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Ramsey\Uuid\Uuid as Generator;

class AccountController extends Controller
{

    public function createStudentAccount(Request $request)
    {
        Log::info('Request to create student');
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'dob' => 'required|date',
            'grade' => 'required|string',
            'phone_number' => 'required|string'
        ]);

        if($validator->fails()){
            return response()->json([
                'code' => 'validation_data',
                'message' => 'The given data is invalid',
                'errors' => $validator->errors(),
            ], 400);
        }

        try {
            $user = User::create([
                'user_id' => Generator::uuid4()->toString(),
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'password' => Hash::make($request->get('password')),
                'dob' => $request->get('dob'),
                'grade' => $request->get('grade'),
                'phone_number' => $request->get('phone_number'),
                'role' => Role::STUDENT,
            ]);
    
            $message = "Student account created";
            $data = $user;
            return response()->json(compact('message','data'),201);
        } catch (Throwable $e){
            Log::error($e);
            return response()->json(['message' => 'Failed to create user'], 500);
        }
    }

    public function createFacilitatorAccount(Request $request)
    {

        Log::info('Request to create student');
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'dob' => 'required|date',
            'grade' => 'required|string',
            'phone_number' => 'required|string'
        ]);

        if($validator->fails()){
            return response()->json([
                'code' => 'validation_data',
                'message' => 'The given data is invalid',
                'errors' => $validator->errors(),
            ], 400);
        }

        try {
            $user = User::create([
                'user_id' => Generator::uuid4()->toString(),
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'password' => Hash::make($request->get('password')),
                'dob' => $request->get('dob'),
                'grade' => $request->get('grade'),
                'phone_number' => $request->get('phone_number'),
                'role' => Role::FACILITATOR,
            ]);


            $message = "Facilitator account created";
            $data = $user;
            return response()->json(compact('message','data'),201);
        } catch (Throwable $e){
            Log::error($e);
            return response()->json(['message' => 'Failed to create user'], 500);
        }
    }

    public function getFacilitatorList()
    {
        $users = User::where('role', '=', Role::FACILITATOR)
            ->orderBy("name","asc")->get();
        return response()->json([
            'data' => $users
        ]);
    }

    public function getStudentList()
    {
        $users = User::where('role', '=', Role::STUDENT)
            ->orderBy("name","asc")->get();
        return response()->json([
            'data' => $users
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $studentId
     * @return \Illuminate\Http\Response
     */
    public function resetPassword($userId, Request $request)
    {
        Log::info('REQUEST TO UPDATE RESET PSWD : ' .$userId);

        $validator = Validator::make($request->all(), [
            'password' => 'required|string|min:6|confirmed',
        ]);;

        if($validator->fails()){
            return response()->json([
                'code' => 'validation_data',
                'message' => 'The given data is invalid',
                'errors' => $validator->errors(),
            ], 400);
        }

        $user = User::find($userId);
        if($user === null){
            return response()->json(['message' => 'User not found'], 404);
        }

        try {
            $user->password = Hash::make($request->password);
            $user->save();

            return response()->json([
                'message' => 'Password is successfully reset',
                'data' => $user,
            ], 200);
        } catch (Throwable $e){
            Log::error($e);
            return response()->json(['message' => 'Failed to save'], 500);
        }
    }

}
