<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Common\Role;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use Ramsey\Uuid\Uuid as Generator;
use Illuminate\Support\Facades\Hash;

class FacilitatorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pagination(Request $request)
    {
        Log::info($request);
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // Rows display per page

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name 
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value

        // Total records
        $totalRecords = User::select('count(*) as allcount')
            ->where('role', '=', Role::FACILITATOR)
            ->count();
        $totalRecordswithFilter = User::select('count(*) as allcount')
            ->where('name', 'like', '%' .$searchValue . '%')
            ->where('role', '=', Role::FACILITATOR)
            ->count();

        // Fetch records
        $records = User::orderBy($columnName,$columnSortOrder)
            ->where('name', 'like', '%' .$searchValue . '%')
            ->where('role', '=', Role::FACILITATOR)
            ->skip($start)
            ->take($rowperpage)
            ->get();

        $data_arr = array();
        $sno = $start+1;
        foreach($records as $record){
            $user_id = $record->user_id;
            $name = $record->name;
            $email = $record->email;
            $dob = $record->dob;
            $grade = $record->grade;
            $phone_number = $record->phone_number;
            $created_at = $record->created_at;
            $updated_at = $record->updated_at;

            $data_arr[] = array(
                "user_id" => $user_id,
                "name" => $name,
                "email" => $email,
                "dob" => $dob,
                "grade" => $grade,
                "phone_number" => $phone_number,
                "created_at" => $created_at,
                "updated_at" => $updated_at
            );
        }

        return response()->json([
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data_arr
        ]);;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $facilitators = User::orderBy("name","asc")
            ->where('role', '=', Role::FACILITATOR)
            ->get();
        return response()->json([
            'data' => $facilitators
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $facilitatorId
     * @return \Illuminate\Http\Response
     */
    public function get($facilitatorId)
    {
        Log::info('REQUEST TO GET User');
        $facilitator = User::find($facilitatorId);
        if($facilitator === null){
            return response()->json(['message' => 'Facilitator not found'], 404);
        } else {
            if ($facilitator->role !== Role::FACILITATOR){
                return response()->json(['message' => 'This account is not facilitator'], 400);
            }
        }
        
        return response()->json([
            'data' => $facilitator
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        Log::info('REQUEST TO CREATE User');
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'dob' => 'required|date',
            'grade' => 'required|string',
            'phone_number' => 'required|string'
        ]);
        if($validator->fails()){
            return response()->json([
                'code' => 'validation_data',
                'message' => 'The given data is invalid',
                'errors' => $validator->errors(),
            ], 400);
        }

        try {
            $facilitator = new User();
            $facilitator->user_id = Generator::uuid4()->toString();
            $facilitator->name = $request->name;
            $facilitator->email = strtolower($request->email);
            $facilitator->password = Hash::make($request->password);
            $facilitator->dob = $request->dob;
            $facilitator->grade = $request->grade;
            $facilitator->phone_number = $request->phone_number;
            $facilitator->role = Role::FACILITATOR;
            $facilitator->save();
            Log::info($facilitator);

            return response()->json([
                'message' => 'User is successfully created',
                'data' => $facilitator
            ], 201);

        } catch (Throwable $e){
            Log::error($e);
            return response()->json(['message' => 'Failed to save'], 500);
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $facilitatorId
     * @return \Illuminate\Http\Response
     */
    public function update($facilitatorId, Request $request)
    {
        Log::info('REQUEST TO UPDATE User : ' .$facilitatorId);

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'dob' => 'required|date',
            'grade' => 'required|string',
            'phone_number' => 'required|string'
        ]);;

        if($validator->fails()){
            return response()->json([
                'code' => 'validation_data',
                'message' => 'The given data is invalid',
                'errors' => $validator->errors(),
            ], 400);
        }

        $facilitator = User::find($facilitatorId);
        if($facilitator === null){
            return response()->json(['message' => 'User not found'], 404);
        } else {
            if ($facilitator->role !== Role::FACILITATOR){
                return response()->json(['message' => 'This account is not facilitator'], 400);
            }

            if ($facilitator->email !== $request->email){
                $facilitatorFound = User::where('email','=',strtolower($request->email)) -> first();
                if($facilitatorFound !== null){
                    return response()->json(['message' => 'Email is already used'], 400);
                }
            }
        }

        try {
            $facilitator->name = $request->name;
            $facilitator->email = strtolower($request->email);
            $facilitator->dob = $request->dob;
            $facilitator->grade = $request->grade;
            $facilitator->phone_number = $request->phone_number;
            $facilitator->role = Role::FACILITATOR;
            $facilitator->save();

            return response()->json([
                'message' => 'User is successfully updated',
                'data' => $facilitator,
            ], 200);
        } catch (Throwable $e){
            Log::error($e);
            return response()->json(['message' => 'Failed to save'], 500);
        }
    }

}
