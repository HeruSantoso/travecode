<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Schedule;
use App\Models\Classroom;
use App\Models\User;
use App\Common\Role;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use Ramsey\Uuid\Uuid as Generator;
use JWTAuth;
use \stdClass;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pagination(Request $request)
    {
        Log::info($request);
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // Rows display per page

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');
        $classroom_id = $request->get('classroom_id');

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name 
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value
        $classroomIdValue = $classroom_id; // Search value

        // $userLogged = JWTAuth::user();
        // $facilitatorId = '';
        // if($userLogged->role === Role::FACILITATOR){
        //     $facilitatorId = $userLogged->user_id;
        // }

        // Total records
        $totalRecords = Schedule::select('count(*) as allcount')
            ->where('classroom_id', 'like', '%' .$classroomIdValue . '%')
            // ->where('schedule.facilitator_id', 'like', '%' .$facilitatorId . '%')
            ->count();
        $totalRecordswithFilter = Schedule::select('count(*) as allcount')
            ->where('topic', 'like', '%' .$searchValue . '%')
            ->where('classroom_id', 'like', '%' .$classroomIdValue . '%')
            // ->where('schedule.facilitator_id', 'like', '%' .$facilitatorId . '%')
            ->count();

        // Fetch records
        $records = Schedule::join('users', 'users.user_id', '=', 'schedule.facilitator_id')
        ->orderBy($columnName,$columnSortOrder)
        ->where('topic', 'like', '%' .$searchValue . '%')
        ->where('schedule.classroom_id', 'like', '%' .$classroomIdValue . '%')
        // ->where('schedule.facilitator_id', 'like', '%' .$facilitatorId . '%')
        ->skip($start)
        ->take($rowperpage)
        ->get([
            'schedule.schedule_id as schedule_id',
            'schedule.topic as topic',
            'schedule.start as start',
            'schedule.end as end',
            'schedule.meeting_link as meeting_link',
            'schedule.classroom_id as classroom_id',
            'schedule.module as module',
            'schedule.evidence_live_session as evidence_live_session',
            'schedule.is_any_homework as is_any_homework',
            'schedule.notes as notes',
            'schedule.facilitator_id as facilitator_id', 
            'users.name as facilitator_name'
        ]);

        $data_arr = array();
        $sno = $start+1;
        foreach($records as $record){
            $schedule_id = $record->schedule_id;
            $topic = $record->topic;
            $start = $record->start;
            $end = $record->end;
            $meeting_link = $record->meeting_link;
            $module = $record->module;
            $evidence_live_session = $record->evidence_live_session;
            $is_any_homework = $record->is_any_homework;
            $notes = $record->notes;
            $classroom_id = $record->classroom_id;
            $facilitator_id = $record->facilitator_id;
            $facilitator_name = $record->facilitator_name;

            $data_arr[] = array(
                "schedule_id" => $schedule_id,
                "topic" => $topic,
                "start" => $start,
                "end" => $end,
                "meeting_link" => $meeting_link,
                "module" => $module,
                "evidence_live_session" => $evidence_live_session,
                "is_any_homework" => $is_any_homework,
                "notes" => $notes,
                "classroom_id" => $classroom_id,
                "facilitator_id" => $facilitator_id,
                "facilitator_name" => $facilitator_name
            );
        }

        return response()->json([
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data_arr
        ]);;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $classroomList = Schedule::orderBy("start","asc")->get();
        return response()->json([
            'data' => $classroomList
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $scheduleId
     * @return \Illuminate\Http\Response
     */
    public function get($scheduleId)
    {
        Log::info('REQUEST TO GET SCHEDULE');
        $schedule = Schedule::find($scheduleId);
        if($schedule === null){
            return response()->json(['message' => 'Schedule not found'], 404);
        }
        
        return response()->json([
            'data' => $schedule
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        try {
            Log::info('REQUEST TO CREATE SCHEDULE');
            $validator = Validator::make($request->all(), [
                'topic' => 'required|string|max:100',
                'meeting_link' => 'required|string|max:255',
                'start' => "required|date",
                'end' => "required|date",
                'classroom_id' => 'required|string',
                'facilitator_id' => 'required|string',
            ]);
            if($validator->fails()){
                return response()->json([
                    'code' => 'validation_data',
                    'message' => 'The given data is invalid',
                    'errors' => $validator->errors(),
                ], 400);
            }

            // Validate facilitator
            $user = User::where('user_id', '=', $request->facilitator_id)->first();
            if($user === null){
                return response()->json(['message' => 'Facilitator not found'], 500);
            } else {
                if($user->role !== Role::FACILITATOR){
                    return response()->json(['message' => 'Invalid facilitator'], 500);
                }
            }
            
            // Validate classroom
            $classroom = Classroom::where('classroom_id', '=', $request->classroom_id)->first();
            if($classroom === null){
                return response()->json(['message' => 'Classroom not found'], 500);
            }
            
            $schedule = new Schedule();
            $schedule->schedule_id = Generator::uuid4()->toString();
            $schedule->topic = $request->topic;
            $schedule->meeting_link = $request->meeting_link;
            $schedule->start = $request->start;
            $schedule->end = $request->end;
            $schedule->date = $request->start;
            $schedule->facilitator_id = $request->facilitator_id;
            $schedule->classroom_id = $request->classroom_id;
            $schedule->save();
            Log::info($schedule);

            return response()->json([
                'message' => 'Schedule is successfully created',
                'data' => $schedule
            ], 201);

        } catch (Throwable $e){
            Log::error($e);
            return response()->json(['message' => 'Failed to save'], 500);
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $scheduleId
     * @return \Illuminate\Http\Response
     */
    public function update($scheduleId, Request $request)
    {
        try {
            Log::info('REQUEST TO UPDATE SCHEDULE : ' .$scheduleId);

            $validator = Validator::make($request->all(), [
                'topic' => 'required|string|max:100',
                'meeting_link' => 'required|string|max:255',
                'start' => "required|date",
                'end' => "required|date",
                'facilitator_id' => 'required|string',
            ]);
            if($validator->fails()){
                return response()->json([
                    'code' => 'validation_data',
                    'message' => 'The given data is invalid',
                    'errors' => $validator->errors(),
                ], 400);
            }

            // Validate facilitator
            $user = User::where('user_id', '=', $request->facilitator_id)->first();
            if($user === null){
                return response()->json(['message' => 'Facilitator not found'], 500);
            } else {
                if($user->role !== Role::FACILITATOR){
                    return response()->json(['message' => 'Invalid facilitator'], 500);
                }
            }

            $schedule = Schedule::find($scheduleId);
            if($schedule === null){
                return response()->json(['message' => 'Schedule not found'], 404);
            }
        
            $schedule->topic = $request->topic;
            $schedule->meeting_link = $request->meeting_link;
            $schedule->start = $request->start;
            $schedule->end = $request->end;
            $schedule->date = $request->start;
            $schedule->facilitator_id = $request->facilitator_id;
            $schedule->save();

            return response()->json([
                'message' => 'Schedule is successfully updated',
                'data' => $schedule,
            ], 200);
        } catch (Throwable $e){
            Log::error($e);
            return response()->json(['message' => 'Failed to save'], 500);
        }
    }

    public function completeSchedule($scheduleId, Request $request)
    {
        try {
            Log::info('REQUEST TO UPDATE SCHEDULE : ' .$scheduleId);

            $validator = Validator::make($request->all(), [
                'module' => 'required|string|max:255',
                'evidence_live_session' => 'required|string|max:255',
                'is_any_homework' => 'required|boolean',
                'notes' => 'required|string|max:255',
            ]);
            if($validator->fails()){
                return response()->json([
                    'code' => 'validation_data',
                    'message' => 'The given data is invalid',
                    'errors' => $validator->errors(),
                ], 400);
            }
        
            $schedule = Schedule::find($scheduleId);
            if($schedule === null){
                return response()->json(['message' => 'Schedule not found'], 404);
            }

            $userLogged = JWTAuth::user();
            if($userLogged->user_id !== $schedule->facilitator_id){
                return response()->json(['message' => 'Unauthorized Facilitator'], 404);
            }
            
            $schedule->module = $request->module;
            $schedule->evidence_live_session = $request->evidence_live_session;
            $schedule->is_any_homework = $request->is_any_homework;
            $schedule->notes = $request->notes;
            $schedule->save();

            return response()->json([
                'message' => 'Schedule completed',
                'data' => $schedule,
            ], 200);
        } catch (Throwable $e){
            Log::error($e);
            return response()->json(['message' => 'Failed to save'], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $scheduleId
     * @return \Illuminate\Http\Response
     */
    public function delete($scheduleId)
    {
        $schedule = Schedule::find($scheduleId);
        if($schedule === null){
            return response()->json(['message' => 'Schedule not found'], 404);
        }

        try {
            $schedule -> delete();
            return response()->json(['message' => 'Schedule is successfully deleted'], 200);
        } catch (Throwable $e){
            Log::error($e);
            return response()->json(['message' => 'Failed to delete'], 500);
        }
    }

    public function getCalender(Request $request){
        $validator = Validator::make($request->all(), [
            'start' => "required|date",
            'end' => "required|date",
        ]);
        if($validator->fails()){
            return response()->json([
                'code' => 'validation_data',
                'message' => 'The given data is invalid',
                'errors' => $validator->errors(),
            ], 400);
        }

        $userLogged = JWTAuth::user();
        if($userLogged === null) {
            return response()->json(['message' => 'User not found'], 404);
        }

        $startDate = $request->get("start");
        $endDate = $request->get('end');
        
        try {
            $records = [];
            if ($userLogged->role === Role::ADMIN) { 
                $records = Schedule::join('users', 'users.user_id', '=', 'schedule.facilitator_id')
                    ->join('classroom', 'classroom.classroom_id', '=', 'schedule.classroom_id')
                    ->orderBy('start','asc')
                    ->whereBetween('date', [$startDate, $endDate])
                    ->get([
                        'schedule.schedule_id as schedule_id',
                        'schedule.topic as topic',
                        'schedule.start as start',
                        'schedule.end as end',
                        "schedule.date as date",
                        'schedule.meeting_link as meeting_link',
                        'classroom.classroom_id as classroom_id',
                        'classroom.classroom_name as classroom_name',
                        'classroom.status as classroom_status',
                        'users.user_id as facilitator_id', 
                        'users.name as facilitator_name'
                    ]);
            } else if ($userLogged->role === Role::FACILITATOR) {
                $records = Schedule::join('users', 'users.user_id', '=', 'schedule.facilitator_id')
                    ->join('classroom', 'classroom.classroom_id', '=', 'schedule.classroom_id')
                    ->orderBy('start','asc')
                    ->whereBetween('date', [$startDate, $endDate])
                    ->where('schedule.facilitator_id','=',$userLogged->user_id)
                    ->where('classroom.status','=',1)
                    ->get([
                        'schedule.schedule_id as schedule_id',
                        'schedule.topic as topic',
                        'schedule.start as start',
                        'schedule.end as end',
                        "schedule.date as date",
                        'schedule.meeting_link as meeting_link',
                        'classroom.classroom_id as classroom_id',
                        'classroom.classroom_name as classroom_name',
                        'classroom.status as classroom_status',
                        'users.user_id as facilitator_id', 
                        'users.name as facilitator_name'
                    ]);
            } else if ($userLogged->role === Role::STUDENT) {
                $records = Schedule::join('users', 'users.user_id', '=', 'schedule.facilitator_id')
                    ->join('classroom', 'classroom.classroom_id', '=', 'schedule.classroom_id')
                    ->join('participant', 'participant.classroom_id','=','schedule.classroom_id')
                    ->orderBy('start','asc')
                    ->whereBetween('date', [$startDate, $endDate])
                    ->where('student_id','=',$userLogged->user_id)
                    ->get([
                        'schedule.schedule_id as schedule_id',
                        'schedule.topic as topic',
                        'schedule.start as start',
                        'schedule.end as end',
                        "schedule.date as date",
                        'schedule.meeting_link as meeting_link',
                        'classroom.classroom_id as classroom_id',
                        'classroom.classroom_name as classroom_name',
                        'classroom.status as classroom_status',
                        'users.user_id as facilitator_id', 
                        'users.name as facilitator_name', 
                        'participant.student_id as student_id'
                    ]);
            }
            
            $data_arr = array();
            foreach($records as $record){
                $extendedProps = new stdClass();
                $extendedProps->schedule_id = $record-> schedule_id;
                $extendedProps->meeting_link = $record-> meeting_link;
                $extendedProps->classroom_id = $record-> classroom_id;
                $extendedProps->classroom_name = $record-> classroom_name;
                $extendedProps->topic = $record-> topic;
                $extendedProps->classroom_status = $record-> classroom_status;
                $extendedProps->facilitator_id = $record-> facilitator_id;
                $extendedProps->facilitator_name = $record-> facilitator_name;

                $className = null;
                if($record-> classroom_status == 0){
                    $className = "fc-warning";
                } else if($record-> classroom_status == 1){
                    $className = "fc-primary";
                } else if($record-> classroom_status == 2){
                    $className = "fc-danger";
                }

                $data_arr[] = array(
                    "id" => $record -> schedule_id,
                    "title" => $record-> classroom_name,
                    "start" => $record-> start,
                    "end" => $record-> end,
                    "description" => $record-> topic,
                    "extendedProps" => $extendedProps,
                    "className" => $className,
                );
            }

            return response()->json($data_arr, 200);
        } catch (Throwable $e){
            Log::error($e);
            return response()->json(['message' => 'Failed to load'], 500);
        }
    }
}
