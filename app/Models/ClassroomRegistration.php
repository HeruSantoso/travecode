<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\BaseModel;

class ClassroomRegistration extends BaseModel
{
    protected $table = 'classroom_registration';
    protected $primaryKey = 'classroom_registration_id';
    
}
