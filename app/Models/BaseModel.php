<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Common\Blameable;

class BaseModel extends Model
{
    use HasFactory;
    use Blameable;

    public $incrementing = false;
    protected $keyType = 'string';
    
}
