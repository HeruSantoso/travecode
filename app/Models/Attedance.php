<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\BaseModel;

class Attedance extends BaseModel
{
    protected $table = 'attedance';
    protected $primaryKey = 'attedance_id';
}
