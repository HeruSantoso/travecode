<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\BaseModel;

class Material extends BaseModel
{
    protected $table = 'material';
    protected $primaryKey = 'material_id';
    
}
