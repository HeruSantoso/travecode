<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\BaseModel;

class Classroom extends BaseModel
{
    protected $table = 'classroom';
    protected $primaryKey = 'classroom_id';
    
}
