<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\BaseModel;

class Schedule extends BaseModel
{
    protected $table = 'schedule';
    protected $primaryKey = 'schedule_id';
    
}
