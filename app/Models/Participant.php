<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\BaseModel;

class Participant extends BaseModel
{
    protected $table = 'participant';
    protected $primaryKey = 'participant_id';
    
}
