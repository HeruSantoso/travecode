<?php
namespace App\Common;

class Role {
   const ADMIN = 'role_admin';
   const STUDENT = 'role_student';
   const FACILITATOR = 'role_facilitator';

   public static function multiple($arr){
      return join(",",$arr);
   }

}
?>