<?php
namespace App\Common;

class ClassroomRegistrationConst {
   const REJECTED = 0;
   const REGISTERED = 1;
   const PAID = 2;
   const VERIFIED = 3;
   const JOINED = 4;
}
?>