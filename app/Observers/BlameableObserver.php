<?php

namespace App\Observers;
use Illuminate\Database\Eloquent\Model;

use JWTAuth;

class BlameableObserver
{

    public function creating(Model $model)
    {
        $userLogged = JWTAuth::user();
        
        if($userLogged == null) {
            $model->created_by = 'ANONYMOUS';
            $model->updated_by = 'ANONYMOUS';
        } else {
            $model->created_by = $userLogged->user_id;
            $model->updated_by = $userLogged->user_id;
        }
    }

    public function updating(Model $model)
    {
        $userLogged = JWTAuth::user();

        if($userLogged == null) {
            $model->updated_by = 'ANONYMOUS';
        } else {
            $model->updated_by = $userLogged->user_id;
        }
    }
}
